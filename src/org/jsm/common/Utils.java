package org.jsm.common;


import java.io.*;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.Map;

/**
 * User: Anatoly Semenov
 * Date: 03.04.14
 */
public class Utils {
    public static void close(AutoCloseable... resources) {
        for (AutoCloseable autoCloseable : resources) {
            try{
                if (autoCloseable != null)
                    autoCloseable.close();
            }
            catch (Exception e) {
                //ignore
            }
        }
    }

    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty() || s.trim().isEmpty();
    }

    public static boolean isEmpty(Collection s) {
        return s == null || s.isEmpty();
    }

    public static boolean isEmpty(Map s) {
        return s == null || s.isEmpty();
    }

    /**
     * Returns true is the value is null or empty string/collection/map. If the value is a string containing only
     * space characters than it is treated as empty as well.
     */
    @SuppressWarnings("SimplifiableIfStatement")
    public static boolean isEmpty(Object value) {
        if (value == null) {
            return true;
        } else if (value instanceof String) {
            return isEmpty((String) value);
        } else if (value instanceof Collection) {
            return isEmpty((Collection) value);
        } else if (value instanceof Map) {
            return isEmpty((Map) value);
        } else if (value.getClass().isArray()) {
            return Array.getLength(value) == 0;
        } else {
            return false;
        }
    }

    /**
     * Returns true if at least one of the objects {@link #isEmpty(Object) isEmpty}.
     */
    public static boolean isEmpty(Object... objects) {
        if (objects == null) {
            return true;
        }
        for (Object object : objects) {
            if (isEmpty(object)) {
                return true;
            }
        }
        return false;
    }

    public static <T> T nvl(T object, T nullReplacement) {
        return object == null ? nullReplacement : object;
    }

    public static String encodeString(String source, String csn) {
        try {
            Charset charset = new CharsetDetector().detectCharset(source, new String[] {"utf-8", "windows-1251"});
            return new String(source.getBytes(charset), csn);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static void assertNotEmpty(Object o, String field) {
        if (isEmpty(o))
            throw new IllegalArgumentException("Field " + field + " is empty");
    }

    public static void assertNotNull(Object o, String field) {
        if (o == null)
            throw new IllegalArgumentException("Object " + field + " is null");
    }

    public static class Serialization {
        public static <T> Path save(T object, Path to, OpenOption... openOptions) throws IOException {
            try (OutputStream fileOutputStream = Files.newOutputStream(to, openOptions);
                ObjectOutputStream oos = new ObjectOutputStream(fileOutputStream)) {
                oos.writeObject(object);
                oos.flush();
            }
            return to;
        }

        public static <T> T read(Path from) throws IOException, ClassNotFoundException {
            Object object;
            try (InputStream fis = Files.newInputStream(from, StandardOpenOption.READ);
                    ObjectInputStream ois = new ObjectInputStream(fis)) {
                object =  ois.readObject();
            }
            //noinspection unchecked
            return (T) object;

        }
    }
}
