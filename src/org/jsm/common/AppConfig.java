package org.jsm.common;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/**
 * User: Anatoly Semenov
 * Date: 23.04.14
 */
public class AppConfig {
    public static final String DEFAULT_CONFIG_PROPERTIES_PATH = "config.properties";

    private volatile Properties properties;

    private AppConfig(Properties properties) {
        this.properties = properties;
    }

    private static AppConfig getInstance() {
        return LazyInitializer.INSTANCE;
    }

    private static Properties loadProperties(String configLocation) {
        InputStream is = null;
        try {
            Properties properties = new Properties();
            //is = configLocation.openStream();
            URL resourceUrl = AppConfig.class.getClassLoader().getResource(configLocation);
            if (resourceUrl != null) {
                is = resourceUrl.openStream();
            } else {
                is = new FileInputStream(configLocation);
            }
            properties.load(is);
            return properties;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            Utils.close(is);
        }
    }

    public static String getProperty(String name) {
        if (!getInstance().properties.containsKey(name)) {
            throw new IllegalArgumentException("No such property stated in AppConfig: " + name);
        } else {
            return getInstance().properties.getProperty(name);
        }
    }

    public static String getSystemEncoding() {
        return getProperty("encoding", "utf-8");
    }

    public static String getProperty(String name, String defaultValue) {
        return getInstance().properties.getProperty(name, defaultValue);
    }

    private static class LazyInitializer {
        private static AppConfig INSTANCE;

        static {
            INSTANCE = new AppConfig(loadProperties(DEFAULT_CONFIG_PROPERTIES_PATH));
        }
    }

    public class TestApi {
        public void setProperty(String key, String value) {
            properties.setProperty(key, value);
        }
    }
}
