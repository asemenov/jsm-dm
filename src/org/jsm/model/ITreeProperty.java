package org.jsm.model;

import org.jsm.model.impl.property.TreeProperty;

import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 03.04.14
 */
public interface ITreeProperty extends IProperty{
    public Collection<? extends IProperty> getChildren();
    public ITreeProperty getParent();
    public void setParent(ITreeProperty parent);
    public void addValue(IValue value);
    public void addProperty(IProperty iProperty);
    //public ITreeProperty get

    public interface ITreePropertyBuilder {
        public ITreePropertyBuilder setParent(ITreeProperty parent);
        public ITreePropertyBuilder addProperty(IProperty property);
        public ITreePropertyBuilder setProperties(Collection<IProperty> properties);
        public ITreePropertyBuilder setName(String name);
        public ITreePropertyBuilder setNullable(boolean nullable);
        public ITreePropertyBuilder setMultiple(boolean multiple);
        public ITreePropertyBuilder setPropertyValues(Collection<IValue> values);
        public ITreePropertyBuilder addPropertyValue(IValue value);
        public ITreeProperty build();
    }


    public static class TreeFactory {
        public static ITreePropertyBuilder treePropertyBuilder() {
            return new TreeProperty.TreePropertyBuilder();
        }

        public static ITreePropertyBuilder treePropertyBuilder(ITreeProperty iTreeProperty) {
            return new TreeProperty.TreePropertyBuilder(iTreeProperty);
        }


    }
}
