package org.jsm.model;

import java.io.Serializable;
import java.util.Map;

/**
 * User: Anatoly Semenov
 * Date: 30.04.2014
 */
public interface IDataSet extends Serializable{
    public IModel getModel();
    public Map<IProperty, IDataItem> getDataItems();
    public String getName();
    public String getComment();
    public void setName(String name);
    public void setComments(String comment);

}
