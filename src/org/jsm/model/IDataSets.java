package org.jsm.model;

import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 06.05.2014
 */
public interface IDataSets {
    public IModel getModel();
    public Collection<IDataSet> getDataSets();
}
