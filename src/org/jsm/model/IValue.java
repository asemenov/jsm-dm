package org.jsm.model;

import org.jsm.model.impl.value.StringValue;

import java.io.Serializable;

/**
 * User: Anatoly Semenov
 * Date: 06.04.14
 */
public interface IValue<T> extends Serializable{
    public T getValue();

    public static class Factory {
        public static IValue<String> createStringValue(String value) {
            return new StringValue(value);
        }
    }
}
