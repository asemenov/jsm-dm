package org.jsm.model;

import org.jsm.model.impl.model.ModelImpl;

import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 03.04.14
 */
public interface IModel {
    public String getName();
    public Collection<IProperty> getProperties();

    public static class Factory {
        public static IModel createModel(String name, Collection<IProperty> iProperties) {
            return new ModelImpl(name, iProperties);
        }
    }
}
