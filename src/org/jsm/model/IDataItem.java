package org.jsm.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 30.04.2014
 */
public interface IDataItem extends Serializable{
    public Collection<IValue> getPositiveValues();
    public Collection<IValue> getNegativeValues();
    public Collection<IValue> getUndefinedValues();
    public IProperty getIProperty();
}
