package org.jsm.model;

import org.jsm.model.impl.property.SelectProperty;

import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 03.04.14
 */
public interface IProperty {
    /**
     * Return name of property
     * */
    public String getName();

    /**
     * Return true if property describes (+,-,t) logic and false if (+,-) logic
     * */
    public boolean isNullable();

    /**
     * Return true if property can have any positive values.
     * */
    public boolean isMultiple();

    /**
     * Return <code>Collection</code> of property values.
     * */
    public Collection<IValue> getPropertyValues();

    public static class Factory {
        public static IPropertyBuilder selectPropertyBuilder() {
            return new SelectProperty.SelectPropertyBuilder();
        }

        public static IPropertyBuilder selectPropertyBuilder(SelectProperty selectProperty) {
            return new SelectProperty.SelectPropertyBuilder(selectProperty);
        }
    }

    public interface IPropertyBuilder {
        public IProperty build();
        public IPropertyBuilder setName(String name);
        public IPropertyBuilder setNullable(boolean nullable);
        public IPropertyBuilder setMultiple(boolean multiple);
        public IPropertyBuilder setPropertyValues(Collection<IValue> values);
        public IPropertyBuilder addPropertyValue(IValue value);
    }
 }
