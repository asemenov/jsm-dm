package org.jsm.model.impl.data;

import org.jsm.model.IDataItem;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.common.Utils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

/**
 * User: Anatoly Semenov
 * Date: 30.04.2014
 */
public class DataItem implements IDataItem, Serializable {
    private IProperty iProperty;
    private Collection<IValue> positive;
    private Collection<IValue> negative;
    private Collection<IValue> undefined;


    public DataItem(IProperty iProperty, Collection<IValue> positive, Collection<IValue> negative, Collection<IValue> undefined) {
        this.iProperty = iProperty;
        this.positive = Utils.nvl(positive, Collections.<IValue>emptyList());
        this.negative = Utils.nvl(negative, Collections.<IValue>emptyList());
        this.undefined = Utils.nvl(undefined, Collections.<IValue>emptyList());
    }

    @Override
    public Collection<IValue> getPositiveValues() {
        return positive;
    }

    @Override
    public Collection<IValue> getNegativeValues() {
        return negative;
    }

    @Override
    public Collection<IValue> getUndefinedValues() {
        return undefined;
    }

    @Override
    public IProperty getIProperty() {
        return iProperty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataItem dataItem = (DataItem) o;

        if (!iProperty.equals(dataItem.iProperty)) return false;
        if (!negative.equals(dataItem.negative)) return false;
        if (!positive.equals(dataItem.positive)) return false;
        if (!undefined.equals(dataItem.undefined)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = iProperty.hashCode();
        result = 31 * result + positive.hashCode();
        result = 31 * result + negative.hashCode();
        result = 31 * result + undefined.hashCode();
        return result;
    }


}
