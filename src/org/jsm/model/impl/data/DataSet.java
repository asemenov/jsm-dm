package org.jsm.model.impl.data;

import org.jsm.model.IDataItem;
import org.jsm.model.IDataSet;
import org.jsm.model.IModel;
import org.jsm.model.IProperty;

import java.util.Map;

/**
 * User: Anatoly Semenov
 * Date: 30.04.2014
 */
public class DataSet implements IDataSet {
    private IModel iModel;
    private Map<IProperty, IDataItem> dataItems;
    private String name;
    private String comments;

    public DataSet(IModel iModel, Map<IProperty, IDataItem> dataItems, String name, String comments)
    {
        this.iModel = iModel;
        this.dataItems = dataItems/*nvl(dataItems, Map.<IDataItem>emptyList())*/;
        this.name = name;
        this.comments = comments;
    }

    @Override
    public IModel getModel() {
        return iModel;
    }

    @Override
    public Map<IProperty, IDataItem> getDataItems() {
        return dataItems;
    }

    public String getComment() {
        return comments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }


}
