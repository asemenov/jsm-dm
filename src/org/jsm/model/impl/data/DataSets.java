package org.jsm.model.impl.data;

import org.jsm.model.IDataSet;
import org.jsm.model.IDataSets;
import org.jsm.model.IModel;
import org.jsm.common.Utils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * User: Anatoly Semenov
 * Date: 06.05.2014
 */
public class DataSets implements IDataSets, Serializable {
    private IModel model;
    private Collection<IDataSet> dataSets;

    public DataSets(IModel model, Collection<IDataSet> dataSets) {
        this.model = model;
        this.dataSets = Utils.nvl(dataSets, Collections.<IDataSet>emptyList());
    }

    public DataSets(IModel model, IDataSet... dataSets) {
        this.model = model;
        this.dataSets = dataSets == null ? Collections.emptyList() : Arrays.asList(dataSets);
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public Collection<IDataSet> getDataSets() {
        return dataSets;
    }
}
