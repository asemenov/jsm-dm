package org.jsm.model.impl.property;

import org.jsm.common.Utils;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;

/**
 * User: Anatoly Semenov
 * Date: 14.04.14
 */
public abstract class AbstractProperty implements IProperty, Serializable {
    protected String name;
    protected boolean isNullable;
    protected boolean isMultiple;
    protected Collection<IValue> values;

    protected AbstractProperty(String name) {
        this(name, false, false, Collections.<IValue>emptyList());
    }

    protected AbstractProperty(String name, boolean isNullable, boolean isMultiple, Collection<IValue> values) {
        this.name = name;
        this.isNullable = isNullable;
        this.isMultiple = isMultiple;
        this.values = Utils.isEmpty(values) ? Collections.<IValue>emptyList() : values;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isNullable() {
        return isNullable;
    }

    @Override
    public boolean isMultiple() {
        return isMultiple;
    }

    @Override
    public Collection<IValue> getPropertyValues() {
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractProperty that = (AbstractProperty) o;

        if (isMultiple != that.isMultiple) return false;
        if (isNullable != that.isNullable) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (values != null ? !values.equals(that.values) : that.values != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (isNullable ? 1 : 0);
        result = 31 * result + (isMultiple ? 1 : 0);
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }
}
