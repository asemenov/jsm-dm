package org.jsm.model.impl.property;

import org.jsm.model.IValue;

import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 06.04.14
 */
public class IntervalProperty extends AbstractProperty {
    private IValue norm;


    public IntervalProperty(String name, Collection<IValue> values, IValue norm) {
        this(name, false, false, values, norm);
    }

    public IntervalProperty(String name, boolean isNullable, boolean isMultiple, Collection<IValue> values, IValue norm) {
        super(name, isNullable, isMultiple, values);
        this.norm = norm;
    }

    public IValue getNorm() {
        return norm;
    }
}
