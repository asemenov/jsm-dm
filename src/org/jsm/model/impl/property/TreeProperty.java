package org.jsm.model.impl.property;

import org.jsm.model.IProperty;
import org.jsm.model.ITreeProperty;
import org.jsm.model.IValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.jsm.common.Utils.nvl;

/**
 * User: Anatoly Semenov
 * Date: 06.04.14
 */
public class TreeProperty extends AbstractProperty implements ITreeProperty{
    private Collection<? extends IProperty> children;
    private ITreeProperty parent;

    public TreeProperty(String name) {
        super(name, true, true, Collections.<IValue>emptyList());
    }

    public TreeProperty(String name, boolean isNullable, boolean isMultiple, Collection<? extends IProperty> children,
                        Collection<IValue> ownValues) {
        super(name, isNullable, isMultiple, ownValues);
        this.children = nvl(children, new ArrayList<IProperty>());
    }

    /**
     * Return collection of tree items l->r;
     *
     * */

    @Override
    public Collection<IValue> getPropertyValues() {
        List<IValue> values = new ArrayList<>(this.values);
//        for (IProperty childProperty : children)
//            values.addAll(childProperty.getPropertyValues());
        return values;
    }

    @Override
    public Collection<? extends IProperty> getChildren() {
        return children;
    }

    public void addValue(IValue value) {
        Collection<IValue> values = new ArrayList<>(this.values);
        values.add(value);
        this.values = values;
    }

    public void addProperty(IProperty iTreeProperty) {
        Collection<IProperty> values = new ArrayList<>(children);
        values.add(iTreeProperty);
        this.children = values;
    }

    public ITreeProperty getParent() {
        return parent;
    }

    public void setParent(ITreeProperty parent) {
        this.parent = parent;
    }

    public static class TreePropertyBuilder implements ITreePropertyBuilder {
        private String name;
        private ITreeProperty parent;
        private Collection<IProperty> child;
        private Collection<IValue> values;
        private boolean isMultiple = true;
        private boolean isNullable = true;


        public TreePropertyBuilder(ITreeProperty iTreeProperty) {
            this.name = iTreeProperty.getName();
            this.parent = iTreeProperty.getParent();
            //noinspection unchecked
            this.child = nvl((Collection<IProperty>)iTreeProperty.getChildren(), new ArrayList<IProperty>());
            this.values = nvl(iTreeProperty.getPropertyValues(), new ArrayList<IValue>());
            this.isMultiple = iTreeProperty.isMultiple();
            this.isNullable = iTreeProperty.isNullable();
        }

        public TreePropertyBuilder() {}

        @Override
        public ITreePropertyBuilder setParent(ITreeProperty parent) {
            this.parent = parent;
            return this;
        }

        @Override
        public ITreePropertyBuilder addProperty(IProperty property) {
            child = nvl(child, new ArrayList<IProperty>());
            child.add(property);
            return this;
        }

        @Override
        public ITreePropertyBuilder setProperties(Collection<IProperty> properties) {
            child = nvl(properties, new ArrayList<IProperty>());
            return this;
        }

        @Override
        public ITreeProperty build() {
            TreeProperty treeProperty = new TreeProperty(name, isNullable, isMultiple, child, values);
            treeProperty.setParent(parent);
            return treeProperty;
        }

        @Override
        public ITreePropertyBuilder setName(String name) {
            this.name = name;
            return this;
        }

        @Override
        public ITreePropertyBuilder setNullable(boolean nullable) {
            this.isNullable = nullable;
            return this;
        }

        @Override
        public ITreePropertyBuilder setMultiple(boolean multiple) {
            this.isMultiple = multiple;
            return this;
        }

        @Override
        public ITreePropertyBuilder setPropertyValues(Collection<IValue> values) {
            this.values = nvl(values, new ArrayList<IValue>());
            return this;
        }

        @Override
        public ITreePropertyBuilder addPropertyValue(IValue value) {
            values = nvl(values, new ArrayList<IValue>());
            values.add(value);
            return this;
        }
    }

    public static ITreePropertyBuilder builder() {
        return new TreePropertyBuilder();
    }

    public static ITreePropertyBuilder builder(ITreeProperty iTreeProperty) {
        return new TreePropertyBuilder(iTreeProperty);
    }
}
