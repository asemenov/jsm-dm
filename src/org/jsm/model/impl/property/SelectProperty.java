package org.jsm.model.impl.property;

import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.common.Utils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 06.04.14
 */
public class SelectProperty extends AbstractProperty {

    public SelectProperty(String name) {
        super(name);
    }

    public SelectProperty(String name, boolean isNullable, boolean isMultiple, Collection<IValue> values) {
        super(name, isNullable, isMultiple, values);
    }

    public static class SelectPropertyBuilder implements IPropertyBuilder {
        private String name;
        private boolean nullable;
        private boolean multiple;
        private Collection<IValue> values = new ArrayList<>();

        public SelectPropertyBuilder(SelectProperty property) {
            this.name = property.getName();
            this.nullable = property.isNullable();
            this.multiple = property.isMultiple();
            this.values = Utils.nvl(property.getPropertyValues(), new ArrayList<IValue>());
        }

        public SelectPropertyBuilder() {}

        @Override
        public IProperty build() {
            return new SelectProperty(name, nullable, multiple, values);
        }

        @Override
        public IPropertyBuilder setName(String name) {
            this.name = name;
            return this;
        }

        @Override
        public IPropertyBuilder setNullable(boolean nullable) {
            this.nullable = nullable;
            return this;
        }

        @Override
        public IPropertyBuilder setMultiple(boolean multiple) {
            this.multiple = multiple;
            return this;
        }

        @Override
        public IPropertyBuilder setPropertyValues(Collection<IValue> values) {
            this.values = Utils.nvl(values, new ArrayList<IValue>());
            return this;
        }

        @Override
        public IPropertyBuilder addPropertyValue(IValue value) {
            values = new ArrayList<>(values);
            values.add(value);
            return this;
        }
    }

    @Override
    public String toString() {
        return "SelectProperty{" +
                    "name = \'" + name + "\', " +
//                    "isMultiple = " + isMultiple + ", " +
//                    "isNullable = " + isNullable + ", " +
//                    "values = " + values +
                "}";
    }
}
