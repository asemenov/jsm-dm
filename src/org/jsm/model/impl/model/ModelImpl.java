package org.jsm.model.impl.model;

import org.jsm.model.IModel;
import org.jsm.model.IProperty;

import java.io.Serializable;
import java.util.Collection;

/**
 * User: Anatoly Semenov
 * Date: 06.04.14
 */
public class ModelImpl implements IModel, Serializable {
    private String name;
    private Collection<IProperty> properties;

    public ModelImpl(String name, Collection<IProperty> properties) {
        this.name = name;
        this.properties = properties;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Collection<IProperty> getProperties() {
        return properties;
    }
}
