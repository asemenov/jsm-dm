package org.jsm.transform;

/**
 * User: Anatoly Semenov
 * Date: 06.04.14
 */

/**
 * Convert from <code>F</code> type to <code>T</code>
 * */
public interface Transformer<F, T> {
    public T transform(F input);
}
