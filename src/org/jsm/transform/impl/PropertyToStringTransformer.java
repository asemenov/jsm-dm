package org.jsm.transform.impl;

import org.jsm.model.IProperty;
import org.jsm.model.ITreeProperty;
import org.jsm.model.IValue;
import org.jsm.model.impl.property.IntervalProperty;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.transform.Transformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Anatoly Semenov
 * Date: 23.04.14
 */
public class PropertyToStringTransformer implements Transformer<IProperty, String> {
    private static final Logger log = LoggerFactory.getLogger(PropertyToStringTransformer.class);
    private static final String NULLABLE_STRING_VALUE = "Value=(+-t)";
    private static final String NON_NULLABLE_STRING_VALUE = "Value=(+-)";

    @Override
    public String transform(IProperty input) {
        StringBuilder stringBuilder = new StringBuilder();
        if (input instanceof SelectProperty || input instanceof IntervalProperty) {
            stringBuilder.append("\t").append("Set {").append(input.getName()).append("} ")
                    .append(input.isNullable() ? NULLABLE_STRING_VALUE : NON_NULLABLE_STRING_VALUE).append("\t")
                    .append(input.isMultiple() ? "Multi" : "Single").append("Select").append("\n");
            String offset = "\t\t";
            for (IValue possibleValue : input.getPropertyValues())
                stringBuilder.append(offset).append("ItemSet {").append(possibleValue.getValue()).append("}\n");
            stringBuilder.append("\tEndSet");
        }
        else if (input instanceof ITreeProperty) {
            stringBuilder.append("\t").append("GroupTree {").append(input.getName()).append("} ")
                    .append(input.isNullable() ? NULLABLE_STRING_VALUE : NON_NULLABLE_STRING_VALUE).append("\t")
                    .append(input.isMultiple() ? "Multi" : "Single").append("Select").append("\n");
            String offset = "\t\t";
            for (IValue ownValue : input.getPropertyValues())
                stringBuilder.append(offset).append("ItemTree {").append(ownValue.getValue()).append("}").append("\n");
            for (IProperty iProperty : ((ITreeProperty)input).getChildren())
                stringBuilder.append(treePropertyInternal(iProperty, offset));
            stringBuilder.append("\tEndGroupTree");
        }
        else {
            log.warn("Unsupported property {} to transform", input.getClass().getName());
        }
        return stringBuilder.toString();
    }

    private String treePropertyInternal(IProperty iProperty, String offset) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(offset).append("Tree {").append(iProperty.getName()).append("}\n");
        String incOffset = offset + "\t";
        for (IValue ownValue : iProperty.getPropertyValues())
                stringBuilder.append(incOffset).append("ItemTree {").append(ownValue.getValue()).append("}").append("\n");
        if (iProperty instanceof ITreeProperty)
            for (IProperty i : ((ITreeProperty)iProperty).getChildren())
                stringBuilder.append(treePropertyInternal(i, incOffset));
        stringBuilder.append(offset).append("EndTree\n");
        return stringBuilder.toString();
    }
}
