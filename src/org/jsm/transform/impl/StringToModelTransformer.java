package org.jsm.transform.impl;

import org.jsm.model.IModel;
import org.jsm.parser.IModelParserFacade;
import org.jsm.transform.Transformer;

/**
 * User: Anatoly Semenov
 * Date: 08.04.14
 */
public class StringToModelTransformer implements Transformer<String, IModel> {
    @Override
    public IModel transform(String input) {
            return IModelParserFacade.parseModel(input);
    }
}
