package org.jsm.transform.impl;

import org.jsm.model.IModel;
import org.jsm.model.IProperty;
import org.jsm.transform.Transformer;

/**
 * User: Anatoly Semenov
 * Date: 06.04.14
 */
public class ModelToStringTransformer implements Transformer<IModel, String>{
    @Override
    public String transform(IModel input) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\\ ModelName {").append(input.getName()).append("}\n");
        PropertyToStringTransformer propertyToStringTransformer = new PropertyToStringTransformer();
        for (IProperty iProperty : input.getProperties())
            stringBuilder.append(propertyToStringTransformer.transform(iProperty)).append("\n");
        return stringBuilder.toString();
    }
}
