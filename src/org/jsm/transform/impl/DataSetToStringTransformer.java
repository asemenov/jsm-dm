package org.jsm.transform.impl;

import org.apache.commons.collections.CollectionUtils;
import org.jsm.common.Utils;
import org.jsm.model.*;
import org.jsm.transform.Transformer;

import java.util.*;

import static org.jsm.common.Utils.nvl;

/**
 * User: Anatoly Semenov
 * Date: 30.04.2014
 */
public class DataSetToStringTransformer implements Transformer<IDataSets, String> {
    @Override
    public String transform(IDataSets input) {
        IModel model = input.getModel();
        Collection<IDataSet> dataSets = input.getDataSets();
        StringBuilder builder = new StringBuilder();
        for (IDataSet dataSet : dataSets) {
            builder.append("Pacient {").append(dataSet.getName()).append("}\n");
            List<IProperty> modelProperties = new ArrayList<>(model.getProperties());
            Map<IProperty, IDataItem> _item = new HashMap<>(dataSet.getDataItems());
            //List<IDataItem> _item = new ArrayList<>(dataSet.getDataItems());
            builder.append(_internal(modelProperties, _item, dataSet.getComment()));
            builder.append("EndPacient\n");
        }
        return builder.toString();
    }

    private String _internal(final List<IProperty> properties, Map<IProperty, IDataItem> items, String comments) {
       /* Collections.sort(items, new Comparator<IDataItem>() {
            @Override
            public int compare(IDataItem o1, IDataItem o2) {
                return Integer.compare(properties.indexOf(o1.getIProperty()), properties.indexOf(o2.getIProperty()));
            }
        });*/
        String offset = "\t";
        StringBuilder builder = new StringBuilder();
        builder.append(offset).append("PComment {").append(nvl(comments, "")).append("}\n");

        Set<Map.Entry<IProperty, IDataItem>> set = items.entrySet();
        Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

        while(iterator.hasNext())
        {
            Map.Entry<IProperty, IDataItem> entry = iterator.next();

            builder.append(offset).append("PProp {").append(_values_internal(entry.getValue())).append("}\n");
        }

        /*for (IDataItem item : items) {
            builder.append(offset).append("PProp {").append(_values_internal(item)).append("}\n");
        }*/
        return builder.toString();
    }

    private String _values_internal(IDataItem dataItem) {
        StringBuilder out = new StringBuilder();
        IProperty iProperty = dataItem.getIProperty();
        Collection<IValue> values = iProperty.getPropertyValues();
        if (!(iProperty instanceof ITreeProperty)) {
            for (IValue iValue : values) {
                if (dataItem.getPositiveValues().contains(iValue))
                    out.append("+");
                else if (dataItem.getNegativeValues().contains(iValue))
                    out.append("-");
                else
                    out.append("t");
            }
        } else {
            out.append(_tree_property_transform(dataItem));
        }
        return out.toString();
    }

    private String _tree_property_transform(IDataItem iDataItem) {
        StringBuilder builder = new StringBuilder();
        ITreeProperty iTreeProperty = (ITreeProperty) iDataItem.getIProperty();
        return builder.append(_tree_internal(iTreeProperty, iDataItem)).toString();
    }

    private String _tree_internal(ITreeProperty parent, IDataItem item) {
        TreeNode parentNode = new TreeNode(parent.getName(), null, null, parent.getPropertyValues(), item.getPositiveValues(),
                item.getNegativeValues(), item.getUndefinedValues());
        parentNode.child = toTreeNodes(parent.getChildren(), item, parentNode);
        processTree(parentNode);
        return parentNode.toString();
    }

    private static Collection<TreeNode> toTreeNodes(Collection<? extends IProperty> properties, IDataItem dataItem,
                                                    TreeNode parentNode) {
        Collection<TreeNode> result = new ArrayList<>();
        for (IProperty iProperty : properties) {
            if (iProperty instanceof ITreeProperty) {
                ITreeProperty iTreeProperty = (ITreeProperty) iProperty;
                TreeNode child = new TreeNode(iProperty.getName(), parentNode, null, iTreeProperty.getPropertyValues(),
                        dataItem.getPositiveValues(), dataItem.getNegativeValues(), dataItem.getUndefinedValues());
                if (!Utils.isEmpty(iTreeProperty.getChildren()))
                    child.child = toTreeNodes(iTreeProperty.getChildren(), dataItem, child);
                result.add(child);
            }
            //todo make possible for Select and Interval
        }
        return result;
    }


    private static class TreeNode {
        private enum TreeNodeState {
            POSITIVE("+"), NEGATIVE("-"), UNDEF("t");

            private String ch;

            private TreeNodeState(String ch) {
                this.ch = ch;
            }

            private String getCh() {
                return ch;
            }
        }

        private TreeNode parent;
        private Collection<TreeNode> child;
        private Collection<IValue> values;
        private Collection<IValue> positiveValues;
        private Collection<IValue> negativeValues;
        private Collection<IValue> undefinedValues;
        private TreeNodeState state;
        private String name;


        private TreeNode(String name, TreeNode parent, Collection<TreeNode> child, Collection<IValue> values
                , Collection<IValue> positiveValues, Collection<IValue> negativeValues, Collection<IValue> undefinedValues
        ) {
            this.parent = parent;
            this.child = nvl(child, Collections.<TreeNode>emptyList());
            this.values = values;
            this.positiveValues = positiveValues;
            this.negativeValues = negativeValues;
            this.undefinedValues = undefinedValues;
            this.name = name;
        }

        @Override
        public String toString() {
            StringBuilder s = new StringBuilder("");
            if (parent != null)
                s = new StringBuilder(state.getCh()).append(s);
            for (IValue iValue : values) {
                if (positiveValues.contains(iValue))
                    s.append("+");
                else if (negativeValues.contains(iValue))
                    s.append("-");
                else
                    s.append("t");
            }
            for (TreeNode treeNode : child)
                s.append(treeNode.toString());
            return s.toString();
        }
    }

    private static void processTree(TreeNode node) {
        Collection<TreeNode> nodes = nvl(node.child, Collections.<TreeNode>emptyList());
        for (TreeNode child : nodes)
            processTree(child);
        if (Utils.isEmpty(nodes)) {
            if (CollectionUtils.containsAny(node.positiveValues, node.values))
                node.state = TreeNode.TreeNodeState.POSITIVE;
            else if (node.negativeValues.containsAll(node.values))
                node.state = TreeNode.TreeNodeState.NEGATIVE;
            else
                node.state = TreeNode.TreeNodeState.UNDEF;
        } else {
            boolean isHavePosChild = false;
            for (TreeNode n : nodes)
                if (TreeNode.TreeNodeState.POSITIVE.equals(n.state))
                    isHavePosChild = true;
            if (!isHavePosChild && CollectionUtils.containsAny(node.positiveValues, node.values)) {
                node.state = TreeNode.TreeNodeState.POSITIVE;
                isHavePosChild = true;
            }
            if (isHavePosChild) {
                node.state = TreeNode.TreeNodeState.POSITIVE;
                return;
            }
            boolean isAllChNegative = true;
            for (TreeNode n : nodes) {
                if (!TreeNode.TreeNodeState.NEGATIVE.equals(n.state)) {
                    isAllChNegative = false;
                    break;
                }
            }
            node.state = isAllChNegative && node.negativeValues.containsAll(node.values) ? TreeNode.TreeNodeState.NEGATIVE
                    : TreeNode.TreeNodeState.UNDEF;
        }
    }
}
