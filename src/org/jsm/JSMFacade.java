package org.jsm;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DelegateFileFilter;
import org.jsm.common.AppConfig;
import org.jsm.common.Utils;
import org.jsm.model.IModel;
import org.jsm.transform.impl.StringToModelTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;

/**
 * User: Anatoly Semenov
 * Date: 05.05.2014
 */
public class JSMFacade {
    private static final Logger log = LoggerFactory.getLogger(JSMFacade.class);

    public static IModel getEtalonModel() throws Exception {
        File dataDir = new File(AppConfig.getProperty("data_dir"));
        File etalonDir = new File(dataDir, "etalon");
        if (!etalonDir.exists())
            throw new IllegalStateException("Folder 'etalon' isnt exists");
        File fileForRead = null;
        File[] listFiles = etalonDir.listFiles();
        if (listFiles == null)
            throw new IllegalStateException("Folder 'etalon' is empty. Put model file to folder");
        for (File file : listFiles) {
            if (file.getName().startsWith("model"))
                fileForRead = file;
        }
        if (fileForRead == null)
            throw new IllegalStateException("File 'model_xxx' doesnt exist");
        String input = FileUtils.readFileToString(fileForRead);
        return new StringToModelTransformer().transform(input);
    }

    public static IModel getLastModel() throws Exception {
        File dataDir = new File(AppConfig.getProperty("data_dir"));
        if (!dataDir.exists())
            throw new IllegalStateException("Folder 'data' isnt exists");
        Collection fs = FileUtils.listFiles(dataDir, new DelegateFileFilter(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith("model_");
            }
        }), null);
        if (Utils.isEmpty(fs))
            throw new IllegalArgumentException("Files 'model_xxx' doesnt exists.");
        @SuppressWarnings("unchecked") ArrayList<File> list = new ArrayList<File>(fs);
        Collections.sort(list, new Comparator<File>() {
            @Override
            public int compare(File o1, File o2) {
                return (int) (o2.lastModified() - o1.lastModified());
            }
        });
        String input = FileUtils.readFileToString(list.get(0));
        return new StringToModelTransformer().transform(input);
    }
}
