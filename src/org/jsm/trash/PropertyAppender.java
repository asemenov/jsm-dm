package org.jsm.trash;

import org.apache.commons.io.FileUtils;
import org.jsm.common.AppConfig;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;
import org.jsm.transform.impl.PropertyToStringTransformer;
import org.jsm.common.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * User: Anatoly Semenov
 * Date: 23.04.14
 */
public class PropertyAppender {
    private static final Logger log = LoggerFactory.getLogger(PropertyAppender.class);
    private static PropertyAppender instance;
    private final File file;
    private static final String DATE_FORMAT = "yyyy_MM_dd_hh_mm_ss";

    private PropertyAppender(String name) {
        String dir = AppConfig.getProperty("data_dir");
        File dirF = new File(dir);
        if (!dirF.exists())
            //noinspection ResultOfMethodCallIgnored
            dirF.mkdirs();
        String fileNamePattern = AppConfig.getProperty("model_file");
        String currentDateFormat = new SimpleDateFormat(DATE_FORMAT).format(new Date());
        fileNamePattern = fileNamePattern.replace("{date}", currentDateFormat);
        file = new File(dirF, fileNamePattern);
        if (!file.exists())
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
                FileUtils.writeStringToFile(file, "\\ ModelName {" +
                        String.valueOf(Utils.nvl(name, currentDateFormat)) + "} \n");
            } catch (IOException e) {
                log.error("Cannt create file");
                throw new RuntimeException(e);
            }
    }

    public static PropertyAppender instance() {
        return instance(null);
    }

    public static PropertyAppender instance(String name) {
        if (instance == null) {
            instance = new PropertyAppender(name);
        }
        return instance;
    }

    public void append(IProperty iProperty) {
        String propertyAsString = new PropertyToStringTransformer().transform(iProperty);
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)))) {
            out.println(propertyAsString);
        }catch (IOException e) {
            log.error("Exception append", e);
        }
    }

    public static void main(String[] args) throws IOException {
        PropertyAppender.instance().append(new SelectProperty("test property", false, true, Arrays.<IValue>asList(
                new StringValue("a"), new StringValue("c"), new StringValue("b"))));

        PropertyAppender.instance().append(new SelectProperty("test property 2", false, true, Arrays.<IValue>asList(
                new StringValue("d"), new StringValue("e"), new StringValue("f"))));

        PropertyAppender.instance().append(new SelectProperty("test property 3", false, true, null));
    }
}
