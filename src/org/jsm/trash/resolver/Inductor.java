package org.jsm.trash.resolver;

import org.jsm.model.IDataSets;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;

/**
 * @author Anatoly Semenov
 *         Date: 24.05.2015
 */
public interface Inductor
{
    public InductionResult induction(IDataSets iDataSets, IProperty targetProperty, IValue targetValue);
}
