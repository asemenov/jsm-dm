package org.jsm.trash.resolver.impl;

import org.jsm.model.IDataItem;
import org.jsm.model.IProperty;

/**
 * Created by Данил on 06.06.2015.
 */
public class Fact {
    public Fact(IProperty property, IDataItem iDataItem) {
        this.property = property;
        this.iDataItem = iDataItem;
        //this.iDataSets = iDataSets;
    }

    public IDataItem getIDataItem() {
        return iDataItem;
    }

    /*public IDataSets getIDataSets()
    {
        return iDataSets;
    }*/

    public static enum TypeFact {
        POSITIVE, NEGATIVE
    }

    public IProperty getIProperty() {
        return property;
    }

    /*public void addDataSet(IDataSet iDataSet)
    {
        iDataSets.add(iDataSet);
    }*/

    private IProperty property;
    private IDataItem iDataItem;
    // private  IDataSets iDataSets;
    // private Collection<IDataSet> iDataSets;
}
