package org.jsm.trash.resolver.impl;

import org.jsm.model.IDataItem;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;

import java.util.Map;

/**
 * Created by Данил on 06.06.2015.
 */
public class Hypothesis {
    public Hypothesis(Map<IProperty, IDataItem> sources, Type type, IProperty targetProperty, IValue targetValue) {
        this.sources = sources;
        this.targetProperty = targetProperty;
        this.targetValue = targetValue;
        this.type = type;
    }

    public Map<IProperty, IDataItem> getSources() {
        return sources;
    }

    public static enum Type {
        POSITIVE, NEGATIVE, UNDEFINED
    }

    private Map<IProperty, IDataItem> sources;
    private final IProperty targetProperty;
    private final IValue targetValue;
    private final Type type;
}
