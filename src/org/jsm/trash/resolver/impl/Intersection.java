package org.jsm.trash.resolver.impl;

import org.jsm.model.IDataItem;
import org.jsm.model.IDataSet;
import org.jsm.model.IDataSets;
import org.jsm.model.IProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.Map.Entry;

/**
 * Created by Данил on 07.06.2015.
 */
public class Intersection {
    public Intersection(IDataSets iDataSets) {
        this.iDataSets = iDataSets;

        log.info("New Facts");

        ArrayList<IDataSet> dataSets = (ArrayList<IDataSet>) iDataSets.getDataSets();

        currentFacts = new ArrayList<>();

        Map<IProperty, IDataItem> iDataItems = dataSets.get(0).getDataItems();

        Set<Entry<IProperty, IDataItem>> set = iDataItems.entrySet();
        Iterator<Entry<IProperty, IDataItem>> iterator = set.iterator();

        fact = new LinkedHashMap<IDataSets, Collection<Fact>>();

        while (iterator.hasNext()) {
            boolean isEquals = true;

            Entry<IProperty, IDataItem> entry = iterator.next();

            for (int i = 1; i < dataSets.size(); i++) {
                if (!dataSets.get(i).getDataItems().get(entry.getKey()).equals(entry.getValue())) {
                    isEquals = false;

                    break;
                }
            }

            if (isEquals) {
                currentFacts.add(new Fact(entry.getKey(), entry.getValue()));

                log.info("Fact " + entry.getKey().getName());
            }
        }

        fact.put(iDataSets, currentFacts);
    }

    public Map<IDataSets, Collection<Fact>> getFacts() {
        return fact;
    }

    private IDataSets iDataSets;
    private static final Logger log = LoggerFactory.getLogger(Intersection.class);
    private Map<IDataSets, Collection<Fact>> fact;
    Collection<Fact> currentFacts;
}
