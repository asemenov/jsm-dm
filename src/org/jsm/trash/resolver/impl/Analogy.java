package org.jsm.trash.resolver.impl;

import org.jsm.model.*;
import org.jsm.model.impl.data.DataSets;

import java.util.*;

/**
 * Created by Данил on 08.06.2015.
 */
public class Analogy

{

    public static IDataSets result(BaseFacts undefinedContradictoryBaseFacts, List<Hypothesis>

            positiveHypothesis, List<Hypothesis> negativeHypothesis, IProperty targetProperty, IValue

                                           targetValue)

    {

        IDataSets currentIDataSets = new

                DataSets(undefinedContradictoryBaseFacts.getDataSets().getModel(),

                undefinedContradictoryBaseFacts.getDataSets().getDataSets());

        Hypothesis currentHypothesis = null;

        boolean isSuccess = false;

        for (IDataSet aDataSet : currentIDataSets.getDataSets())

        {

            for (Hypothesis aHypothesis : positiveHypothesis)

            {

                if ((currentHypothesis == null) || (aHypothesis.getSources().size() >

                        currentHypothesis.getSources().size()))

                {

                    Set<Map.Entry<IProperty, IDataItem>> hypothesisSet =

                            aHypothesis.getSources().entrySet();

                    Iterator<Map.Entry<IProperty, IDataItem>> hypothesisIterator =

                            hypothesisSet.iterator();

                    boolean isTrue = true;

                    while (hypothesisIterator.hasNext())

                    {

                        Map.Entry<IProperty, IDataItem> entry = hypothesisIterator.next();

                        if (!aDataSet.getDataItems().get(entry.getKey()).equals(entry.getValue()))

                        {

                            isTrue = false;

                            break;

                        }

                    }

                    if (isTrue)

                    {

                        currentHypothesis = aHypothesis;

                    }

                }

            }

            if (currentHypothesis != null)

            {


                aDataSet.getDataItems().get(targetProperty).getUndefinedValues().remove(targetValue);

                aDataSet.getDataItems().get(targetProperty).getPositiveValues().add(targetValue);

            } else

            {

                for (Hypothesis aHypothesis : negativeHypothesis)

                {

                    if ((currentHypothesis == null) || (aHypothesis.getSources().size() >

                            currentHypothesis.getSources().size()))

                    {

                        Set<Map.Entry<IProperty, IDataItem>> hypothesisSet =

                                aHypothesis.getSources().entrySet();

                        Iterator<Map.Entry<IProperty, IDataItem>> hypothesisIterator =

                                hypothesisSet.iterator();

                        boolean isTrue = true;

                        while (hypothesisIterator.hasNext())

                        {

                            Map.Entry<IProperty, IDataItem> entry = hypothesisIterator.next();

                            if (!aDataSet.getDataItems().get(entry.getKey()).equals(entry.getValue()))

                            {

                                isTrue = false;

                                break;

                            }

                        }

                        if (isTrue)

                        {

                            currentHypothesis = aHypothesis;

                        }

                    }

                }

                if (currentHypothesis != null)

                {

                    aDataSet.getDataItems().get(targetProperty).getUndefinedValues().remove(targetValue);

                    aDataSet.getDataItems().get(targetProperty).getNegativeValues().add(targetValue);

                }

            }

        }

        return currentIDataSets;

    }

}
