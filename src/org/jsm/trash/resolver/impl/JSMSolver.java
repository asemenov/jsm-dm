package org.jsm.trash.resolver.impl;

import com.google.common.collect.Lists;
import org.jsm.model.*;
import org.jsm.model.impl.data.DataSet;
import org.jsm.model.impl.data.DataSets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by Данил on 06.06.2015.
 */
public class JSMSolver {
    public JSMSolver(IDataSets iDataSets, IDataSets positiveDataSets, IDataSets negativeDataSets, IProperty targetProperty, IValue targetValue) {
        this.mainIDataSets = iDataSets;
        this.targetProperty = targetProperty;
        this.targetValue = targetValue;

        IDataSets currentIDataSets;
        IDataSets previousIDataSets;

        currentIDataSets = new DataSets(mainIDataSets.getModel(), new ArrayList<>());

        currentIDataSets.getDataSets().addAll(positiveDataSets.getDataSets());
        currentIDataSets.getDataSets().addAll(negativeDataSets.getDataSets());

        for (IDataSet iDataSet : mainIDataSets.getDataSets()) {
            if (iDataSet.getDataItems().get(targetProperty).getUndefinedValues().size() > 0) {
                for (IValue value : iDataSet.getDataItems().get(targetProperty).getUndefinedValues()) {
                    if (value.equals(targetValue)) {
                        currentIDataSets.getDataSets().add(iDataSet);

                        break;
                    }
                }
            }
        }

        boolean isNotEquals;

        currentIDataSets = mainIDataSets;

        // do
        //{
        /**
         * Создание банков данных
         */

        positiveBaseFacts = new BaseFacts(currentIDataSets.getModel());
        negativeBaseFacts = new BaseFacts(currentIDataSets.getModel());
        undefinedContradictoryBaseFacts = new BaseFacts(currentIDataSets.getModel());

        for (IDataSet iDataSet : currentIDataSets.getDataSets()) {
            boolean isTrue = false;

            if (iDataSet.getDataItems().get(targetProperty).getPositiveValues().size() > 0) {
                for (IValue value : iDataSet.getDataItems().get(targetProperty).getPositiveValues()) {
                    if (value.equals(targetValue)) {
                        positiveBaseFacts.addDataSet(iDataSet);

                        isTrue = true;

                        break;
                    }
                }
            }

            if (!isTrue) {
                if (iDataSet.getDataItems().get(targetProperty).getNegativeValues().size() > 0) {
                    for (IValue value : iDataSet.getDataItems().get(targetProperty).getNegativeValues()) {
                        if (value.equals(targetValue)) {
                            negativeBaseFacts.addDataSet(iDataSet);

                            isTrue = true;

                            break;
                        }
                    }
                }
            }

            if (!isTrue) {
                if (iDataSet.getDataItems().get(targetProperty).getUndefinedValues().size() > 0) {
                    for (IValue value : iDataSet.getDataItems().get(targetProperty).getUndefinedValues()) {
                        if (value.equals(targetValue)) {
                            undefinedContradictoryBaseFacts.addDataSet(iDataSet);

                            isTrue = true;

                            break;
                        }
                    }
                }
            }
        }

        positiveHypothesis = Lists.newArrayList();
        negativeHypothesis = Lists.newArrayList();

        List<Hypothesis> positiveHypothesisWithCounterexamples;
        List<Hypothesis> negativeHypothesisWithCounterexamples;

        positiveHypothesisWithCounterexamples = (ArrayList<Hypothesis>) Inductor.result(positiveBaseFacts, negativeBaseFacts, Hypothesis.Type.POSITIVE, targetProperty, targetValue);
        negativeHypothesisWithCounterexamples = (ArrayList<Hypothesis>) Inductor.result(negativeBaseFacts, positiveBaseFacts, Hypothesis.Type.NEGATIVE, targetProperty, targetValue);

        negativeHypothesis = negativeHypothesisWithCounterexamples;

        /**
         * Запрет на контрпримеры
         */

        for (Hypothesis aPositiveHypothesis : positiveHypothesisWithCounterexamples) {
            boolean isCounterexamples = false;

            for (Hypothesis aNegativeHypothesis : negativeHypothesisWithCounterexamples) {
                if (aPositiveHypothesis.getSources().equals(aNegativeHypothesis.getSources())) {
                    isCounterexamples = true;

                    negativeHypothesis.remove(aNegativeHypothesis);

                    break;
                }
            }

            if (!isCounterexamples) {
                positiveHypothesis.add(aPositiveHypothesis);
            }
        }

        positiveHypothesis = Inductor.cleaning(positiveHypothesis, negativeBaseFacts);

        previousIDataSets = currentIDataSets;

        currentIDataSets = Analogy.result(undefinedContradictoryBaseFacts, positiveHypothesis, negativeHypothesis, targetProperty, targetValue);

        isNotEquals = false;

        for (IDataSet aCurrentDataSet : currentIDataSets.getDataSets()) {
            for (IDataSet aPreviousDataSet : previousIDataSets.getDataSets()) {
                if (!aCurrentDataSet.getDataItems().equals(aPreviousDataSet.getDataItems())) {
                    isNotEquals = true;

                    break;
                }
            }
        }
        //}
        //while(isNotEquals);

        //positiveHypothesis = Abductor.result(mainIDataSets, positiveHypothesis, targetProperty, targetValue, Hypothesis.Type.POSITIVE);
        //negativeHypothesis = Abductor.result(mainIDataSets, negativeHypothesis, targetProperty, targetValue, Hypothesis.Type.NEGATIVE);

        newDataSets = new DataSets(mainIDataSets.getModel(), new ArrayList<>());

        for (IDataSet iDataSet : mainIDataSets.getDataSets()) {
            if (iDataSet.getDataItems().get(targetProperty).getUndefinedValues().size() > 0) {
                for (IValue value : iDataSet.getDataItems().get(targetProperty).getUndefinedValues()) {
                    if (value.equals(targetValue)) {
                        IDataSet dataSet = new DataSet(iDataSet.getModel(), iDataSet.getDataItems(), iDataSet.getName(), iDataSet.getComment());

                        boolean isTrue = true;

                        for (Hypothesis aHypothesis : positiveHypothesis) {
                            Set<Map.Entry<IProperty, IDataItem>> set = aHypothesis.getSources().entrySet();
                            Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                            while (iterator.hasNext()) {
                                Map.Entry<IProperty, IDataItem> entry = iterator.next();

                                if (!entry.getValue().equals(iDataSet.getDataItems().get(entry.getKey()))) {
                                    isTrue = false;

                                    break;
                                }
                            }


                            if (isTrue) {
                                dataSet.getDataItems().get(targetProperty).getPositiveValues().add(targetValue);
                                dataSet.getDataItems().get(targetProperty).getUndefinedValues().remove(targetValue);

                                break;
                            }
                        }

                        if (!isTrue) {
                            for (Hypothesis aHypothesis : negativeHypothesis) {
                                Set<Map.Entry<IProperty, IDataItem>> set = aHypothesis.getSources().entrySet();
                                Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                                while (iterator.hasNext()) {
                                    Map.Entry<IProperty, IDataItem> entry = iterator.next();

                                    if (!entry.getValue().equals(iDataSet.getDataItems().get(entry.getKey()))) {
                                        isTrue = false;

                                        break;
                                    }
                                }

                                if (isTrue) {
                                    dataSet.getDataItems().get(targetProperty).getNegativeValues().add(targetValue);
                                    dataSet.getDataItems().get(targetProperty).getUndefinedValues().remove(targetValue);

                                    break;
                                }
                            }

                        }

                        newDataSets = undefinedContradictoryBaseFacts.getDataSets();

                        log.info(newDataSets.getDataSets().size() + "");

                        break;
                    }
                }
            }
        }


    }

    public List<Hypothesis> getPositiveHypothesis() {
        return positiveHypothesis;
    }

    public List<Hypothesis> getNegativeHypothesis() {
        return negativeHypothesis;
    }

    public IDataSets getNewDataSets() {
        return newDataSets;
    }

    private IDataSets mainIDataSets;
    private IProperty targetProperty;
    private IValue targetValue;
    private List<Hypothesis> positiveHypothesis;
    private List<Hypothesis> negativeHypothesis;
    private IDataSets newDataSets;
    private BaseFacts positiveBaseFacts;
    private BaseFacts negativeBaseFacts;
    private BaseFacts undefinedContradictoryBaseFacts;
    private Inductor inductor;
    private static final Logger log = LoggerFactory.getLogger(JSMSolver.class);
}
