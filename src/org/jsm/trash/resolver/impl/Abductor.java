package org.jsm.trash.resolver.impl;

import org.jsm.model.*;

import java.util.*;

/**
 * Created by Данил on 15.06.2015.
 */
public class Abductor

{

    public static List<Hypothesis> result(IDataSets iDataSets, List<Hypothesis> hypothesis,

                                          IProperty targetProperty, IValue targetValue, Hypothesis.Type type)

    {

        List<Hypothesis> newHypothesis = new ArrayList<Hypothesis>();

        BaseFacts positiveBaseFacts = new BaseFacts(iDataSets.getModel());

        BaseFacts negativeBaseFacts = new BaseFacts(iDataSets.getModel());

        BaseFacts undefinedContradictoryBaseFacts = new BaseFacts(iDataSets.getModel());

        for (IDataSet iDataSet : iDataSets.getDataSets())

        {

            boolean isTrue = false;

            if (iDataSet.getDataItems().get(targetProperty).getPositiveValues().size() > 0)

            {

                for (IValue value : iDataSet.getDataItems().get(targetProperty).getPositiveValues())

                {

                    if (value.equals(targetValue))

                    {

                        positiveBaseFacts.addDataSet(iDataSet);

                        isTrue = true;

                        break;

                    }

                }

            }

            if (!isTrue)

            {

                if (iDataSet.getDataItems().get(targetProperty).getNegativeValues().size() > 0)

                {

                    for (IValue value : iDataSet.getDataItems().get(targetProperty).getNegativeValues())

                    {

                        if (value.equals(targetValue))

                        {

                            negativeBaseFacts.addDataSet(iDataSet);

                            isTrue = true;

                            break;

                        }

                    }

                }

            }

            if (!isTrue)

            {

                if (iDataSet.getDataItems().get(targetProperty).getUndefinedValues().size() > 0)

                {

                    for (IValue value :

                            iDataSet.getDataItems().get(targetProperty).getUndefinedValues())

                    {

                        if (value.equals(targetValue))

                        {

                            undefinedContradictoryBaseFacts.addDataSet(iDataSet);

                            isTrue = true;

                            break;

                        }

                    }

                }

            }

        }

        if (type.equals(Hypothesis.Type.POSITIVE))

        {

            for (Hypothesis aHypothesis : hypothesis)

            {

                boolean isTrue = true;

                for (IDataSet iDataSet : positiveBaseFacts.getDataSets().getDataSets())

                {

                    Set<Map.Entry<IProperty, IDataItem>> set = aHypothesis.getSources().entrySet();

                    Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                    while (iterator.hasNext())

                    {

                        Map.Entry<IProperty, IDataItem> entry = iterator.next();

                        if (!entry.getValue().equals(iDataSet.getDataItems().get(entry.getKey())))

                        {

                            isTrue = false;

                            break;

                        }

                    }

                    if (!isTrue)

                    {

                        break;

                    }

                }

                if (isTrue)

                {

                    newHypothesis.add(aHypothesis);

                }

            }

        }

        if (type.equals(Hypothesis.Type.NEGATIVE))

        {

            for (Hypothesis aHypothesis : hypothesis)

            {

                boolean isTrue = true;

                for (IDataSet iDataSet : negativeBaseFacts.getDataSets().getDataSets())

                {

                    Set<Map.Entry<IProperty, IDataItem>> set = aHypothesis.getSources().entrySet();

                    Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                    while (iterator.hasNext())

                    {

                        Map.Entry<IProperty, IDataItem> entry = iterator.next();

                        if (!entry.getValue().equals(iDataSet.getDataItems().get(entry.getKey())))

                        {

                            isTrue = false;

                            break;

                        }

                    }

                    if (!isTrue)

                    {

                        break;

                    }

                }

                if (isTrue)

                {

                    newHypothesis.add(aHypothesis);

                }

            }

        }

        return newHypothesis;

    }

}
