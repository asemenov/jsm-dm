package org.jsm.trash.resolver.impl;

import org.jsm.model.IDataSets;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.trash.resolver.InductionResult;
import org.jsm.trash.resolver.Inductor;

/**
 * @author Anatoly Semenov
 *         Date: 24.05.2015
 */
public class InductorImpl implements Inductor {
    @Override
    public InductionResult induction(IDataSets iDataSets, IProperty targetProperty, IValue targetValue) {
        return null;
    }
}
