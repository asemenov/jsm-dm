package org.jsm.trash.resolver.impl;

import org.jsm.model.IDataSet;
import org.jsm.model.IProperty;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Данил on 06.06.2015.
 */
public class StructureFragment {
    public StructureFragment(int id) {
        this.id = id;

        iProperties = new ArrayList<IProperty>();
        iDataSets = new ArrayList<IDataSet>();
    }

    public StructureFragment(int id, Collection<IProperty> iProperties, Collection<IDataSet> iDataSets) {
        this.id = id;

        this.iProperties = new ArrayList<IProperty>(iProperties);
        this.iDataSets = new ArrayList<IDataSet>(iDataSets);
    }

    public void addIProperty(IProperty iProperty) {
        iProperties.add(iProperty);
    }

    public void addIDataSet(IDataSet iDataSet) {
        iDataSets.add(iDataSet);
    }

    public int getId() {
        return id;
    }

    public Collection<IProperty> getIProperties() {
        return iProperties;
    }

    public Collection<IDataSet> getIDataSets() {
        return iDataSets;
    }

    public void addAllIProperties(Collection<IDataSet> iDataSets) {
        this.iDataSets.addAll(iDataSets);
    }

    public int id;
    public Collection<IProperty> iProperties;
    public Collection<IDataSet> iDataSets;
}
