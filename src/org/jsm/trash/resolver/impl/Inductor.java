package org.jsm.trash.resolver.impl;

import org.jsm.model.*;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.model.ModelImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class Inductor

{

    static private List<Hypothesis> createHypothesis(BaseFacts baseFacts, Hypothesis.Type type,

                                                     IProperty targetProperty, IValue targetValue)

    {

        List<Hypothesis> hypothesis = new ArrayList<Hypothesis>();

        IDataSets subset;


        IDataSets dataSets = new DataSets(new

                ModelImpl(baseFacts.getDataSets().getModel().getName(),

                baseFacts.getDataSets().getModel().getProperties()), baseFacts.getDataSets().getDataSets());

        log.info(dataSets.getModel().getProperties().size() + "a ");

        int N = dataSets.getDataSets().size() + 1;

        int[] b = new int[N + 1];

        for (int i = 0; i < N; i++)

        {

            b[i] = 0;

        }

        int i = 1, j = 1, k = 0, k1 = 1;

        while (b[N] != 1)

        {

            subset = new DataSets(dataSets.getModel(), new ArrayList<IDataSet>());

            i = 1;

            while (b[i] == 1)

            {

                b[i++] = 0;

            }

            b[i] = 1;


            for (i = 0; i < N; i++)

                if (b[i] == 1)

                {

                    subset.getDataSets().add(((ArrayList<IDataSet>)

                            baseFacts.getDataSets().getDataSets()).get(i - 1));


                }

            if (subset.getDataSets().size() > 1)

            {

                String out = "";

                for (IDataSet t : subset.getDataSets())

                {

                    out = out + t.getName() + " ";

                }

                log.info("vy " + out);

                Hypothesis currentHypothesis = intersection(subset, type, targetProperty, targetValue);

                boolean isEquals = false;

                for (Hypothesis aHypothesis : hypothesis)

                {

                    if (aHypothesis.getSources().equals(currentHypothesis.getSources()))

                    {

                        isEquals = true;

                        break;

                    }

                }

                if (!isEquals)

                {

                    hypothesis.add(currentHypothesis);

                }

            }

        }

        return hypothesis;

    }

    public static Hypothesis intersection(IDataSets iDataSets, Hypothesis.Type type, IProperty

            targetProperty, IValue targetValue)

    {

        Map<IProperty, IDataItem> sources = new LinkedHashMap<IProperty, IDataItem>();

        ArrayList<IDataSet> dataSets = (ArrayList<IDataSet>) iDataSets.getDataSets();


        Map<IProperty, IDataItem> iDataItems = dataSets.get(0).getDataItems();

        Set<Map.Entry<IProperty, IDataItem>> set = iDataItems.entrySet();

        Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();


        while (iterator.hasNext())

        {

            boolean isEquals = true;

            Map.Entry<IProperty, IDataItem> entry = iterator.next();

            for (int i = 1; i < dataSets.size(); i++)

            {

                if (!dataSets.get(i).getDataItems().get(entry.getKey()).equals(entry.getValue()))

                {

                    isEquals = false;

                    break;

                }

            }

            if (isEquals)

            {

                if ((!entry.getKey().equals(targetProperty)))

                {

                    sources.put(entry.getKey(), entry.getValue());

                }

                log.info("Fact " + entry.getKey().getName());

            }

        }

        return new Hypothesis(sources, type, targetProperty, targetValue);

    }

    public static List<Hypothesis> result(BaseFacts baseFacts, BaseFacts oppositeBaseFact,

                                          Hypothesis.Type type, IProperty targetProperty, IValue targetValue)

    {

        List<Hypothesis> hypothesis = createHypothesis(baseFacts, type, targetProperty,

                targetValue);

        return hypothesis;

    }

    public static List<Hypothesis> cleaning(List<Hypothesis> hypothesisWithoutCleaning,

                                            BaseFacts oppositeBaseFact)

    {

        List<Hypothesis> hypothesis = new ArrayList<>(hypothesisWithoutCleaning);

        for (IDataSet aDataSet : oppositeBaseFact.getDataSets().getDataSets())

        {

            log.info("В " + aDataSet.getName());

            for (Hypothesis aHypothesis : hypothesisWithoutCleaning)

            {

                boolean isEquals = true;

                Set<Map.Entry<IProperty, IDataItem>> set = aHypothesis.getSources().entrySet();

                Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                while (iterator.hasNext())

                {

                    Map.Entry<IProperty, IDataItem> entry = iterator.next();

                    if (!aDataSet.getDataItems().get(entry.getKey()).equals(entry.getValue()))

                    {

                        isEquals = false;

                        break;

                    }

                }

                if (isEquals)

                {

                    hypothesis.remove(aHypothesis);

                }

            }

        }

        return hypothesis;

    }

    public List<Hypothesis> getHypothesis()

    {

        return hypothesis;

    }

    private BaseFacts positiveBaseFacts;

    private BaseFacts negativeBaseFacts;

    private BaseFacts undefinedContradictoryBaseFacts;

    private List<Hypothesis> positiveHypothesis;

    private List<Hypothesis> negativeHypothesis;

    private List<Hypothesis> hypothesis;

    private BaseFacts baseFacts;

    private Map<IDataSets, Collection<Fact>> positiveFacts;

    private Map<IDataSets, Collection<Fact>> negativeFacts;

    private static final Logger log = LoggerFactory.getLogger(Inductor.class);

}