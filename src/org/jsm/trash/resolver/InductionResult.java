package org.jsm.trash.resolver;

import org.jsm.model.IProperty;
import org.jsm.model.IValue;

import java.util.List;

/**
 * @author Anatoly Semenov
 *         Date: 24.05.2015
 */
public class InductionResult
{
    private final IProperty targetProperty;
    private final IValue targetValue;

    private List<Hypothesis> positiveHypothesis;
    private List<Hypothesis> negativeHypothesis;


    public InductionResult(IProperty targetProperty, IValue targetValue)
    {
        this.targetProperty = targetProperty;
        this.targetValue = targetValue;
    }

    public IProperty getTargetProperty() {
        return targetProperty;
    }

    public IValue getTargetValue() {
        return targetValue;
    }

    public List<Hypothesis> getPositiveHypothesis() {
        return positiveHypothesis;
    }

    public List<Hypothesis> getNegativeHypothesis() {
        return negativeHypothesis;
    }

    public boolean addPositiveHypothesis(Hypothesis hypothesis) {
        return positiveHypothesis.add(hypothesis);
    }

    public boolean addNegativeHypothesis(Hypothesis hypothesis) {
        return positiveHypothesis.add(hypothesis);
    }
}
