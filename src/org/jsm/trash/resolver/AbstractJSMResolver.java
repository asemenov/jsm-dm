package org.jsm.trash.resolver;

import org.jsm.model.IDataSets;
import org.jsm.model.IProperty;

import java.util.List;

/**
 * @author Anatoly Semenov
 *         Date: 24.05.2015
 */
public abstract class AbstractJSMResolver implements JSMSolver {

    protected final Inductor inductor;
    protected final Analogue analogue;
    protected final Abductor abductor;

    protected AbstractJSMResolver(Inductor inductor, Analogue analogue, Abductor abductor) {
        this.inductor = inductor;
        this.analogue = analogue;
        this.abductor = abductor;
    }


    @Override
    public List<Hypothesis> resolve(IDataSets dataSets, IProperty targetProperty) {
        return null;
    }
}
