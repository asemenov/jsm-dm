package org.jsm.trash.resolver;

import org.jsm.model.IDataSets;
import org.jsm.model.IProperty;

import java.util.List;

/**
 * @author Anatoly Semenov
 *         Date: 24.05.2015
 */
public interface JSMSolver {
    public List<Hypothesis> resolve(IDataSets dataSets, IProperty targetProperty);
}
