package org.jsm.trash.resolver;

import org.jsm.model.IDataItem;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;

import java.util.List;

/**
 * @author Anatoly Semenov
 *         Date: 24.05.2015
 */
public class Hypothesis {

    private final Type type;
    private final IProperty targetProperty;
    private final IValue targetValue;
    private final List<IDataItem> sources;

    public Hypothesis(Type type, IProperty targetProperty, IValue targetValue, List<IDataItem> sources) {
        this.type = type;
        this.targetProperty = targetProperty;
        this.targetValue = targetValue;
        this.sources = sources;
    }

    public enum Type {
        POSITIVE, NEGATIVE, UNDEFINED
    }

    public Type getType() {
        return type;
    }

    public IProperty getTargetProperty() {
        return targetProperty;
    }

    public IValue getTargetValue() {
        return targetValue;
    }

    public List<IDataItem> getSources() {
        return sources;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hypothesis that = (Hypothesis) o;

        if (sources != null ? !sources.equals(that.sources) : that.sources != null) return false;
        if (targetProperty != null ? !targetProperty.equals(that.targetProperty) : that.targetProperty != null)
            return false;
        if (targetValue != null ? !targetValue.equals(that.targetValue) : that.targetValue != null) return false;
        if (type != that.type) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (targetProperty != null ? targetProperty.hashCode() : 0);
        result = 31 * result + (targetValue != null ? targetValue.hashCode() : 0);
        result = 31 * result + (sources != null ? sources.hashCode() : 0);
        return result;
    }
}
