package org.jsm.trash;

import org.jsm.common.AppConfig;
import org.jsm.model.IDataSets;
import org.jsm.transform.impl.DataSetToStringTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * User: Anatoly Semenov
 * Date: 30.04.2014
 */
public class DataSetAppender {
    private static final Logger log = LoggerFactory.getLogger(PropertyAppender.class);
    private static DataSetAppender instance = new DataSetAppender();
    private final File file;
    private static final String DATE_FORMAT = "yyyy_MM_dd_hh_mm_ss";

    private DataSetAppender() {
        String dir = AppConfig.getProperty("data_dir");
        File dirF = new File(dir);
        if (!dirF.exists())
            //noinspection ResultOfMethodCallIgnored
            dirF.mkdirs();
        String fileNamePattern = AppConfig.getProperty("dataset_file");
        fileNamePattern = fileNamePattern.replace("{date}", new SimpleDateFormat(DATE_FORMAT).format(new Date()));
        file = new File(dirF, fileNamePattern);
        if (!file.exists())
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
            } catch (IOException e) {
                log.error("Cannt create file");
                throw new RuntimeException(e);
            }
    }

    public static DataSetAppender instance() {
        return instance;
    }

    public void append(IDataSets iDataSets) {
        String dataSetAsString = new DataSetToStringTransformer().transform(iDataSets);
        try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)))) {
            out.println(dataSetAsString);
        }catch (IOException e) {
            log.error("Exception append", e);
        }
    }
}
