package org.jsm.ui.old;

/**
 * Created by IntelliJ IDEA.
 * User: Evgeny
 * Date: 20.04.14
 * Time: 0:14
 * To change this template use File | Settings | File Templates.
 */

import org.jsm.JSMFacade;
import org.jsm.model.*;
import org.jsm.model.impl.data.DataItem;
import org.jsm.model.impl.data.DataSet;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.model.ModelImpl;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.property.TreeProperty;
import org.jsm.model.impl.value.StringValue;
import org.jsm.trash.DataSetAppender;
import org.jsm.trash.PropertyAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.List;

/**
 * @author Evgeny
 */
public class MainFrame extends javax.swing.JFrame {
    private static final Logger log = LoggerFactory.getLogger(MainFrame.class);

    // Variables declaration - do not modify
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox10;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    private javax.swing.JCheckBox jCheckBox6;
    private javax.swing.JCheckBox jCheckBox7;
    private javax.swing.JCheckBox jCheckBox8;
    private javax.swing.JCheckBox jCheckBox9;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLayeredPane jLayeredPane1;
    private javax.swing.JLayeredPane jLayeredPane2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButton3;
    private javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JRadioButton jRadioButton5;
    private javax.swing.JRadioButton jRadioButton6;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollBar jScrollBar2;
    private javax.swing.JTabbedPane jTabbedPane3;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField10;
    private javax.swing.JTextField jTextField11;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    private javax.swing.JTextField jTextField7;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JTextField jTextField9;
    Collection<IValue> values = new ArrayList();
    private Map<JLabel, List<JCheckBox>> secondPanelLabelsAndCheckBoxs = new HashMap();
    private Map<JLabel, ButtonGroup> secondPanelLabelsAndJRadioButtons = new HashMap();
    TreeProperty valuesTree = null;
    ITreeProperty latestTree = null;
    IProperty createdStructure;
    String border = "             ";
    int counter = 0;
    int l = 0;
     boolean  rootNullable=false;
     boolean  rootMultiple=false;
    private static List<JLabel> labels = new ArrayList<JLabel>();
    String[] alfabet = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    IModel collectionOfStructure = null;
    JTextField[] valuesFields = new JTextField[8];
    List<IProperty> structure = new ArrayList<>();
    boolean flagFile = true;
    // End of variables declaration
    /**
     * Creates new form MainFrame
     */
    static Integer i = 0;
    static List stack = new ArrayList();
    static MainFrame frame;

    public MainFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        buttonGroup6 = new javax.swing.ButtonGroup();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jLayeredPane2 = new javax.swing.JLayeredPane();
        jTextField1 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jTextField2 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jScrollBar2 = new javax.swing.JScrollBar();
        jButton2 = new javax.swing.JButton();
        jTextField3 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        jTextField6 = new javax.swing.JTextField();
        jTextField7 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextField9 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLayeredPane1 = new javax.swing.JLayeredPane();
        jScrollBar1 = new javax.swing.JScrollBar();
        jButton5 = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextField10 = new javax.swing.JTextField();
        jTextField11 = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();


        valuesFields[0] = jTextField2;
        valuesFields[1] = jTextField3;
        valuesFields[2] = jTextField4;
        valuesFields[3] = jTextField5;
        valuesFields[4] = jTextField6;
        valuesFields[5] = jTextField7;
        valuesFields[6] = jTextField8;
        valuesFields[7] = jTextField9;


        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("JSM-DM");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(java.awt.Color.lightGray);
        setPreferredSize(new java.awt.Dimension(920, 730));
        jTabbedPane3.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if ((((JTabbedPane) e.getSource()).getSelectedIndex()) == 1) {
//                             if (counter<1)
//                {
                    jTabbedPane3MouseClicked(e);
//              counter++;
//                }
                }
            }
        });



        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel1.setText("Name of Structure:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Set", "Interval", "GroupTree"}));


        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel2.setText("Type of structure:");



        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel3.setText("Set of Values:");

        jCheckBox2.setText("+,-");


        jCheckBox3.setText("t");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel4.setText("Select:");

        jRadioButton1.setText("SingleSelect");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        jRadioButton2.setText("MultiSelect");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });


        jButton1.setText("Create Values");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("1.");

        jScrollBar2.setPreferredSize(new java.awt.Dimension(17, 730));

        jButton2.setText("Create Structure");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        jLabel6.setText("2.");

        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel7.setText("3.");

        jLabel8.setText("4.");

        jLabel9.setText("5.");

        jLabel10.setText("6.");

        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });

        jLabel11.setText("7.");

        jLabel12.setText("8.");

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 16)); // NOI18N
        jLabel13.setText("  Item Values:");

        jButton3.setText("Create New Tree");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("End Last Tree");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });


        jLayeredPane2.setBackground(new java.awt.Color(0, 102, 255));
        jLayeredPane2.setPreferredSize(new java.awt.Dimension(920, 730));
        javax.swing.GroupLayout jLayeredPane2Layout = new javax.swing.GroupLayout(jLayeredPane2);
        jLayeredPane2.setLayout(jLayeredPane2Layout);
        jLayeredPane2Layout.setHorizontalGroup(
                jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                .addGap(26, 26, 26)
                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 204, Short.MAX_VALUE)
                                                                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                                                .addGap(9, 9, 9)
                                                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                                .addComponent(jLabel2)))
                                                                .addGap(48, 48, 48))))
                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                .addContainerGap()
                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addGap(50, 50, 50)
                                                .addComponent(jLabel4)
                                                .addGap(18, 75, Short.MAX_VALUE)
                                                .addComponent(jScrollBar2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jCheckBox2)
                                                                        .addComponent(jCheckBox1))
                                                                .addGap(64, 64, 64)
                                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                        .addComponent(jRadioButton1)
                                                                        .addComponent(jRadioButton2)))
                                                        .addComponent(jCheckBox3)
                                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jLayeredPane2Layout.createSequentialGroup()
                                                                .addComponent(jButton2)
                                                                .addGap(6, 6, 6)))
                                                .addContainerGap())))
        );
        jLayeredPane2Layout.setVerticalGroup(
                jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jCheckBox1)
                                        .addComponent(jRadioButton1))
                                .addGap(18, 18, 18)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jLayeredPane2Layout.createSequentialGroup()
                                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                        .addComponent(jCheckBox2)
                                                        .addComponent(jRadioButton2))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                                .addComponent(jCheckBox3))
                                        .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel5)
                                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel6)
                                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(8, 8, 8)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel7))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel8))
                                .addGap(8, 8, 8)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel9))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel11))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextField9, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel12))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(jLayeredPane2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())
                        .addComponent(jScrollBar2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
        );
        jLayeredPane2.setLayer(jTextField1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jComboBox1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jCheckBox1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jCheckBox2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jCheckBox3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel4, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jRadioButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jRadioButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jButton1, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel5, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jScrollBar2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jScrollBar2.getAccessibleContext().setAccessibleParent(this);
        jLayeredPane2.setLayer(jButton2, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel6, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField4, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel7, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel8, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel9, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel10, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField5, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField6, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField7, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel11, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField8, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel12, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jTextField9, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jLabel13, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jButton3, javax.swing.JLayeredPane.DEFAULT_LAYER);
        jLayeredPane2.setLayer(jButton4, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jTabbedPane3.addTab("JSM-Modeler", jLayeredPane2);
        jTabbedPane3.setFont(new java.awt.Font("Comic", 1, 16));
        jTabbedPane3.addTab("JSM-DataCreator", jLayeredPane1);
        jMenu1.setText("File");
        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Save");
        jMenu1.add(jMenuItem1);
        jMenuItem1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jButton2ActionPerformed(e);
            }
        });
         jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Exit");
             jMenuItem4.addActionListener(new ActionListener() {
                 @Override
                 public void actionPerformed(ActionEvent e) {

                      if(frame!=null)
                      {
                          frame.setVisible(false) ;
                          System.exit(0);
                      }
                 }
             });
        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Open File of Etalon Structure");
        jMenuItem2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    collectionOfStructure = JSMFacade.getEtalonModel();  //getEtalon().getProperties()    //first
                } catch (Exception e1) {
                    jCheckBox6ActionPerformed(e);
                    log.debug(e1.getMessage());
                    e1.printStackTrace();  //To chanfge body of catch statement use File | Settings | File Templates.
                       return;
                }
                if (collectionOfStructure != null) {
                    jCheckBox5ActionPerformed(e);
                    flagFile = false;
                    log.info("File opened!");
                    counter = 0;
                }
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Edit Structure File");
         //to do action on open file
               jMenuItem3.addActionListener(new ActionListener() {
                   @Override
                   public void actionPerformed(ActionEvent e) {
                       showChooser();
                   }
               });


        jMenu2.add(jMenuItem3);
        jMenu1.add(jMenuItem4);
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jTabbedPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 842, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jTabbedPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 668, Short.MAX_VALUE))
        );


        getAccessibleContext().setAccessibleName("JSM-DataModeler");
        jButton5.setText("Create Client Data");


        jButton5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jButton5ActionPerformed(e);
            }
        });
        jLabel19.setFont(new java.awt.Font("Times New Roman", 1, 20)); // NOI18N
        jLabel19.setText("Client Profile                              ");
        jLabel19.setPreferredSize(new Dimension(10, 60));
        jLabel14.setFont(new java.awt.Font("Tahoma", 5, 18)); // NOI18N
        jLabel14.setText("Full Name:    ");
        JLabel label22 = new JLabel("                           ");
        jLabel15.setFont(new java.awt.Font("Tahoma", 5, 18)); // NOI18N
        jLabel15.setText("Comments:");
//          final Font font = new Font("Verdana", Font.PLAIN, 25);
        JPanel butPanel = new JPanel();
        butPanel.setLayout(new BoxLayout(butPanel, BoxLayout.Y_AXIS));
        butPanel.add(jLabel19);
        butPanel.add(label22);
        JPanel contentPane = new JPanel();
        contentPane.setComponentOrientation(
                ComponentOrientation.LEFT_TO_RIGHT);

//        Any number of rows and 5 columns
        contentPane.setLayout(new GridLayout(0, 5));

        contentPane.add(jLabel14);
        contentPane.add(jTextField10);
        contentPane.add(new JLabel());
        contentPane.add(new JLabel());
        contentPane.add(new JLabel());
        contentPane.add(jLabel15);
        contentPane.add(jTextField11);
        contentPane.add(new JLabel());
        contentPane.add(new JLabel());
        contentPane.add(jButton5);
        butPanel.add(contentPane);

        jLayeredPane1.setLayout(new BorderLayout());
        jLayeredPane1.add(butPanel, BorderLayout.NORTH);

        pack();
    }// </editor-fold>

    MouseListener listener = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {

            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void mousePressed(MouseEvent e) {
            if ((e.getModifiersEx() & MouseEvent.BUTTON3_DOWN_MASK) != 0) {
                AbstractButton absB = (AbstractButton) e.getSource();
                if (absB.getForeground().getBlue() == Color.BLUE.getBlue()) {
                    absB.setForeground(null);
                } else {
                    absB.setForeground(Color.BLUE);
                }
                // right button has been pressed
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        @Override
        public void mouseExited(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    };

String ext,description;

  public void showChooser()
    {
      JFileChooser dialog = new JFileChooser();
        dialog.setFileFilter(new myFileFilter(".txt",""));
        dialog.setCurrentDirectory(new File("./"));
        dialog.showOpenDialog(this);
        if(dialog.getSelectedFile()!=null){
        String[] commands = {"cmd", "/c",dialog.getSelectedFile().getAbsolutePath()};
        try {
            Runtime.getRuntime().exec(commands);
        } catch (Exception ex) {
            log.debug(ex.getMessage());
            ex.printStackTrace();
        }
        }
    }


   public class myFileFilter extends javax.swing.filechooser.FileFilter {
      String ext,description;

    public String getDescription() {
        return description;
    }

    myFileFilter(String ext, String description) {
          this.ext = ext;
      }
      public boolean accept(File f) {
          if(f != null && f.toString()!=null) {
              if(f.isDirectory()) {
                  return true;
              }
              return f.toString().endsWith(ext);
          }
          return false;
      }



}

    private JLabel createNewLabelOnPanel2(String name) {
        JLabel jLabel = new javax.swing.JLabel();
        int number = labels.size() + 1;
        jLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel.setText("  " + number + ". " + name);
        return jLabel;
//         for
//           secondPanelJLabels
    }

    private List<JCheckBox> createNewCheckBox(List<IValue> values, String border) {
        List<JCheckBox> jCheckBoxList = new ArrayList<>();
        int k = 0;
        for (IValue valueItem : values) {
            JCheckBox jCheckBox = new javax.swing.JCheckBox();
            jCheckBox.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
            if (border == null) {
                jCheckBox.setText("                 " + alfabet[k] + ")   " + (String) valueItem.getValue() + "   ");
            } else {
                jCheckBox.setText("                 " + border + alfabet[k] + ")   " + (String) valueItem.getValue() + "   ");
            }
            jCheckBox.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

            jCheckBoxList.add(jCheckBox);
            k++;
            if (k > 21) {
                k = 0;
            }
        }
        return jCheckBoxList;
    }

    private List<JRadioButton> createNewRadioButton(List<IValue> values, String border) {
        List<JRadioButton> jRadioButtonList = new ArrayList<>();
//        final ButtonGroup group = new ButtonGroup();
        int k = 0;
        for (IValue valueItem : values) {
            JRadioButton jRadioButton = new JRadioButton();
            jRadioButton.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
            String value = (String) valueItem.getValue();
            if (border == null) {
                jRadioButton.setText("                 " + alfabet[k] + ")   " + (String) valueItem.getValue() + "   ");
            } else {
                jRadioButton.setText("                 " + border + alfabet[k] + ")   " + (String) valueItem.getValue() + "   ");
            }
            jRadioButton.setActionCommand(value);
            jRadioButton.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

            jRadioButtonList.add(jRadioButton);
            k++;
            if (k > 21) {
                k = 0;
            }
        }
        return jRadioButtonList;
    }

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

        for (int i = 0; i < 8; i++) {
            String value = valuesFields[i].getText();
            if (value != null && !value.equals("")) {
                values.add(new StringValue(value));
            }
        }
        for (int i = 0; i < 8; i++) {
            valuesFields[i].setText("");

        }
    }


    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        if (jRadioButton2.isSelected()) {
            jRadioButton2.setSelected(false);
        }

    }

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        if (jRadioButton1.isSelected()) {
            jRadioButton1.setSelected(false);
        }

        // TODO add your handling code here:
    }

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
        //нужно добавлять дерево, или список значений в котором можеь быть ещё список а в списке еще список
        String typeOfStructure = jComboBox1.getModel().getSelectedItem().toString();
        String nameOfStructure = jTextField1.getText();
        boolean isNullble = false;
        boolean isMultiple = false;

        if (typeOfStructure.equals("GroupTree")) {
            if (jRadioButton2.isSelected()) {
                isMultiple = true;
            }
            if (jCheckBox3.isSelected()) {
                isNullble = true;
            }
            Collection<IValue> valuesTree = new ArrayList();

            for (int i = 0; i < 8; i++) {
                String value = valuesFields[i].getText();
                if (value != null && !value.equals("")) {
                    valuesTree.add(new StringValue(value));
                }
            }
            for (int i = 0; i < 8; i++) {
                valuesFields[i].setText("");

            }
            jTextField1.setText("");

            if (createdStructure == null) {

                values.addAll(valuesTree);
                createdStructure = new TreeProperty(nameOfStructure, isNullble, isMultiple, new ArrayList<IProperty>(), values);
                latestTree = (TreeProperty) createdStructure;
            } else {
                ITreeProperty newTree = new TreeProperty(nameOfStructure, isNullble, isMultiple, new ArrayList<IProperty>(), valuesTree);
                newTree.setParent(latestTree);
                if (latestTree != null)
                    latestTree.addProperty(newTree);
                latestTree = newTree;
            }

        }
        i++;


    }


    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
        ITreeProperty valueParentTree = latestTree.getParent();
        if (valueParentTree != null) {
            latestTree = latestTree.getParent();

        } else {
            latestTree = (TreeProperty) createdStructure;
            log.info("Root Element is detected!");
        }
    }

    private void jCheckBox6ActionPerformed(java.awt.event.ActionEvent evt) {

        JOptionPane.showMessageDialog(this, "Etalon file cannot be opened. Please verify path and it structure!",
                "Failed!", JOptionPane.INFORMATION_MESSAGE);
    }

    private void jCheckBox5ActionPerformed(java.awt.event.ActionEvent evt) {
        JOptionPane.showMessageDialog(this, "Etalon file opened!",
                "Successful!", JOptionPane.INFORMATION_MESSAGE);
    }


    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
        String nameOfClient = jTextField10.getText().trim();
        String comments = jTextField11.getText().trim();

        Map<IProperty, IDataItem> mapOfDataItem = new HashMap<IProperty, IDataItem>();

        Set<JLabel> valCheckBox = secondPanelLabelsAndCheckBoxs.keySet();
        for (JLabel lab : valCheckBox) {
            List<IValue> positive = new ArrayList<>();
            List<IValue> negative = new ArrayList<>();
            List<IValue> undefined = new ArrayList<>();
//        List<IProperty> iProperties= (List)  collectionOfStructure.getProperties();
            List<IProperty> iProperties = structure;

            for (IProperty property : iProperties) {

                String labelName = lab.getText().trim().substring(3);
                if (property.getName().equals(labelName)) {
                    Collection<IValue> values = null;
                    if (property instanceof TreeProperty) {
                        values = returnAllTreeLists((TreeProperty) property);
                    } else {
                        values = property.getPropertyValues();
                    }

                    List<JCheckBox> items = secondPanelLabelsAndCheckBoxs.get(lab);
                    for (JCheckBox box : items) {

                        IValue iVal = null;
                        for (IValue va : values) {
                            if (iVal != null) {
                                break;
                            }
                            if (((String)va.getValue()).trim().equals(box.getText().trim().substring(3).trim())) {
                                iVal = va;
                            }
                        }
                        if (iVal != null) {
                            if (box.isSelected()) {
                                positive.add(iVal);
                                box.setSelected(false);
                                box.setForeground(getBackground());

                            } else if (box.getForeground().getBlue() == Color.BLUE.getBlue()) {
                                undefined.add(iVal);
                                box.setForeground(getBackground());

                            } else {
                                negative.add(iVal);

                            }
                        }
                    }
                    IDataItem item = new DataItem(property, positive, negative, undefined);
                    mapOfDataItem.put(property, item);
                    //.add(item);


                }
            }

        }

        Set<JLabel> valRadioButton = secondPanelLabelsAndJRadioButtons.keySet();
        for (JLabel lab : valRadioButton) {
            List<IValue> positive = new ArrayList<>();
            List<IValue> negative = new ArrayList<>();
            List<IValue> undefined = new ArrayList<>();
            List<IProperty> iProperties = structure;

            for (IProperty property : iProperties) {

                String labelName = lab.getText().substring(5);
                if (property.getName().equals(labelName)) {
                    IValue iVal = null;

                    ButtonGroup group = secondPanelLabelsAndJRadioButtons.get(lab);
                    ButtonModel radioB = group.getSelection();
                    Collection<IValue> values = property.getPropertyValues();

                    if (radioB != null) {
                        String choice = radioB.getActionCommand();

                        for (IValue va : values) {
                            if (choice != null && va.getValue().equals(choice.trim())) {
                                iVal = va;
                                positive.add(iVal);
                                group.clearSelection();
                            }
                        }
                    }

                    IDataItem item = new DataItem(property, positive, negative, undefined);
                    mapOfDataItem.put(property, item);
                    //collectionOfDataItem.add(item);
                }


            }
        }

        IModel model = new ModelImpl("Test", structure);
        IDataSets result = new DataSets(model, Collections.<IDataSet>singletonList(new DataSet(model,
                mapOfDataItem, nameOfClient, comments)));
        mapOfDataItem = new HashMap<IProperty, IDataItem>();
        DataSetAppender.instance().append(result);
        jTextField10.setText("");
        jTextField11.setText("");
        JOptionPane.showMessageDialog(this, "New client was successfully created!",
                "Successful!", JOptionPane.INFORMATION_MESSAGE);
    }

    private void jTabbedPane3MouseClicked(ChangeEvent evt) {
        boolean failed = false;
        if (flagFile) {
            try {
                collectionOfStructure = JSMFacade.getLastModel();//getLast. getProperties.      //second
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Please create new structure or update etalon structure file!",
                        "Failed!", JOptionPane.INFORMATION_MESSAGE);
                failed = true;
               log.debug(e.getMessage());
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        } else {
            flagFile = true;

        }
         secondPanelLabelsAndCheckBoxs=new HashMap<>();
           secondPanelLabelsAndJRadioButtons=new HashMap<>();

        if (!failed) {
            final JPanel labPanel = new JPanel();
            final JScrollPane scrollPane = new JScrollPane(labPanel);
            labPanel.setLayout(new BoxLayout(labPanel, BoxLayout.Y_AXIS));

//         Collection<IValue> values=new ArrayList<>();
//         Collection<IValue> values1=new ArrayList<>();
//         Collection<IValue> values2=new ArrayList<>();
//         Collection<IValue> values3=new ArrayList<>();
//         Collection<IValue> values4=new ArrayList<>();
//         Collection<IValue> values5=new ArrayList<>();
//         Collection<IValue> valuesTree=new ArrayList<>();
//         Collection<IValue> valuesTree1=new ArrayList<>();
//         Collection<IValue> valuesTree2=new ArrayList<>();
//        values1.add(new StringValue("мужской")) ;
//        values1.add(new StringValue("женский"))   ;
//
//        values2.add(new StringValue("до 2"))   ;
//        values2.add(new StringValue("3 - 6"))   ;
//        values2.add(new StringValue("7 -15"))   ;
//        values2.add(new StringValue("16 - 25"))   ;
//        values2.add(new StringValue("26-49"))   ;
//        values2.add(new StringValue("50 и более"))   ;
//        values3.add(new StringValue("снижение зрения"))   ;
//        values3.add(new StringValue("выпадение участков в поле зрения"))   ;
//        values3.add(new StringValue("искривление изображения"))   ;
//        values3.add(new StringValue("метаморфопсии"))   ;
//        values3.add(new StringValue("фотопсии"))   ;
//        values3.add(new StringValue("\"куриная слепота\""))   ;
//        values3.add(new StringValue("туман (пятно) перед глазом"))   ;
//        values4.add(new StringValue("да"))   ;
//        values4.add(new StringValue("нет"))   ;
//        values5.add(new StringValue("структура нормальна"))   ;
//        values5.add(new StringValue("плавающие помутнения"))   ;
//        values5.add(new StringValue("нитчатые помутнения"))   ;
//        values5.add(new StringValue("мембраны (вуали)"))   ;
//        values5.add(new StringValue("выраженная зернистая деструкция"))   ;
//        values5.add(new StringValue("преретинальные пленки"))   ;
//        values5.add(new StringValue("грубые пленки, шварты"))   ;
//        values5.add(new StringValue("задняя отслойка стекловидного тела"))   ;
//        values5.add(new StringValue("оптически пустое"))   ;
//        valuesTree.add(new StringValue("valueItem1"))   ;
//        valuesTree.add(new StringValue("valueItem2"))   ;
//        valuesTree.add(new StringValue("valueItem3"))   ;
//
//              valuesTree1.add(new StringValue("valueItem4"))   ;
//        valuesTree1.add(new StringValue("valueItem5"))   ;
//        valuesTree1.add(new StringValue("valueItem6"))   ;
//
//              valuesTree2.add(new StringValue("valueItem7"))   ;
//        valuesTree2.add(new StringValue("valueItem8"))   ;
//        valuesTree2.add(new StringValue("valueItem9"))   ;
////        values.add(new StringValue("Value 2"))   ;
////        values.add(new StringValue("Valueaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa 3"))   ;
//        structure.add(new SelectProperty("Пол: ",false,false,values1)) ;
//        structure.add(new SelectProperty("Возраст начала заболевания(лет) ",false,false,values2)) ;
//        structure.add(new SelectProperty("Субъективные жалобы (да, нет, не описано)",true,true,values3)) ;
//        structure.add(new SelectProperty("Имеется ли сходное заболевание среди членов семьи ",false,false,values4)) ;
//
//        TreeProperty child=  new TreeProperty("ChildTree 1",true,true,new ArrayList<IProperty>(),valuesTree1) ;
//        TreeProperty child2=  new TreeProperty("ChildTree 2" ,true,true,new ArrayList<IProperty>(),valuesTree2) ;
////        TreeProperty child3=  new TreeProperty("ChildTree 3",true,true,new ArrayList<IProperty>(),valuesTree) ;
////        TreeProperty child4=  new TreeProperty("ChildTree 4",true,true,new ArrayList<IProperty>(),valuesTree) ;
////        TreeProperty child5=  new TreeProperty("ChildTree 5",true,true,new ArrayList<IProperty>(),valuesTree) ;
////        TreeProperty child6=  new TreeProperty("ChildTree 6",true,true,new ArrayList<IProperty>(),valuesTree) ;
////        TreeProperty child7=  new TreeProperty("ChildTree 7",true,true,new ArrayList<IProperty>(),valuesTree) ;
////        TreeProperty child8=  new TreeProperty("ChildTree 8",true,true,new ArrayList<IProperty>(),valuesTree) ;
////        TreeProperty child9=  new TreeProperty("ChildTree 9",true,true,new ArrayList<IProperty>(),valuesTree) ;
//
//       TreeProperty root= new TreeProperty("Tree",true,true,new ArrayList<IProperty>(),valuesTree);
////        List<IProperty> children=new ArrayList<>();
//        child.setParent(root);
//        root.addProperty(child);
//
//        child2.setParent(child);
//        child.addProperty(child2);

//        child3.setParent(child);
//        child.addProperty(child3);
//
//          child4.setParent(child2);
//        child2.addProperty(child4);
//
//        child5.setParent(child2);
//        child2.addProperty(child5);
//
//
//        child6.setParent(child);
//        child.addProperty(child6);
//
//        child7.setParent(child6);
//        child6.addProperty(child7);
//
//        child8.setParent(child7);
//        child7.addProperty(child8);
//
//                        child9.setParent(child5);
//        child5.addProperty(child9);
//
//        structure.add( root);
//
//        structure.add(new SelectProperty("Стекловидное тело",false,true,values5)) ;

            JLabel label = null;
            List<JCheckBox> jCheckBoxList = new ArrayList<>();
            List<JRadioButton> jRadioButtons = null;

//        labPanel.add(emptyLable);
            if (collectionOfStructure != null) {
                structure = (List) collectionOfStructure.getProperties();
            }
            for (IProperty item : structure) {
                JLabel emptyLable1 = new JLabel(" ");
                emptyLable1.setPreferredSize(new Dimension(10, 10));
                JLabel emptyLable2 = new JLabel(" ");
                emptyLable2.setPreferredSize(new Dimension(10, 10));
                labPanel.add(emptyLable1);
//            labPanel.add(emptyLable2);
                if (item instanceof SelectProperty) {
                    if (item.isMultiple()) {
                        label = createNewLabelOnPanel2(item.getName());

                        jCheckBoxList = createNewCheckBox((List) item.getPropertyValues(), null);
                        if (item.isNullable()) {
                            for (JCheckBox check : jCheckBoxList) {
//                       check.addItemListener(itemlistener);
                                check.addMouseListener(listener);
                            }
                        }
                        secondPanelLabelsAndCheckBoxs.put(label, jCheckBoxList);
                        labels.add(label);
                        label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
                        labPanel.add(label);

                        JLabel emptyLable = new JLabel(" ");
                        emptyLable1.setPreferredSize(new Dimension(10, 10));
//                emptyLable.setAlignmentX(JLabel.LEFT_ALIGNMENT);
                        labPanel.add(emptyLable);

                        for (JCheckBox checkBox : jCheckBoxList) {
                            labPanel.add(checkBox);
                        }

                        scrollPane.revalidate();

                    } else {
                        label = createNewLabelOnPanel2(item.getName());

                        jRadioButtons = createNewRadioButton((List) item.getPropertyValues(), null);

                        ButtonGroup jButtonGroup = new ButtonGroup();
                        for (JRadioButton radioButton : jRadioButtons) {
                            jButtonGroup.add(radioButton);
                        }

                        secondPanelLabelsAndJRadioButtons.put(label, jButtonGroup);
                        labels.add(label);
                        label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
                        labPanel.add(label);

                        JLabel emptyLable = new JLabel(" ");
                        emptyLable1.setPreferredSize(new Dimension(10, 10));
                        labPanel.add(emptyLable);
                        for (JRadioButton radioButton : jRadioButtons) {
                            labPanel.add(radioButton);
                        }
                        scrollPane.revalidate();
                    }

                } else if (item instanceof TreeProperty) {
                    drawTree(scrollPane, label, labPanel, jCheckBoxList, jRadioButtons, (TreeProperty) item);
                    l = 0;
                      rootNullable=false;
        rootMultiple=false;
                }
            }
            if (jLayeredPane1.getComponents().length > 1) {
                jLayeredPane1.remove(1);
            }

            jLayeredPane1.add(scrollPane, BorderLayout.CENTER);
            labels = new ArrayList<>();
        }

    }

    private String returnBorder(int k) {
        if (k == 0) {
            return null;
        }

        String result = "             ";
        for (int i = 0; i < k; i++) {
            result += border;
        }
        return result;
    }

    List resultLists = new ArrayList();

    private List<IValue> returnAllTreeLists(TreeProperty root) {
        resultLists.addAll(root.getPropertyValues());
        List nodes = ((LinkedList) root.getChildren());
        for (int i = 0; i < nodes.size(); i++) {

            if (stack.add(nodes.get(i))) {

                returnAllTreeLists((TreeProperty) nodes.get(i));
            }
        }
        return resultLists;
    }

    public void drawTree(JScrollPane scrollPane, JLabel label, JPanel labPanel, List<JCheckBox> jCheckBoxList, List<JRadioButton> jRadioButtons, TreeProperty node) { // Node - вершина графа

        createStructureItem(scrollPane, label, labPanel, jCheckBoxList, jRadioButtons, node, returnBorder(l));
        l++;
        List nodes = ((LinkedList) node.getChildren());
        for (int i = 0; i < nodes.size(); i++) {

            if (stack.add(nodes.get(i))) {

                drawTree(scrollPane, label, labPanel, jCheckBoxList, jRadioButtons, (TreeProperty) nodes.get(i));
            }
            l--;
        }

    }

    public boolean createStructureItem(JScrollPane scrollPane, JLabel label, JPanel labPanel, List<JCheckBox> jCheckBoxList, List<JRadioButton> jRadioButtons, IProperty item, String border) {
        JLabel emptyLable1 = new JLabel(" ");
        emptyLable1.setPreferredSize(new Dimension(10, 10));
        if (item.isMultiple()|| rootMultiple) {
            String name = item.getName();
            int number = labels.size() + 1;
            if (border == null) {
                label = createNewLabelOnPanel2(name);
                jCheckBoxList = createNewCheckBox((List) item.getPropertyValues(), border);
                rootNullable=item.isNullable() ;
                rootMultiple=item.isMultiple() ;
                if (rootNullable) {
                    for (JCheckBox check : jCheckBoxList) {
//                       check.addItemListener(itemlistener);
                        check.addMouseListener(listener);
                    }
                }
                secondPanelLabelsAndCheckBoxs.put(label, jCheckBoxList);
                labels.add(label);
            }
            else {
                ITreeProperty root = ((TreeProperty) item);
                while (root.getParent() != null) {
                    root = ((TreeProperty) root).getParent();
                }


                label = new javax.swing.JLabel();
                label.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

                labPanel.add(emptyLable1);
                label.setText("  " + border + name);


                border += " ";
                jCheckBoxList = createNewCheckBox((List) item.getPropertyValues(), border);


                if (rootNullable) {
                    for (JCheckBox check : jCheckBoxList) {
                        check.addMouseListener(listener);
                    }
                }

                JLabel rootLabel = null;
                Set<JLabel> keys = secondPanelLabelsAndCheckBoxs.keySet();
                for (JLabel lab : keys) {
                    String labelName = lab.getText().trim().substring(3);

                    if (root != null && root.getName().equals(labelName)) {
                        rootLabel = lab;
                        break;
                    }
                }
                if (rootLabel != null) {
                    List<JCheckBox> listOfTreeValue = secondPanelLabelsAndCheckBoxs.get(rootLabel);
                    listOfTreeValue.addAll(jCheckBoxList);
                    secondPanelLabelsAndCheckBoxs.put(rootLabel, listOfTreeValue);
                }

            }

            label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            labPanel.add(label);

            JLabel emptyLable = new JLabel(" ");
            emptyLable1.setPreferredSize(new Dimension(10, 10));
//                emptyLable.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            labPanel.add(emptyLable);

            for (JCheckBox checkBox : jCheckBoxList) {
                labPanel.add(checkBox);
            }
            border += border;
            scrollPane.revalidate();

        }
        else {

            if (border == null) {

                label = createNewLabelOnPanel2(item.getName());
                labels.add(label);
                jRadioButtons = createNewRadioButton((List) item.getPropertyValues(), border);

            } else {
                label = new javax.swing.JLabel();
                label.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

                labPanel.add(emptyLable1);
                label.setText("  " + border + item.getName());

                border += " ";
                jRadioButtons = createNewRadioButton((List) item.getPropertyValues(), border);

            }

            ButtonGroup jButtonGroup = new ButtonGroup();
            for (JRadioButton radioButton : jRadioButtons) {
                jButtonGroup.add(radioButton);
            }

            secondPanelLabelsAndJRadioButtons.put(label, jButtonGroup);
            label.setAlignmentX(JLabel.LEFT_ALIGNMENT);
            labPanel.add(label);

            JLabel emptyLable = new JLabel(" ");
            emptyLable1.setPreferredSize(new Dimension(10, 10));
            labPanel.add(emptyLable);
            for (JRadioButton radioButton : jRadioButtons) {
                labPanel.add(radioButton);
            }
            scrollPane.revalidate();
        }
        return true;
    }

    //Action Panel 1
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
        String nameOfStructure = jTextField1.getText().trim();
        String typeOfStructure = jComboBox1.getModel().getSelectedItem().toString();
        String setOfValues = null;
        String select = null;
        boolean isNullble = false;
        boolean isMultiple = false;
        if (jCheckBox2.isSelected()) {
            setOfValues = jCheckBox2.getText();
        }

        if (jCheckBox3.isSelected()) {
            setOfValues += "|" + jCheckBox3.getText();
            isNullble = true;
        }
        if (jCheckBox1.isSelected()) {
            setOfValues += "|" + jCheckBox1.getText();
        }


        if (jRadioButton1.isSelected()) {
            select = jRadioButton1.getText();
        } else if (jRadioButton2.isSelected()) {
            select = jRadioButton2.getText();
            isMultiple = true;
        }
        for (int i = 0; i < 8; i++) {
            String value = valuesFields[i].getText().trim();
            if (value != null && !value.equals("")) {
                values.add(new StringValue(value));
            }
        }

        if (typeOfStructure != null) {
            if (typeOfStructure.equals("Set")) {
                createdStructure = new SelectProperty(nameOfStructure, isNullble, isMultiple, values);
            } else if (typeOfStructure.equals("Interval")) {
                createdStructure = new SelectProperty(nameOfStructure, isNullble, isMultiple, values);

            }  else if (typeOfStructure.equals("GroupTree")) {
                if (createdStructure == null) {


                    createdStructure = new TreeProperty(nameOfStructure, isNullble, isMultiple, new ArrayList<IProperty>(), values);

                }
            }
        }

        if (createdStructure != null && !createdStructure.getPropertyValues().isEmpty()
) {
            log.info("Write data to file!");
            PropertyAppender.instance().append(createdStructure);
            createdStructure = null;
            values = new ArrayList<>();
             JOptionPane.showMessageDialog(this, "New structure was successfully created!",
                "Successful!", JOptionPane.INFORMATION_MESSAGE);
             for (int i = 0; i < 8; i++) {
            valuesFields[i].setText("");

        }
        jCheckBox1.setSelected(false);
        jCheckBox2.setSelected(false);
        jCheckBox3.setSelected(false);
        jRadioButton1.setSelected(false);
        jRadioButton2.setSelected(false);
        jTextField1.setText("");
        jComboBox1.getModel().setSelectedItem(jComboBox1.getModel().getElementAt(0));
        }
        else
        {
             JOptionPane.showMessageDialog(this, "Please enter all properties of structure!",
                "Failed!", JOptionPane.INFORMATION_MESSAGE);
        }

    }


    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
           frame=     new MainFrame();
                frame.setVisible(true);
                frame.setAutoRequestFocus(true);
                 frame.pack();
                 frame.setLocationRelativeTo(null);
            }
        });
    }


}
