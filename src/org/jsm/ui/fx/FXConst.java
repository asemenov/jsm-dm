package org.jsm.ui.fx;

/**
 * @author Anatoly Semenov
 *         Date: 12.10.2014
 */
public class FXConst {
    private FXConst() {}

    public static final String MODEL_TAB = "model_tab";
    public static final String MODEL_TAB_PROPERTIES_TREE = "properties_tree";
    public static final String MODEL_TAB_FREE_PANE = "free_pane";
    public static final String MODEL_TAB_PANE = "tab_pane";
    public static final String EXPERIMENT_TAB_PANE = "experiment_pane";
    public static final String MODEL_TAB_PROPERTIES_LIST = "properties_list";
}
