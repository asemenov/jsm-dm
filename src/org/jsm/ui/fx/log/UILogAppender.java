package org.jsm.ui.fx.log;

import javafx.scene.control.TextArea;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * @author Anatoly Semenov
 *         Date: 11.10.2014
 */
public class UILogAppender extends ConsoleAppender {
    public static TextArea TEXT_AREA;

    @Override
    public synchronized void doAppend(LoggingEvent event) {
        if (TEXT_AREA != null) {
            TEXT_AREA.appendText(this.layout.format(event));
            if (event.getThrowableInformation() != null)
                for (String s : event.getThrowableInformation().getThrowableStrRep())
                    TEXT_AREA.appendText(s + "\n");
        }
    }

    public static void setTextArea(TextArea textArea) {
        TEXT_AREA = textArea;
    }
}
