package org.jsm.ui.fx.util;

/**
 * @author Anatoly Semenov
 *         Date: 12.10.2014
 */
public class UIUtils {
    public static String idAsSelector(String id) {
        return "#" + id;
    }
}
