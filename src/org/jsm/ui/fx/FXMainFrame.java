package org.jsm.ui.fx;

import com.google.common.collect.Lists;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.controlsfx.dialog.Dialogs;
import org.jsm.common.Utils;
import org.jsm.model.IDataSets;
import org.jsm.model.IModel;
import org.jsm.model.IProperty;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.model.ModelImpl;
import org.jsm.transform.impl.DataSetToStringTransformer;
import org.jsm.transform.impl.ModelToStringTransformer;
import org.jsm.ui.fx.component.list.PropertiesList;
import org.jsm.ui.fx.component.pane.DataTabPane;
import org.jsm.ui.fx.component.pane.ExperimentTapPane;
import org.jsm.ui.fx.log.UILogAppender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * @author Anatoly Semenov
 *         Date: 27.09.2014
 */
public class FXMainFrame extends Application{


    private static final Logger log = LoggerFactory.getLogger(FXMainFrame.class);

    private IDataSets dataSets;

    private Stage primaryStage;
    private Path lastOpenPath;
    private Scene mainScene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        // start is called on the FX Application Thread,
        // so Thread.currentThread() is the FX application thread:
        Thread.currentThread().setUncaughtExceptionHandler((thread, throwable) -> {
            log.error("Exception", throwable);
            Dialogs.create()
                    .title("Error")
                    .message(throwable.getMessage())
                    .showException(throwable);
        });

        primaryStage.setTitle("JSM-DM");
        this.mainScene = new Scene(new VBox(), Screen.getPrimary().getVisualBounds().getWidth(),
                Screen.getPrimary().getVisualBounds().getHeight());
        this.primaryStage = primaryStage;
        mainScene.setFill(Color.OLDLACE);
        reload();
        primaryStage.setScene(mainScene);
        primaryStage.show();
    }





    private MenuBar createMenuBar() {
        MenuBar menuBar = new MenuBar();

//        File
        Menu menuFile = new Menu("File");

        MenuItem exit = new MenuItem("Exit");
        exit.setOnAction(event -> System.exit(0));

        MenuItem fake = new MenuItem("Fake");
        MenuItem newProject = new MenuItem("New project");
        newProject.setOnAction((e)-> {
            lastOpenPath = null;
            String newProjectName = Dialogs.create()
                    .title("New project")
                    .message("Name: ")
                    .showTextInput();
            Utils.assertNotEmpty(newProjectName, "new project name");
            this.dataSets = new DataSets(new ModelImpl(newProjectName, Lists.newArrayList()), Lists.newArrayList());
            reload();
            log.info("New Project action = {} name = {}", e.getEventType(), newProjectName);
        });
        newProject.setAccelerator(KeyCombination.valueOf("Ctrl+N"));

        MenuItem openProject = new MenuItem("Open project");
        openProject.setOnAction((e)-> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Select project file");
            File toOpen = fileChooser.showOpenDialog(primaryStage);
            if (toOpen != null)  {
                try {
                    this.dataSets = Utils.Serialization.read(toOpen.toPath());
                    log.info("Open project {}", e.getEventType());
                    lastOpenPath = toOpen.toPath();
                    reload();
                } catch (IOException|ClassNotFoundException e1) {
                    log.error("Open project exception", e1);
                }
            }
        });
        openProject.setAccelerator(KeyCombination.valueOf("Ctrl+O"));
        MenuItem save = new MenuItem("Save");
        save.setOnAction((e)-> {
            if (lastOpenPath != null) {
                try {
                    Utils.Serialization.save(dataSets, lastOpenPath, StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
                    log.info("Save to {}", lastOpenPath);
                } catch (IOException e1) {
                    log.error("Save exception", e1);
                }
            }
        });
        save.setAccelerator(KeyCombination.valueOf("Ctrl+S"));

        MenuItem saveAs = new MenuItem("Save as");
        saveAs.setOnAction((e)-> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Save as");
            File f = fileChooser.showSaveDialog(primaryStage);
            if (f != null) {
                try {
                    Utils.Serialization.save(dataSets, f.toPath(), f.exists() ? StandardOpenOption.TRUNCATE_EXISTING :
                            StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE);
                    log.info("Save as {}", e.getEventType());
                } catch (IOException e1) {
                    log.error("SaveAs exception", e1);
                }

            }

        });
        saveAs.setAccelerator(KeyCombination.valueOf("Ctrl+Shift+S"));
        menuFile.getItems().addAll(newProject, openProject, save, saveAs, fake, new SeparatorMenuItem(),exit);

//        Edit
        Menu menuEdit = new Menu("Edit");

//        Edit
        Menu menuTools = new Menu("Tools");
        MenuItem modelStringRepresent = new MenuItem("Model as string");
        MenuItem dataSetsStringRepresent = new MenuItem("Dataset as string"); //todo impl
        menuTools.getItems().addAll(modelStringRepresent, dataSetsStringRepresent);

        modelStringRepresent.setOnAction(event -> {
//            IModel model = ((PropertiesTree) this.mainScene.lookup(UIUtils.idAsSelector(FXConst.MODEL_TAB_PROPERTIES_TREE))).getModel(); //todo think about refs between scenes
            IModel model = this.dataSets.getModel(); //todo think about refs between scenes
            if (model != null) {
                String transform = new ModelToStringTransformer().transform(model);
                log.info("Model as string \n {}", transform);
                Dialogs.create().message(transform).showInformation(); //todo show on dialog window with TextArea, for copy-paste model, with save and save as.. controls
            }
        });

        dataSetsStringRepresent.setOnAction(event -> {
            IDataSets iDataSets = this.dataSets;
            if (iDataSets != null) {
                String transform = new DataSetToStringTransformer().transform(iDataSets);
                log.info("Dataset as string \n {}", transform);
                Dialogs.create().message(transform).showInformation(); //todo show on dialog window with TextArea, for copy-paste model, with save and save as.. controls
            }

        });

//        Help
        Menu help = new Menu("Help");

        menuBar.getMenus().addAll(menuFile, menuEdit, menuTools, help);

        return menuBar;
    }

    private void reload() {
        ObservableList<Node> children = ((VBox) mainScene.getRoot()).getChildren();
        children.clear();
        children.addAll(createMenuBar(), tabPane(this.dataSets));
    }

    private TabPane tabPane(IDataSets iDataSets) {
        TabPane tabPane = new TabPane();
        Tab modelTab = modelTab(iDataSets);
        Tab dataTab = dataTab();
        Tab logTab = logTab();
        Tab experimentTab = experimentTab(iDataSets);

        tabPane.getTabs().addAll(modelTab, dataTab, experimentTab, logTab);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tabPane.setId(FXConst.MODEL_TAB_PANE);
        return tabPane;
    }

    private Tab experimentTab(IDataSets iDataSets)
    {
        Tab experimentTab = new Tab("Experiment");

        experimentTab.setOnSelectionChanged(event -> {
            experimentTab.setContent(new ExperimentTapPane(this.dataSets));
        });

        /*FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/ui/fx/component/pane/experiment-tab.fxml"));

        try
        {
            Pane content = loader.load();
            ExperimentPaneController controller = loader.getController();
            controller.init(iDataSets);
            experimentTab.setContent(content);
            experimentTab.setId(FXConst.EXPERIMENT_TAB_PANE);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }*/


        /*ExperimentTapPane experimentTapPane;

        if (iDataSets != null)
        {
            experimentTapPane = new ExperimentTapPane(iDataSets.getModel());

            /*experimentTab.setOnSelectionChanged(event -> {
                experimentTab.setContent(experimentTapPane);
            });*/
       /*     experimentTab.setContent(experimentTapPane);
            experimentTab.setId(FXConst.EXPERIMENT_TAB_PANE);
        }*/





        //experimentTab.setContent(new ExperimentTapPane(iDataSets.getModel()));
        //experimentTab.setId(FXConst.EXPERIMENT_TAB_PANE);
        return experimentTab;
    }

    private Tab modelTab(IDataSets iDataSets) {
        Tab modelTab = new Tab("Model");
        SplitPane splitPane = new SplitPane();
        splitPane.setOrientation(Orientation.HORIZONTAL);
        splitPane.setDividerPositions(0.3D, 0.7D);
        splitPane.getItems().addAll(modelTabLeftVBox(iDataSets), modelTableRightPane());
        modelTab.setContent(splitPane);
        modelTab.setId(FXConst.MODEL_TAB);
        return modelTab;
    }

    private VBox modelTabLeftVBox(IDataSets iDataSets) {
        VBox vBox = new VBox();
        if (iDataSets != null)
            vBox.getChildren().add(propertiesList(iDataSets.getModel(), iDataSets));
        vBox.getChildren().addAll(propertiesTable());
        return vBox;
    }

    private PropertiesList propertiesList(IModel model, IDataSets iDataSets)
    {
        PropertiesList propertiesList = new PropertiesList(model, mainScene, iDataSets);
        propertiesList.setId(FXConst.MODEL_TAB_PROPERTIES_LIST);
        //propertiesList.setContextMenu(new PropertiesListContextMenu(mainScene, model));
        return propertiesList;
    }

    /*private PropertiesTree propertiesTree(IModel model) {
        PropertiesTree propertiesTree = new PropertiesTree(model, mainScene);
        propertiesTree.setId(FXConst.MODEL_TAB_PROPERTIES_TREE);
        propertiesTree.setContextMenu(new PropertiesTreeContextMenu(mainScene, model));
        return propertiesTree;
    }*/

    private TableView<IProperty> propertiesTable() {
        TableView<IProperty> propertiesTable = new TableView<>();
        return propertiesTable;
    }


    private Pane modelTableRightPane() {
        Pane pane = new Pane();
        pane.setId(FXConst.MODEL_TAB_FREE_PANE);
        return pane;
    }

    private Tab dataTab() {
        Tab dataTab = new Tab("Data");
        dataTab.setOnSelectionChanged(event -> {
            dataTab.setContent(new DataTabPane(this.dataSets));
        });
        return dataTab;
    }

    private Tab logTab() {
        Tab logTab = new Tab("Logs");
        TextArea textArea = new TextArea();
        UILogAppender.setTextArea(textArea);
        logTab.setContent(textArea);
        return logTab;
    }

    public static void main(String[] args) {
        Application.launch(args);
    }

}
