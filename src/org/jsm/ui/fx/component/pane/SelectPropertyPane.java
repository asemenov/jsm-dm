package org.jsm.ui.fx.component.pane;

import com.google.common.collect.Lists;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.controlsfx.dialog.Dialogs;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;
import org.jsm.ui.fx.FXConst;
import org.jsm.ui.fx.component.list.PropertiesList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import static org.jsm.common.Utils.assertNotEmpty;
import static org.jsm.common.Utils.isEmpty;
import static org.jsm.ui.fx.util.UIUtils.idAsSelector;


/**
 * @author Anatoly Semenov
 *         Date: 12.10.2014
 */
public class SelectPropertyPane extends Pane{
    private static final Logger log = LoggerFactory.getLogger(SelectPropertyPane.class);


    public enum ValuesSet {
        PLUS_MINUS("+,-"), PLUS_MINUS_T("+,-,t");

        private final String s;


        ValuesSet(String s) {
            this.s = s;
        }

        public String asString() {
            return s;
        }

        public static ValuesSet forString(String s) {
            for (ValuesSet v : ValuesSet.values())
                if (v.asString().equals(s))
                    return v;
            return null;
        }

        public static ValuesSet forBoolean(boolean b) {
            return b ? PLUS_MINUS_T : PLUS_MINUS;
        }
    }

    private final Scene mainScene;
    public static final String NEW_SELECT_PROPERTY = "New select property";
    public static final String UPDATE_SELECT_PROPERTY = "Update select property";
    public static final String CREATE = "Create";
    public static final String UPDATE = "Update";

    public SelectPropertyPane(Scene mainScene, String title, String nameButton, SelectProperty selectProperty) {
        this.mainScene = mainScene;
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text sceneTitle = new Text(title);
        sceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(sceneTitle, 0, 0, 2, 1);

        Label propertyName = new Label("Name:");
        grid.add(propertyName, 0, 1);

        TextField propertyNameTextField = new TextField();
        grid.add(propertyNameTextField, 1, 1);
        if (selectProperty != null)
            propertyNameTextField.setText(selectProperty.getName());

        Label propertyValuesLabel = new Label("Property values set:");
        grid.add(propertyValuesLabel, 0, 2);

        ChoiceBox<String> propertyValuesList = new ChoiceBox<>();
        ObservableList<String> v = FXCollections.unmodifiableObservableList(
                FXCollections.observableArrayList(ValuesSet.PLUS_MINUS.asString(), ValuesSet.PLUS_MINUS_T.asString()));
        propertyValuesList.setItems(v);
        grid.add(propertyValuesList, 1, 2);
        if (selectProperty != null)
            propertyValuesList.getSelectionModel().select(ValuesSet.forBoolean(selectProperty.isNullable()).asString());

        Label multipleLabel = new Label("Is multiple:");
        grid.add(multipleLabel, 0, 3);

        ChoiceBox<Boolean> multipleListVariations = new ChoiceBox<>(
                FXCollections.observableArrayList(Boolean.TRUE, Boolean.FALSE));
        grid.add(multipleListVariations, 1, 3);
        if (selectProperty != null)
            multipleListVariations.setValue(selectProperty.isMultiple());


        Label availableValuesLabel = new Label("Available values:");
        grid.add(availableValuesLabel, 0, 4);

        ListView<String> availableValuesList = new ListView<>();
        availableValuesList.setOrientation(Orientation.VERTICAL);
        availableValuesList.setEditable(true);
        availableValuesList.setItems(FXCollections.observableArrayList(selectProperty == null ?
                Collections.<String>emptyList() : getPropertyValueStrings(selectProperty.getPropertyValues())));
        availableValuesList.setCellFactory(TextFieldListCell.forListView());
        availableValuesList.setContextMenu(new ContextMenu(){{
            MenuItem add = new MenuItem("Add");
            MenuItem remove = new MenuItem("Remove");

            add.setOnAction(event -> {
                log.info("add new property");
                availableValuesList.getItems().add("");
                availableValuesList.getSelectionModel().selectLast();
            });

            remove.setOnAction(event -> {
                int selectedIndex = availableValuesList.getSelectionModel().getSelectedIndex();
                if (selectedIndex != -1) {
                    String s = availableValuesList.getItems().remove(selectedIndex);
                    log.info("remove property {}", s);
                }
            });
            getItems().addAll(add, remove);
        }});
        grid.add(availableValuesList, 1, 4);

        Button create = new Button(nameButton);
        create.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                String name = propertyNameTextField.getText();
                String isNullableString = propertyValuesList.getValue();
                ValuesSet set = ValuesSet.forString(isNullableString);
                Boolean isMultiple = multipleListVariations.getValue();
                Collection<IValue> values = Lists.newArrayList();
                values.addAll(availableValuesList.getItems().stream()
                        .filter(s -> !isEmpty(s)).map(StringValue::new).collect(Collectors.toList()));
                try {
                    assertNotEmpty(name, "Name");
                    assertNotEmpty(set, "Property values set");
                    assertNotEmpty(isMultiple, "Is multiple");
                    assertNotEmpty(values, "Available values");
                    IProperty iProperty = SelectProperty.Factory.selectPropertyBuilder()
                            .setName(name)
                            .setNullable(ValuesSet.PLUS_MINUS_T.equals(set))
                            .setMultiple(isMultiple)
                            .setPropertyValues(values)
                            .build();

                    PropertiesList propertiesList = ((PropertiesList)SelectPropertyPane.this.mainScene
                            .lookup(idAsSelector(FXConst.MODEL_TAB_PROPERTIES_LIST)));
                    if (((Button) event.getSource()).getText().equals(CREATE)){
                        propertiesList.addIProperty(iProperty);
                    }else{
                        propertiesList.changeProperty(selectProperty, iProperty);
                    }

                    /*PropertiesTree propertiesTree = ((PropertiesTree)SelectPropertyPane.this.mainScene
                            .lookup(idAsSelector(FXConst.MODEL_TAB_PROPERTIES_TREE)));
                    if (((Button) event.getSource()).getText().equals(CREATE)){
                        propertiesTree.addIProperty(iProperty);
                    }else{
                        propertiesTree.changeProperty(selectProperty, iProperty);
                    }-*/

                    grid.setVisible(false);
                }
                catch (Throwable th) {
                    log.error("Select property create exception", th);
                    Dialogs.create()
                            .title("Select property create error")
                            .message(th.getMessage())
                            .showError();
                }
            }
        });
        grid.add(create, 1, 5);
        getChildren().addAll(grid);
    }

    private Collection<String> getPropertyValueStrings(Collection<IValue> availableValues){
        Collection<String> availableValuesStrings = new ArrayList<>();
        for (IValue availableValue : availableValues) {
            availableValuesStrings.add(((StringValue)availableValue).getValue());
        }
        return availableValuesStrings;
    }
}
