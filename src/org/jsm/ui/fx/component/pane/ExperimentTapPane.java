package org.jsm.ui.fx.component.pane;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import org.jsm.model.IDataSet;
import org.jsm.model.IDataSets;
import org.jsm.model.IProperty;
import org.jsm.model.impl.data.DataSets;
import org.jsm.trash.resolver.impl.Hypothesis;
import org.jsm.ui.fx.component.controller.ExperimentPaneController;
import org.jsm.ui.fx.component.controller.ExperimentResultPaneController;
import org.jsm.model.IValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Данил on 09.06.2015.
 */
public class ExperimentTapPane extends Pane
{
    public ExperimentTapPane(IDataSets iDataSets)
    {
        this.iDataSets = iDataSets;

        positiveHypothesis = new ArrayList<>();
        negativeHypothesis = new ArrayList<>();

        newDataSets = new DataSets(iDataSets.getModel(), new ArrayList<IDataSet>());

        TabPane tabPane = new TabPane();
        tabPane.setSide(Side.LEFT);

        Tab configurationTab = new Tab("Configuration");
        configurationTab.setContent(new ConfigurationPane(iDataSets, newDataSets, positiveHypothesis, negativeHypothesis, targetProperty, targetValue, this));

        Tab resultTab = new Tab("Result");
        resultTab.setContent(new ResultPane(newDataSets, positiveHypothesis, negativeHypothesis, targetProperty, targetValue));

        tabPane.getTabs().addAll(configurationTab, resultTab);

        tabPane.setOnMouseClicked(event->
        {
            if(tabPane.getSelectionModel().getSelectedIndex() == 0)
            {
                configurationTab.setContent(new ConfigurationPane(iDataSets, newDataSets, positiveHypothesis, negativeHypothesis, targetProperty, targetValue, this));
            }
            if(tabPane.getSelectionModel().getSelectedIndex() == 1)
            {
                resultTab.setContent(new ResultPane(newDataSets, positiveHypothesis, negativeHypothesis, targetProperty, targetValue));
            }

            log.info(targetProperty != null ? "not null" : "null");
        });

        getChildren().add(tabPane);
    }

    public void setTargetProperty(IProperty targetProperty)
    {
        this.targetProperty = targetProperty;
    }

    public void setTargetValue(IValue targetValue)
    {
        this.targetValue = targetValue;
    }

    public void setHypothesis(List<Hypothesis> positiveHypothesis, List<Hypothesis> negativeHypothesis)
    {
        this.positiveHypothesis = positiveHypothesis;
        this.negativeHypothesis = negativeHypothesis;
    }

    public void setNewDataSets(IDataSets iDataSets)
    {
        this.newDataSets = iDataSets;
    }

    protected class ConfigurationPane extends Pane
    {
        protected ConfigurationPane(IDataSets iDataSets, IDataSets newDataSets, List<Hypothesis> positiveHypothesis, List<Hypothesis> negativeHypothesis, IProperty targetProperty, IValue targetValue, ExperimentTapPane experimentTapPane)
        {
            this.newDataSets = newDataSets;
            this.positiveHypothesis = positiveHypothesis;
            this.negativeHypothesis = negativeHypothesis;
            this.iDataSets = iDataSets;
            this.targetProperty = targetProperty;
            this.targetValue = targetValue;

            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/ui/fx/component/pane/experiment-tab.fxml"));

            try {
                Pane content = loader.load();
                ExperimentPaneController controller = loader.getController();
                controller.init(iDataSets, newDataSets, positiveHypothesis, negativeHypothesis, targetProperty, targetValue, experimentTapPane);
                this.getChildren().addAll(content);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private IDataSets iDataSets;
        private IDataSets newDataSets;
        private List<Hypothesis> positiveHypothesis;
        private List<Hypothesis> negativeHypothesis;
        private IProperty targetProperty;
        private IValue targetValue;
    }

    protected class ResultPane extends Pane
    {
        protected ResultPane(IDataSets newDataSets, List<Hypothesis> positiveHypothesis, List<Hypothesis> negativeHypothesis, IProperty targetProperty, IValue targetValue)
        {
            this.positiveHypothesis = positiveHypothesis;
            this.negativeHypothesis = negativeHypothesis;
            this.newDataSets = newDataSets;
            this.targetProperty = targetProperty;
            this.targetValue = targetValue;

            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/ui/fx/component/pane/experiment-result-tab.fxml"));

            try {
                Pane content = loader.load();
                ExperimentResultPaneController controller = loader.getController();
                controller.init(newDataSets, positiveHypothesis, negativeHypothesis, targetProperty, targetValue);
                this.getChildren().addAll(content);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private IDataSets newDataSets;
        private List<Hypothesis> positiveHypothesis;
        private List<Hypothesis> negativeHypothesis;
        private IProperty targetProperty;
        private IValue targetValue;
    }

    private static final Logger log = LoggerFactory.getLogger(ExperimentTapPane.class);
    private ConfigurationPane configurationPane;
    //private final ToggleGroup toggleGroup = new ToggleGroup();
    private IDataSets iDataSets;
    private IDataSets newDataSets;
    private List<Hypothesis> positiveHypothesis;
    private List<Hypothesis> negativeHypothesis;
    public IProperty targetProperty;
    private IValue targetValue;
}
