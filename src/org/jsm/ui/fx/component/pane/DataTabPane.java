package org.jsm.ui.fx.component.pane;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Side;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.*;
import org.jsm.model.IDataSets;
import org.jsm.ui.fx.component.controller.FormViewPaneController;
import org.jsm.ui.fx.component.controller.TableViewPaneController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author Anatoly Semenov
 *         Date: 04.01.2015
 */
public class DataTabPane extends TabPane {
    private static final Logger log = LoggerFactory.getLogger(DataTabPane.class);
    private IDataSets iDataSets;


    public DataTabPane(IDataSets iDataSets) {
        this.iDataSets = iDataSets;
        setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        setSide(Side.LEFT);
        setMaxHeight(Double.MAX_VALUE);
        setMaxWidth(Double.MAX_VALUE);
        getTabs().addAll(formTab(), tableTab());
    }

    private Tab formTab() {
        Tab tab = new Tab("Form View");
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/ui/fx/component/pane/data-tab-grid-pane.fxml"));
        try {
            Pane content = loader.load();
            FormViewPaneController controller = loader.getController();
            controller.initData(iDataSets);
            tab.setContent(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tab;
    }

    private Tab tableTab() {
        Tab tab = new Tab("Table View");
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/ui/fx/component/pane/data-tab-table-pane.fxml"));
        try {
            Pane content = loader.load();
            TableViewPaneController controller = loader.getController();
            controller.initData(iDataSets);
            tab.setContent(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tab;
    }
}
