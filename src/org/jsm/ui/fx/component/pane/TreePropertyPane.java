package org.jsm.ui.fx.component.pane;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import org.jsm.model.*;
import org.jsm.model.impl.property.TreeProperty;
import org.jsm.ui.fx.component.list.PropertiesList;
import org.jsm.ui.fx.FXConst;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import org.controlsfx.dialog.Dialogs;
import org.jsm.model.IValue;
import org.jsm.model.impl.property.SelectProperty;

import java.util.Collection;

import static org.jsm.ui.fx.util.UIUtils.idAsSelector;
import static org.jsm.common.Utils.*;


/**
 * @author Menshchikov Danil
 *         Date: 12.12.2014
 */
public class TreePropertyPane extends Pane
{
    private static final Logger log = LoggerFactory.getLogger(TreePropertyPane.class);
    private Scene mainScene;
    public static final String NEW_TREE_PROPERTY = "New tree property";
    public static final String UPDATE_TREE_PROPERTY = "Update tree property";
    public static final String CREATE = "Create";
    public static final String UPDATE = "Update";
    private boolean rootBool = false;
    public Collection<IValue> availableValues = Lists.newArrayList();
    public static IProperty rootTreeProperty;
    //public TreePane treePane;
    public IModel model;
    //static TreeProperty treePropertyOne;
    static Boolean multiple;
    static Boolean nullable;
    String title;
    public TreeView<IProperty> treePropertyTreeView;
    protected String nameButton;
    protected TreeProperty treeProperty;

    protected IModel getModel()
   {
        return model;
   }

    public TreePropertyPane(Scene mainScene, String title, String nameButton, TreeProperty treeProperty)
    {
        this.mainScene = mainScene;
        this.treeProperty = treeProperty;
        //this.rootTreeProperty = rootTreeProperty;
        this.title = title;
        this.treePropertyTreeView = new TreeView<IProperty>();
        this.nameButton = nameButton;

        treePropertyTreeView.setCellFactory(param -> new TreeViewCell());

        paint();

        if(nameButton.equals(TreePropertyPane.UPDATE))
        {
            paintTree();
        }
    }

    protected void setMultiple(Boolean isMultiple)
    {
        multiple = isMultiple;
    }

    protected void setNullable(Boolean nullable)
    {
        TreePropertyPane.nullable = nullable;
    }

    protected boolean isMultiple()
    {
        return multiple;
    }

    protected boolean isNullable()
    {
        return nullable;
    }

    protected void setRoot(ITreeProperty root)
    {
        treePropertyTreeView.setRoot(new TreeItem<IProperty>(root));
    }

    protected void paintTree()
    {
        setRoot((ITreeProperty) treeProperty);

        paintTreeElements(treePropertyTreeView.getRoot());
    }

    protected void paintTreeElements(TreeItem<IProperty> parentItem)
    {
        for(IProperty iProperty : ((ITreeProperty) parentItem.getValue()).getChildren())
        {
            TreeItem<IProperty> childItem = new TreeItem<IProperty>(iProperty);
            parentItem.getChildren().add(childItem);

            if ((iProperty instanceof ITreeProperty) && (((ITreeProperty) iProperty).getChildren() != null))
            {
                paintTreeElements(childItem);
            }
        }
    }

    protected void paint()
    {
        getChildren().clear();

        GridPane grid;

        grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(12);
        grid.setVgap(12);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Text sceneTitle = new Text(title);
        sceneTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(sceneTitle, 0, 0, 2, 1);

        //treePane = new TreePane(model, mainScene, iTreeProperty);

        TreePropertyPane p = this;

        Button create = new Button(nameButton);

        create.setOnAction((event)->
        {
            PropertiesList propertiesList = ((PropertiesList) TreePropertyPane.this.mainScene
                    .lookup(idAsSelector(FXConst.MODEL_TAB_PROPERTIES_LIST)));
            if (((Button) event.getSource()).getText().equals(CREATE)){
                propertiesList.addIProperty(rootTreeProperty);
            }else{
                propertiesList.changeProperty(treeProperty, rootTreeProperty);
            }

            grid.setVisible(false);
        });

        treePropertyTreeView.setOnMouseClicked(event ->
        {
            if(event.getButton().equals(MouseButton.PRIMARY) && (treePropertyTreeView.getRoot()!=null) && (treePropertyTreeView.getSelectionModel().getSelectedItem() != null))
            {
                grid.getChildren().clear();

                grid.add(sceneTitle, 0, 0, 2, 1);

                grid.add(treePropertyTreeView, 0, 1);

                grid.add(create, 0, 2);

                if(treePropertyTreeView.getRoot().equals(treePropertyTreeView.getSelectionModel().getSelectedItem()))
                {
                    log.info("add new root");

                    Text rootTitle = new Text("Update root");
                    rootTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

                    grid.add(rootTitle, 1, 0, 2, 1);

                    grid.add(new RootPane(TreePropertyPane.UPDATE, treePropertyTreeView.getRoot()), 1, 1);
                }
                else
                {
                    IProperty property = treePropertyTreeView.getSelectionModel().getSelectedItem().getValue();

                    if(property instanceof ITreeProperty)
                    {
                        log.info("update node");

                        Text nodeTitle = new Text("Update node");
                        nodeTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

                        grid.add(nodeTitle, 1, 0, 2, 1);

                        grid.add(new NotePane(treePropertyTreeView.getSelectionModel().getSelectedItem().getParent(), treePropertyTreeView.getSelectionModel().getSelectedItem(), TreePropertyPane.UPDATE), 1, 1);
                    }
                    else
                    {
                        log.info(property.getName());

                        log.info("update leaf");

                        Text leafTitle = new Text("Update leaf");
                        leafTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

                        grid.add(leafTitle, 1, 0, 2, 1);

                        grid.add(new LeafPane(treePropertyTreeView.getSelectionModel().getSelectedItem().getParent(), treePropertyTreeView.getSelectionModel().getSelectedItem(), TreePropertyPane.UPDATE), 1, 1);
                    }
                }
            }
        });

        treePropertyTreeView.setContextMenu(new ContextMenu() {
            {
                MenuItem addRoot = new MenuItem("Add root");
                MenuItem addNode = new MenuItem("Add node");
                MenuItem addLeaf = new MenuItem("Add leaf");

                addRoot.setOnAction(event -> {

                    grid.getChildren().clear();

                    grid.add(sceneTitle, 0, 0, 2, 1);

                    grid.add(treePropertyTreeView, 0, 1);

                    grid.add(create, 0, 2);

                    log.info("add new root");

                    rootBool = true;

                    Text rootTitle = new Text("Create root");
                    rootTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
                    grid.add(rootTitle, 1, 0, 2, 1);

                    grid.add(new RootPane(TreePropertyPane.CREATE, null), 1, 1);
                });

                addNode.setOnAction(event ->
                {
                    grid.getChildren().clear();

                    grid.add(sceneTitle, 0, 0, 2, 1);

                    grid.add(treePropertyTreeView, 0, 1);

                    grid.add(create, 0, 2);

                    log.info("add new node");

                    Text nodeTitle = new Text("Create node");
                    nodeTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

                    grid.add(nodeTitle, 1, 0, 2, 1);

                    grid.add(new NotePane(treePropertyTreeView.getSelectionModel().getSelectedItem(), null, TreePropertyPane.CREATE), 1, 1);
                    rootBool = true;
                });

                addLeaf.setOnAction(event ->
                {
                    grid.getChildren().clear();

                    grid.add(sceneTitle, 0, 0, 2, 1);

                    grid.add(treePropertyTreeView, 0, 1);

                    grid.add(create, 0, 2);

                    log.info("add new leaf");

                    Text leafTitle = new Text("Create leaf");
                    leafTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

                    grid.add(leafTitle, 1, 0, 2, 1);

                    grid.add(new LeafPane(treePropertyTreeView.getSelectionModel().getSelectedItem(), null, TreePropertyPane.CREATE), 1, 1);
                    rootBool = true;
                });

                if (rootBool == false) {
                    getItems().addAll(addRoot);
                } else {
                    getItems().addAll(addNode, addLeaf);
                }
            }
        });

        grid.add(treePropertyTreeView, 0, 1);

        grid.add(create, 0, 2);

        getChildren().addAll(grid);
    }

    public enum ValuesSet {
        PLUS_MINUS("+,-"), PLUS_MINUS_T("+,-,t");

        private final String s;

        ValuesSet(String s) {
            this.s = s;
        }

        public String asString() {
            return s;
        }

        public static ValuesSet forString(String s) {
            for (ValuesSet v : ValuesSet.values())
                if (v.asString().equals(s))
                    return v;
            return null;
        }

        public static ValuesSet forBoolean(boolean b) {
            return b ? PLUS_MINUS_T : PLUS_MINUS;
        }
    }

    protected class RootPane extends Pane
    {
        protected RootPane(String nameButton, TreeItem<IProperty> rootItem)
        {
            GridPane rootPane = new GridPane();
            rootPane.setAlignment(Pos.CENTER);
            rootPane.setHgap(10);
            rootPane.setVgap(10);

            Label rootName = new Label("Name:");
            rootPane.add(rootName, 0, 1);

            TextField rootNameTextField = new TextField();
            rootPane.add(rootNameTextField, 1, 1);
            if(rootItem != null)
            {
                rootNameTextField.setText(rootItem.getValue().getName());
            }

            Label propertyValuesLabel = new Label("Property values set:");
            rootPane.add(propertyValuesLabel, 0, 2);

            ChoiceBox<String> propertyValuesList = new ChoiceBox<>();
            ObservableList<String> v = FXCollections.unmodifiableObservableList(
                    FXCollections.observableArrayList(ValuesSet.PLUS_MINUS.asString(), ValuesSet.PLUS_MINUS_T.asString()));
            propertyValuesList.setItems(v);
            rootPane.add(propertyValuesList, 1, 2);

            if(rootItem != null)
            {
                propertyValuesList.getSelectionModel().select(ValuesSet.forBoolean(rootItem.getValue().isNullable()).asString());
            }

            Label multipleLabel = new Label("Is multiple:");
            rootPane.add(multipleLabel, 0, 3);

            ChoiceBox<Boolean> multipleListVariations = new ChoiceBox<>(
                    FXCollections.observableArrayList(Boolean.TRUE, Boolean.FALSE));

            if(rootItem != null)
            {
                multipleListVariations.setValue(rootItem.getValue().isMultiple());
            }

            rootPane.add(multipleListVariations, 1, 3);

            Button button = new Button(nameButton);

            button.setOnAction(event->
            {
                String name = rootNameTextField.getText();
                String isNullableString = propertyValuesList.getValue();
                ValuesSet set = ValuesSet.forString(isNullableString);
                Boolean isMultiple = multipleListVariations.getValue();

                try
                {
                    assertNotEmpty(name, "Name");
                    assertNotEmpty(set, "Property values set");
                    assertNotEmpty(isMultiple, "Is multiple");

                    if(rootItem != null)
                    {
                        rootTreeProperty = (ITreeProperty) TreeProperty.TreeFactory.treePropertyBuilder()
                                .setName(name)
                                .setNullable(ValuesSet.PLUS_MINUS_T.equals(set))
                                .setMultiple(isMultiple)
                                .build();

                        updateRootProperty(rootItem.getValue());

                        treePropertyTreeView.setRoot(new TreeItem<IProperty>(rootTreeProperty));

                        paintTreeElements(treePropertyTreeView.getRoot());

                        paint();
                    }
                    else
                    {
                        rootTreeProperty = (ITreeProperty) TreeProperty.TreeFactory.treePropertyBuilder()
                                .setName(name)
                                .setNullable(ValuesSet.PLUS_MINUS_T.equals(set))
                                .setMultiple(isMultiple)
                                .build();

                        treePropertyTreeView.setRoot(new TreeItem<IProperty>(rootTreeProperty));

                        paint();

                        log.info("create new root");
                    }
                }
                catch (Throwable th)
                {
                    log.error("Root create exception", th);
                    Dialogs.create()
                            .title("Root create error")
                            .message(th.getMessage())
                            .showError();
                }
            });

            rootPane.add(button, 1, 5);

            getChildren().addAll(rootPane);
        }

        public void updateRootProperty(IProperty oldProperty)
        {
            Collection<? extends IProperty> children = ((ITreeProperty) oldProperty).getChildren();

            for(IProperty child : children)
            {
                ((ITreeProperty) rootTreeProperty).addProperty(child);
            }
        }
    }

    private class NotePane extends Pane
    {
        NotePane(TreeItem<IProperty> parentItem, TreeItem<IProperty> nodeItem, String buttonName)
        {
            GridPane nodePane = new GridPane();
            nodePane.setAlignment(Pos.CENTER);
            nodePane.setHgap(10);
            nodePane.setVgap(10);

            Label rootName = new Label("Name:");
            nodePane.add(rootName, 0, 1);

            TextField rootNameTextField = new TextField();
            nodePane.add(rootNameTextField, 1, 1);

            if(nodeItem != null)
            {
                rootNameTextField.setText(nodeItem.getValue().getName());
            }

            Button create = new Button(buttonName);

            create.setOnAction(event->
            {
                String name = rootNameTextField.getText();

                try
                {
                    assertNotEmpty(name, "Name");

                    if(nodeItem != null)
                    {
                        IProperty iProperty = (ITreeProperty) TreeProperty.TreeFactory.treePropertyBuilder()
                                .setName(name)
                                .setMultiple(rootTreeProperty.isMultiple())
                                .setNullable(rootTreeProperty.isNullable())
                                .build();

                        updateNode((IProperty) nodeItem.getValue(), iProperty, (TreeProperty) parentItem.getValue());

                        treePropertyTreeView.getRoot().getChildren().clear();
                        treePropertyTreeView.setRoot(new TreeItem<IProperty>(rootTreeProperty));

                        paintTreeElements(treePropertyTreeView.getRoot());

                        paint();
                    }
                    else
                    {
                        ITreeProperty node = (ITreeProperty) TreeProperty.TreeFactory.treePropertyBuilder()
                                .setName(name)
                                .setMultiple(rootTreeProperty.isMultiple())
                                .setNullable(rootTreeProperty.isNullable())
                                .build();

                        ((ITreeProperty) parentItem.getValue()).addProperty(node);

                        parentItem.getChildren().add(new TreeItem<IProperty>(node));

                        paint();

                        log.info("create new node");
                    }
                }
                catch (Throwable th)
                {
                    log.error("Node create exception", th);
                    Dialogs.create()
                            .title("Node create error")
                            .message(th.getMessage())
                            .showError();
                }
            });

            nodePane.add(create, 1, 3);

            getChildren().addAll(nodePane);
        }

        public void updateNode(IProperty oldProperty, IProperty newProperty, ITreeProperty parent)
        {
            Collection<? extends IProperty> children = ((ITreeProperty) oldProperty).getChildren();

            for(IProperty child : children)
            {
                ((ITreeProperty) newProperty).addProperty(child);
            }

            parent.getChildren().remove(oldProperty);
            parent.addProperty(newProperty);
        }

        private final Logger log = LoggerFactory.getLogger(TreePropertyPane.class);
    }

    private class LeafPane extends Pane
    {
        LeafPane(TreeItem<IProperty> parentItem, TreeItem<IProperty> leafItem, String buttonName)
        {
            GridPane leafPane = new GridPane();
            leafPane.setAlignment(Pos.CENTER);
            leafPane.setHgap(12);
            leafPane.setVgap(12);

            Label leafName = new Label("Name:");
            leafPane.add(leafName, 0, 1);

            TextField leafNameTextField = new TextField();
            leafPane.add(leafNameTextField, 1, 1);

            if(leafItem != null)
            {
                leafNameTextField.setText(leafItem.getValue().getName());
            }

            Button create = new Button(buttonName);

            create.setOnAction(event->
            {
                String name = leafNameTextField.getText();

                try {
                    assertNotEmpty(name, "Name");

                    IProperty leaf = SelectProperty.Factory.selectPropertyBuilder()
                            .setName(name)
                            .setNullable(rootTreeProperty.isNullable())
                            .setMultiple(rootTreeProperty.isMultiple())
                            .build();

                    if(leafItem != null)
                    {
                        updateLeaf((IProperty) leafItem.getValue(), leaf, (TreeProperty) parentItem.getValue());

                        treePropertyTreeView.getRoot().getChildren().clear();
                        treePropertyTreeView.setRoot(new TreeItem<IProperty>(rootTreeProperty));

                        paintTreeElements(treePropertyTreeView.getRoot());

                        paint();
                    }
                    else
                    {
                        ((ITreeProperty) parentItem.getValue()).addProperty(leaf);

                        parentItem.getChildren().add(new TreeItem<IProperty>(leaf));

                        paint();

                        log.info("create new leaf" + parentItem.getValue().getName() + " - parent");
                    }
                }
                catch (Throwable th) {
                    log.error("Select property create exception", th);
                    Dialogs.create()
                            .title("Select property create error")
                            .message(th.getMessage())
                            .showError();
                }
            });

            leafPane.add(create, 1, 3);

            getChildren().addAll(leafPane);
        }

        public void updateLeaf(IProperty oldProperty, IProperty newProperty, ITreeProperty parent)
        {
            parent.getChildren().remove(oldProperty);
            parent.addProperty(newProperty);
        }

        private final Logger log = LoggerFactory.getLogger(TreePropertyPane.class);
    }

    protected class TreeViewCell extends TreeCell<IProperty>
    {
        @Override
        protected void updateItem(IProperty item, boolean empty)
        {
            super.updateItem(item, empty);

            if ( empty || item == null )
            {

                setText(null);
                setGraphic(null);
            }
            else
            {
                Label propertyName = new Label(item.getName());
                propertyName.setMinWidth(50);
                // setText("fsdf");
                setGraphic(new HBox(propertyName));
            }
        }
    }
}