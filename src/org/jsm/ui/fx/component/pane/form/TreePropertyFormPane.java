package org.jsm.ui.fx.component.pane.form;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.VBox;
import org.jsm.model.*;
import org.jsm.model.impl.property.TreeProperty;
import org.jsm.common.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Collections2.transform;

/**
 * @author Anatoly Semenov
 *         Date: 17.12.2014
 */
class TreePropertyFormPane extends PropertyFormPane
{
    private final ITreeProperty treeProperty;
    private final TreeView<String> treeView = new TreeView<>();
    private static final Logger log = LoggerFactory.getLogger(TreePropertyFormPane.class);
    private int treeSize;

    protected TreePropertyFormPane(ITreeProperty iTreeProperty) {
        super();
        this.treeProperty = iTreeProperty;
    }

    @Override
    public IDataItem getDataItem() {
        return null;
    }

    @Override
    public void init(@Nullable IDataItem dataItem)
    {
        VBox vBox = new VBox();
        vBox.getChildren().add(new Label(treeProperty.getName()));


        treeSize = 0;

        if(treeProperty.isNullable())
        {

        }
        else
        {

        }

        CheckBoxTreeItem<String> root = new CheckBoxTreeItem<>(treeProperty.getName());
        root.getChildren().addAll(transform(treeProperty.getPropertyValues(), VALUE_CHECK_BOX_TREE_ITEM_FUNCTION));
        root.getChildren().addAll(propertiesToCheckBoxTreeItem(treeProperty.getChildren()));
        root.setExpanded(true);
        root.setIndeterminate(treeProperty.isNullable());
        root.setIndependent(true);

        treeView.setOnMouseClicked(event->
        {
            if(!treeView.getRoot().isExpanded())
            {
                treeView.setPrefSize(280, treeSize * 25);
            }
            else
            {
                treeView.setPrefSize(280, 25);
            }
        });

        /*root.getGraphic().setOnMouseClicked(event->
        {
            if(root.isExpanded())
            {
                treeView.setPrefSize(280, treeSize * 30);
            }
            else
            {
                treeView.setPrefSize(280, 30);
            }
        });*/

        treeView.setRoot(root);
        treeView.setCellFactory(CheckBoxTreeCell.forTreeView());
        //treeView.setPadding(new Insets(5, 5, 5, 5));
        this.setPadding(new Insets(5, 5, 5, 5));
        //treeView.setMinSize(280, 40);

        treeSize++;

        if(treeView.getRoot().getChildren() != null)
        {
            treeView.setPrefSize(280, treeSize * 25);
        }
        else
        {
            treeView.setPrefSize(280, 25);
        }

        //treeView.setSc

        //treeView.setStyle("-fx-background-color: grey");

        //log.info("size = " + size);
        getChildren().addAll(treeView);
    }

   /* private TreeView<RadioButton> createRadioButtonTreeView(Collection<? extends IProperty> children)
    {

    }*/

    private List<TreeItem<String>> propertiesToCheckBoxTreeItem(Collection<? extends IProperty> children) {
        List<TreeItem<String>> result = Lists.newArrayList();
        for (IProperty child : Utils.nvl(children, Collections.<IProperty>emptyList())) {
            CheckBoxTreeItem<String> nameCheckBox = new CheckBoxTreeItem<>(child.getName());
            nameCheckBox.getChildren().addAll(transform(Utils.nvl(child.getPropertyValues(), Collections.<IValue>emptyList()), VALUE_CHECK_BOX_TREE_ITEM_FUNCTION));
            if (child instanceof ITreeProperty)
                nameCheckBox.getChildren().addAll(propertiesToCheckBoxTreeItem(((ITreeProperty) child).getChildren()));
            nameCheckBox.setExpanded(true);
            nameCheckBox.setIndeterminate(treeProperty.isNullable());
            nameCheckBox.setIndependent(true);
            result.add(nameCheckBox);

            size++;
        }
        return result;
    }

        /* private List<CheckBoxTreeItem<String>> propertiesToCheckBoxTreeItem(Collection<? extends IProperty> children) {
        List<CheckBoxTreeItem<String>> result = Lists.newArrayList();
        for (IProperty child : nvl(children, Collections.<IProperty>emptyList())) {
            CheckBoxTreeItem<String> nameCheckBox = new CheckBoxTreeItem<>(child.getName());
            nameCheckBox.getChildren().addAll(transform(nvl(child.getPropertyValues(), Collections.<IValue>emptyList()), VALUE_CHECK_BOX_TREE_ITEM_FUNCTION));

            if (child instanceof ITreeProperty)
            {
                nameCheckBox.getChildren().addAll(propertiesToCheckBoxTreeItem(((ITreeProperty) child).getChildren()));

                /*nameCheckBox.getGraphic().setOnMouseClicked(event->
                {
                    if(nameCheckBox.isExpanded())
                    {
                        treeView.setPrefSize(280, treeSize * 30);
                    }
                    else
                    {
                        //treeView.setPrefSize(280, 30);
                    }
                });*/
          /*  }

            nameCheckBox.setExpanded(true);
            nameCheckBox.setIndeterminate(treeProperty.isNullable());
            nameCheckBox.setIndependent(true);

            result.add(nameCheckBox);

            treeSize++;
        }
        return result;
    }

    /*private static class SingleTreePropertyForm extends TreePropertyFormPane {

        private final ToggleGroup toggleGroup = new ToggleGroup();

        private SingleTreePropertyForm(TreeProperty treeProperty) {
            super(treeProperty);
        }

        @Override
        protected List<ButtonBase> buttonsList(@Nullable IDataItem dataItem) {
            List<ButtonBase> radioButtons = Lists.newArrayList();

            for(IProperty iProperty : treeProperty.getChildren())
            {

            }

            for (IValue iValue : treeProperty.getPropertyValues()) {
                RadioButton radioButton = new RadioButton();
                radioButton.setText(String.valueOf(iValue));
                radioButton.setToggleGroup(toggleGroup);
                if (dataItem != null) {
                    radioButton.setSelected(
                            nvl(dataItem.getPositiveValues(), Collections.<IValue>emptyList()).contains(iValue));
                }
                radioButtons.add(radioButton);
            }
            return radioButtons;
        }

        @Override
        public IDataItem getDataItem() {
            String selectedString = ((ToggleButton)toggleGroup.getSelectedToggle()).getText();
            IValue selectedValue = new StringValue(selectedString);
            Collection<IValue> negative = Lists.newArrayList();
            Collection<IValue> undef = Lists.newArrayList();
            Collection<IValue> nonSelected = Collections2.transform(
                    toggleGroup.getToggles().filtered(toggle -> !toggle.isSelected()),
                    input -> new StringValue(((ToggleButton) input).getText()));
            if (selectProperty.isNullable())
                undef.addAll(nonSelected);
            else
                negative.addAll(nonSelected);
            return new DataItem(selectProperty, Collections.singletonList(selectedValue), negative, undef);
        }
    }*/

    private static class SingleTreePropertyFormPane extends TreePropertyFormPane
    {
        static ITreeProperty iTreeProperty;

        protected SingleTreePropertyFormPane(ITreeProperty iTreeProperty)
        {
            super(iTreeProperty);

            this.iTreeProperty = iTreeProperty;

            ListView<TreeView<RadioButton>> listView = new ListView<>();

            //List<Tr>

            for(IProperty property : iTreeProperty.getChildren())
            {

            }

            VBox vBox = new VBox();
            vBox.getChildren().add(new Label(iTreeProperty.getName()));

           // createTree();
        }

       // @Override
        protected List<TreeItem<RadioButton>> createTreeView(@Nullable IDataItem dataItem)
        {
            List<TreeItem<RadioButton>> listTreeItem = Lists.newArrayList();

           /* for()
            {

            }*/

            TreeItem<RadioButton> root = new TreeItem<RadioButton>();

            return listTreeItem;
        }

       /* @Override
        public IDataItem getDataItem()
        {
            Collection<IValue> positive = Lists.newArrayList();
            Collection<IValue> negative = Lists.newArrayList();
            Collection<IValue> undef = Lists.newArrayList();
        }*/

        protected void createTree(Collection<? extends IProperty> children)
        {
          /*  for()
            {

            }*/
        }

        protected void createTree(TreeItem<RadioButton> node, Collection<? extends IProperty> children)
        {
            ToggleGroup toggleGroup = new ToggleGroup();

            for(IProperty property : children)
            {
                RadioButton radioButton = new RadioButton();
                radioButton.setText(property.getName());
                radioButton.setToggleGroup(toggleGroup);

                TreeItem<RadioButton> item = new TreeItem<>(radioButton);

                node.getChildren().add(item);

                if(property instanceof ITreeProperty)
                {
                    createTree(item, ((ITreeProperty) property).getChildren());
                }
            }
        }
    }

    public static class Factory {
        public static TreePropertyFormPane forProperty(ITreeProperty iTreeProperty) {
            return new TreePropertyFormPane(iTreeProperty);
        }
    }

    private static final Function<IValue, CheckBoxTreeItem<String>> VALUE_CHECK_BOX_TREE_ITEM_FUNCTION = new Function<IValue, CheckBoxTreeItem<String>>() {
        @Override
        public CheckBoxTreeItem<String> apply(IValue input) {
            return new CheckBoxTreeItem<>(String.valueOf(input.getValue()));
        }
    };

    static TreePropertyFormPane forProperty(TreeProperty treeProperty, IDataSet iDataSet)
    {
        TreePropertyFormPane TreePropertyFormPane = new TreePropertyFormPane(treeProperty);



        /*if(treeProperty.isMultiple())
        {

        }
        else
        {

        }

        return null;*/
        return TreePropertyFormPane;
    }

    /*protected class ChoiceBoxTreeItem extends TreeItem<ButtonBase>
    {

    }*/
}
