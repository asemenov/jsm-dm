package org.jsm.ui.fx.component.pane.form;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import org.jsm.model.IDataItem;
import org.jsm.model.IDataSet;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.model.impl.data.DataItem;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;

import javax.annotation.Nullable;
import java.util.*;

import static org.jsm.common.Utils.nvl;

/**
 * @author Anatoly Semenov
 *         Date: 17.12.2014
 */
abstract class SelectPropertyFormPane extends PropertyFormPane {
    protected final SelectProperty selectProperty;
   // public int size = 0;

    protected SelectPropertyFormPane(SelectProperty selectProperty) {
        super();
        this.selectProperty = selectProperty;
    }

    public void init(@Nullable IDataItem dataItem) {
        VBox vBox = new VBox();
        vBox.getChildren().add(new Label(selectProperty.getName()));
        for (Object buttonBase : buttonsList(dataItem)) {
            VBox.setMargin((ButtonBase)buttonBase, new Insets(0, 0, 0, 16));
            vBox.getChildren().add((ButtonBase) buttonBase);

            size++;
        }
        size++;
        vBox.setPadding(new Insets(5, 5, 5, 5));
        getChildren().add(vBox);

        //setPadding(new Insets(5, 5, 5, 5));
    }

    protected abstract List<? extends ButtonBase> buttonsList(@Nullable IDataItem dataItem);

    private static class SingleSelectPropertyForm extends SelectPropertyFormPane {

        private final ToggleGroup toggleGroup = new ToggleGroup();

        private SingleSelectPropertyForm(SelectProperty selectProperty) {
            super(selectProperty);
        }

        @Override
        protected List<ButtonBase> buttonsList(@Nullable IDataItem dataItem) {
            List<ButtonBase> radioButtons = Lists.newArrayList();
            for (IValue iValue : selectProperty.getPropertyValues()) {
                RadioButton radioButton = new RadioButton();
                radioButton.setText(String.valueOf(iValue));
                radioButton.setToggleGroup(toggleGroup);
                if (dataItem != null) {
                    radioButton.setSelected(
                            nvl(dataItem.getPositiveValues(), Collections.<IValue>emptyList()).contains(iValue));
                }
                radioButtons.add(radioButton);
            }
            return radioButtons;
        }

        @Override
        public IDataItem getDataItem() {
            String selectedString = ((ToggleButton)toggleGroup.getSelectedToggle()).getText();
            IValue selectedValue = new StringValue(selectedString);
            Collection<IValue> negative = Lists.newArrayList();
            Collection<IValue> undef = Lists.newArrayList();
            Collection<IValue> nonSelected = Collections2.transform(
                    toggleGroup.getToggles().filtered(toggle -> !toggle.isSelected()),
                    input -> new StringValue(((ToggleButton)input).getText()));
            if (selectProperty.isNullable())
                undef.addAll(nonSelected);
            else
                negative.addAll(nonSelected);
            return new DataItem(selectProperty, Collections.singletonList(selectedValue), negative, undef);
        }
    }

    private static class MultipleSelectPropertyForm extends SelectPropertyFormPane {

        private final List<CheckBox> buttonBases = Lists.newArrayList();

        private MultipleSelectPropertyForm(SelectProperty selectProperty) {
            super(selectProperty);
            for (IValue iValue : selectProperty.getPropertyValues()) {
                CheckBox checkBox = new CheckBox(String.valueOf(iValue.getValue()));
                checkBox.setAllowIndeterminate(selectProperty.isNullable());
                buttonBases.add(checkBox);
            }
        }

        @Override
        protected List<? extends ButtonBase> buttonsList(IDataItem dataItem) {
            if (dataItem != null) {
                for (CheckBox checkBox : buttonBases) {
                    IValue checkBoxValue = IValue.Factory.createStringValue(checkBox.getText());
                    if (nvl(dataItem.getNegativeValues(), Collections.<IValue>emptyList()).contains(checkBoxValue)) {
                        checkBox.setIndeterminate(true);
                    }
                    else if (nvl(dataItem.getPositiveValues(), Collections.<IValue>emptyList()).contains(checkBoxValue)) {
                        checkBox.setSelected(true);
                    }
                }
            }
            return buttonBases;
        }

        @Override
        public IDataItem getDataItem() {
            Collection<IValue> positive = Lists.newArrayList();
            Collection<IValue> negative = Lists.newArrayList();
            Collection<IValue> undef = Lists.newArrayList();
            for (CheckBox checkBox : buttonBases) {
                StringValue stringValue = new StringValue(checkBox.getText());
                if (checkBox.isIndeterminate())
                    negative.add(stringValue);
                else if (checkBox.isSelected())
                    positive.add(stringValue);
                else if (checkBox.isAllowIndeterminate())
                    undef.add(stringValue);
                else
                    negative.add(stringValue);
            }
            return new DataItem(selectProperty, positive, negative, undef);
        }
    }

    static SelectPropertyFormPane forProperty(SelectProperty selectProperty) {
        return selectProperty.isMultiple() ? new MultipleSelectPropertyForm(selectProperty) :
                new SingleSelectPropertyForm(selectProperty);
    }

    static SelectPropertyFormPane forProperty(SelectProperty selectProperty, IDataSet iDataSet)
    {
        if(selectProperty.isMultiple())
        {
            MultipleSelectPropertyForm multipleSelectPropertyForm = new MultipleSelectPropertyForm(selectProperty);

            Set<Map.Entry<IProperty, IDataItem>> set = iDataSet.getDataItems().entrySet();
            Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

            while(iterator.hasNext())
            {
                Map.Entry<IProperty, IDataItem> entry = iterator.next();

                multipleSelectPropertyForm.buttonsList(entry.getValue());
            }


            /*for(IDataItem iDataItem : iDataSet.getDataItems())
            {
                multipleSelectPropertyForm.buttonsList(iDataItem);
            }*/

            return multipleSelectPropertyForm;
        }

        else
        {
            SingleSelectPropertyForm singleSelectPropertyForm = new SingleSelectPropertyForm(selectProperty);

            Set<Map.Entry<IProperty, IDataItem>> set = iDataSet.getDataItems().entrySet();
            Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

            while(iterator.hasNext())
            {
                Map.Entry<IProperty, IDataItem> entry = iterator.next();

                singleSelectPropertyForm.buttonsList(entry.getValue());
            }

            return singleSelectPropertyForm;
        }
    }
}
