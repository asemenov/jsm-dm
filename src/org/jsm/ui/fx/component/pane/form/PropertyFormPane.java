package org.jsm.ui.fx.component.pane.form;

import javafx.scene.layout.Pane;
import org.jsm.model.IDataItem;
import org.jsm.model.IDataSet;
import org.jsm.model.IProperty;
import org.jsm.model.ITreeProperty;
import org.jsm.model.impl.property.SelectProperty;

import javax.annotation.Nullable;

/**
 * @author Anatoly Semenov
 *         Date: 17.12.2014
 */
public abstract class PropertyFormPane extends Pane {
    protected PropertyFormPane() {
        super();
    }

    int size;
    //public abstract IDataItem getDataItem();
    public abstract IDataItem getDataItem();
    public abstract void init(@Nullable IDataItem dataItem);

    public int getSize()
    {
        return size;
    }

    public static class Factory {
        public static PropertyFormPane newSelectPropertyFormPane(SelectProperty selectProperty) {
            return SelectPropertyFormPane.forProperty(selectProperty);
        }

        public static PropertyFormPane newSelectPropertyFormPane(SelectProperty selectProperty, IDataSet iDataSet) {
            return SelectPropertyFormPane.forProperty(selectProperty, iDataSet);
        }

        public static PropertyFormPane newTreePropertyPane(ITreeProperty iTreeProperty) {
            return TreePropertyFormPane.Factory.forProperty(iTreeProperty);
        }

        public static PropertyFormPane newPropertyFormPane(IProperty iProperty) {
            if (iProperty instanceof SelectProperty)
                return newSelectPropertyFormPane((SelectProperty) iProperty);
            else if (iProperty instanceof ITreeProperty)
                return newTreePropertyPane((ITreeProperty) iProperty);
            else
                throw new IllegalArgumentException("unsupported property type " + iProperty.getClass().getName());
        }

        public static PropertyFormPane newPropertyFormPane(IProperty iProperty, IDataSet iDataSet) {
            if (iProperty instanceof SelectProperty)
                return newSelectPropertyFormPane((SelectProperty) iProperty, iDataSet);
            else if (iProperty instanceof ITreeProperty)
                return newTreePropertyPane((ITreeProperty) iProperty);
            else
                throw new IllegalArgumentException("unsupported property type " + iProperty.getClass().getName());
        }
    }
}
