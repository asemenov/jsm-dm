package org.jsm.ui.fx.component.pane.form;

import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.jsm.model.IDataItem;
import org.jsm.model.IProperty;
import org.jsm.trash.resolver.impl.Hypothesis;

import java.util.Map;

/**
 * Created by Данил on 13.06.2015.
 */
public class HypothesisFormPane extends Pane
{
    public HypothesisFormPane(Hypothesis hypothesis, int id)
    {
        this.hypothesis = hypothesis;
        this.id = id;
    }

    public void init()
    {
        VBox vBox = new VBox();
        vBox.getChildren().add(new Label("Гипотеза №" + id));

        getChildren().add(vBox);

        TableView<Map<IProperty, IDataItem>> tableView = new TableView<Map<IProperty, IDataItem>>();

        tableView.getItems().clear();
        //tableView.getItems().addAll(hypothesis.getSources());

        tableView.getColumns().add(new TableColumn<>("Property"));
        tableView.getColumns().add(new TableColumn<>("Value"));

        //Set<Map.Entry<IProperty, IDataItem>> set = hypothesis.getSources().entrySet();
    }

    private Hypothesis hypothesis;
    private int id;
}
