package org.jsm.ui.fx.component.controller;

import com.sun.javafx.binding.StringConstant;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import org.jsm.common.Utils;
import org.jsm.model.*;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.trash.resolver.impl.Hypothesis;
import org.jsm.ui.fx.component.pane.form.PropertyFormPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.*;

/**
 * Created by Данил on 15.06.2015.
 */
public class ExperimentResultPaneController implements Initializable
{
    private static final Logger log = LoggerFactory.getLogger(ExperimentResultPaneController.class);

    @FXML
    private ListView positiveListView;

    @FXML
    private ListView negativeListView;

    @FXML
    private TableView<IDataSet> resultDataSetsTable;

    @FXML
    private GridPane gridPaneFX;

    public ExperimentResultPaneController()
    {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        log.info("initialize {}, {}", location, resources);
    }

    public void init(IDataSets newDataSets, List<Hypothesis> positiveHypothesis, List<Hypothesis> negativeHypothesis, IProperty targetProperty, IValue targetValue)
    {
        this.newDataSets = newDataSets;
        this.positiveHypothesis = positiveHypothesis;
        this.negativeHypothesis = negativeHypothesis;
        this.targetProperty = targetProperty;
        this.targetValue = targetValue;

        log.info("aaa");

        gridPaneFX.setMaxSize(Screen.getPrimary().getVisualBounds().getWidth() - 100, Screen.getPrimary().getVisualBounds().getHeight() - 100);

        if(targetProperty == null)
        {

        }
        else
        {
            positiveListView.getItems().clear();

            positiveListView.setOnMouseClicked(event->
            {
                log.info(positiveListView.getItems().size() + "");
            });

            int i = 0;

            int coutItem = 0;

            for(Hypothesis aHypothesis : this.positiveHypothesis)
            {
                Set<Map.Entry<IProperty, IDataItem>> set = aHypothesis.getSources().entrySet();
                Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                TreeView positiveTreeView = new TreeView();

                TreeItem root = new TreeItem("Гипотеза №" + (i + 1));

                coutItem++;

                positiveTreeView.setRoot(root);

                while(iterator.hasNext())
                {
                    Map.Entry entry = iterator.next();

                    TreeItem item = new TreeItem(((IProperty) entry.getKey()).getName());

                    positiveTreeView.getRoot().getChildren().add(item);

                    if(((IProperty) entry.getKey()) instanceof SelectProperty)
                    {
                        if(((IProperty) entry.getKey()).isMultiple())
                        {
                            for(IValue aValue : ((IProperty) entry.getKey()).getPropertyValues())
                            {
                                CheckBox checkBox = new CheckBox(aValue.toString());

                                for(IValue currentValue : ((IDataItem) entry.getValue()).getPositiveValues())
                                {
                                    if(currentValue.equals(aValue))
                                    {
                                        checkBox.setSelected(true);

                                        break;
                                    }
                                }

                                TreeItem leaf = new TreeItem(checkBox);

                                item.getChildren().add(leaf);
                            }
                        }
                        else
                        {
                            for(IValue aValue : ((IProperty) entry.getKey()).getPropertyValues())
                            {
                                RadioButton radioButton = new RadioButton(aValue.toString());

                                for(IValue currentValue : ((IDataItem) entry.getValue()).getPositiveValues())
                                {
                                    if(currentValue.equals(aValue))
                                    {
                                        radioButton.setSelected(true);

                                        break;
                                    }
                                }

                                TreeItem leaf = new TreeItem(radioButton);

                                item.getChildren().add(leaf);
                            }
                        }
                    }



                    coutItem++;

                    log.info(((IProperty) entry.getKey()).getName());
                }

                int cellSize = 20;
                positiveTreeView.setFixedCellSize(cellSize);
                positiveTreeView.setPrefHeight(cellSize + 5);

                positiveTreeView.setOnMouseClicked(mouseEvent->
                {
                    if(positiveTreeView.getRoot().isExpanded())
                    {
                        positiveTreeView.setPrefHeight(cellSize*(positiveTreeView.getRoot().getChildren().size() + 1) + 5);
                    }
                    else
                    {
                        positiveTreeView.setPrefHeight(cellSize + 5);
                    }
                });

                positiveListView.getItems().add(positiveTreeView);

                i++;
            }

            negativeListView.getItems().clear();

            i = 0;

            coutItem = 0;

            for(Hypothesis aHypothesis : this.negativeHypothesis)
            {
                Set<Map.Entry<IProperty, IDataItem>> set = aHypothesis.getSources().entrySet();
                Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                TreeView negativeTreeView = new TreeView();

                TreeItem root = new TreeItem("Гипотеза №" + (i + 1));

                coutItem++;

                negativeTreeView.setRoot(root);

                while(iterator.hasNext())
                {
                    Map.Entry entry = iterator.next();

                    TreeItem item = new TreeItem(((IProperty) entry.getKey()).getName());

                    negativeTreeView.getRoot().getChildren().add(item);

                    if(((IProperty) entry.getKey()) instanceof SelectProperty)
                    {
                        if(((IProperty) entry.getKey()).isMultiple())
                        {
                            for(IValue aValue : ((IProperty) entry.getKey()).getPropertyValues())
                            {
                                CheckBox checkBox = new CheckBox(aValue.toString());

                                for(IValue currentValue : ((IDataItem) entry.getValue()).getPositiveValues())
                                {
                                    if(currentValue.equals(aValue))
                                    {
                                        checkBox.setSelected(true);

                                        break;
                                    }
                                }

                                TreeItem leaf = new TreeItem(checkBox);

                                item.getChildren().add(leaf);
                            }
                        }
                        else
                        {
                            for(IValue aValue : ((IProperty) entry.getKey()).getPropertyValues())
                            {
                                RadioButton radioButton = new RadioButton(aValue.toString());

                                for(IValue currentValue : ((IDataItem) entry.getValue()).getPositiveValues())
                                {
                                    if(currentValue.equals(aValue))
                                    {
                                        radioButton.setSelected(true);

                                        break;
                                    }
                                }

                                TreeItem leaf = new TreeItem(radioButton);

                                item.getChildren().add(leaf);
                            }
                        }
                    }

                    coutItem++;

                    log.info(((IProperty) entry.getKey()).getName());
                }

                int cellSize = 20;
                negativeTreeView.setFixedCellSize(cellSize);
                negativeTreeView.setPrefHeight(cellSize + 5);

                negativeTreeView.setOnMouseClicked(mouseEvent->
                {
                    if(negativeTreeView.getRoot().isExpanded())
                    {
                        negativeTreeView.setPrefHeight(cellSize*(negativeTreeView.getRoot().getChildren().size() + 1) + 5);
                    }
                    else
                    {
                        negativeTreeView.setPrefHeight(cellSize + 5);
                    }
                });

                negativeListView.getItems().add(negativeTreeView);

                i++;

                resultDataSetsTable.getItems().clear();
                resultDataSetsTable.getItems().addAll(newDataSets.getDataSets());

                resultDataSetsTable.getColumns().add(createTextColumn(TextColumn.NAME));
                resultDataSetsTable.getColumns().add(createTextColumn(TextColumn.COMMENT));

                for (IProperty property : newDataSets.getModel().getProperties())
                {
                    if(property.equals(targetProperty))
                    {
                        resultDataSetsTable.getColumns().add(createTargetPropertyColumn(property));
                    }
                    else
                    {
                        resultDataSetsTable.getColumns().add(createPropertyColumn(property));
                    }

                }

            /*dataSetTableView.getColumns().addAll(
                    iDataSets.getModel().getProperties().stream().map(this::createPropertyColumn).findAny().get());*/

                for (TableColumn column : resultDataSetsTable.getColumns()) {
                    column.setPrefWidth(150);
                }

                /*for(IProperty iProperty : newDataSets.getModel().getProperties())
                {
                    TableColumn<IDataSet, String> column = new TableColumn<IDataSet, String>(iProperty.getName());
                    column.setCellFactory(TableViewCell::new);
                    resultDataSetsTable.getColumns()
                            .add(column);
                }*/



               /* for(IDataSet iDataSet : newDataSets.getDataSets())
                {
                    resultDataSetsTable.getItems().add(iDataSet);

                /*if(dataSetMap.get(iDataSet))
                {
                   // resultDataSetsTable.getColumns().get().
                }*/
                //}*/
            }
        }
    }

    protected class TableViewCell extends TableCell<IDataSet, String>
    {
        private final TableColumn<IDataSet, String> column;

        private TableViewCell(TableColumn<IDataSet, String> column) {
            this.column = column;
        }

        @Override
        public void startEdit() {
            super.startEdit(); //todo show edit pane with AV
            IProperty prop = newDataSets.getModel().getProperties().stream()
                    .filter(property -> property.getName().equals(column.getText()))
                    .findFirst()
                    .get();

            if (prop != null)
            {
                IDataSet dataSet = newDataSets.getDataSets().stream().skip(this.getTableRow().getIndex()).findFirst().get();

                if(prop.getName().equals(targetProperty.getName()))
                {

                    if(dataSet.getDataItems().get(targetProperty).getPositiveValues().size() > 0)
                    {
                        for(IValue value : dataSet.getDataItems().get(targetProperty).getPositiveValues())
                        {
                            if(value.equals(targetValue))
                            {
                                setText("Да");
                                setColor(Color.GREEN);
                            }
                        }
                    }

                    if(dataSet.getDataItems().get(targetProperty).getNegativeValues().size() > 0)
                    {
                        for(IValue value : dataSet.getDataItems().get(targetProperty).getNegativeValues())
                        {
                            if(value.equals(targetValue))
                            {
                                setText("Нет");
                                setColor(Color.RED);
                            }
                        }
                    }

                    if(dataSet.getDataItems().get(targetProperty).getUndefinedValues().size() > 0)
                    {
                        for(IValue value : dataSet.getDataItems().get(targetProperty).getUndefinedValues())
                        {
                            if(value.equals(targetValue))
                            {
                                setText("Незвестно");
                                setColor(Color.YELLOW);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(item);
            setGraphic(null);
        }

        public void setColor(Color property)
        {

        }
    }

    private TableColumn<IDataSet, String> createTextColumn(TextColumn textColumn) {
        TableColumn<IDataSet, String> column = new TableColumn<>(textColumn.getColumnName());
        column.setCellValueFactory(param -> StringConstant.valueOf(textColumn.getDataSetValue(param.getValue())));
        column.setCellFactory(TextFieldTableCell.forTableColumn());
        column.setOnEditCommit(event -> textColumn.setDataSetValue(event.getRowValue(), event.getNewValue()));
        return column;
    }

    private enum TextColumn {
        NAME("Name") {
            @Override
            public String getDataSetValue(IDataSet dataSet) {
                return dataSet.getName();
            }

            @Override
            public void setDataSetValue(IDataSet dataSet, String value) {
                dataSet.setName(value);
            }
        },
        COMMENT("Comment") {
            @Override
            public String getDataSetValue(IDataSet dataSet) {
                return dataSet.getComment();
            }

            @Override
            public void setDataSetValue(IDataSet dataSet, String value) {
                dataSet.setComments(value);
            }
        };

        private String columnName;

        private TextColumn(String columnName) {
            this.columnName = columnName;
        }

        public String getColumnName() {
            return columnName;
        }

        public abstract String getDataSetValue(IDataSet dataSet);

        public abstract void setDataSetValue(IDataSet dataSet, String value);
    }

    private class PropertyEditableCell extends TableCell<IDataSet, String> {
        private final TableColumn<IDataSet, String> column;
        private PropertyFormPane formPane;
        private IDataItem dataItem;

        private PropertyEditableCell(TableColumn<IDataSet, String> column) {
            this.column = column;
        }

        @Override
        public void startEdit() {
            super.startEdit(); //todo show edit pane with AV

            IProperty prop = newDataSets.getModel().getProperties().stream()
                    .filter(property -> property.getName().equals(column.getText()))
                    .findFirst()
                    .get();
            if (prop != null)
            {
                if(prop.getName().equals(targetProperty.getName()))
                {
                    IDataSet dataSet = newDataSets.getDataSets().stream().skip(this.getTableRow().getIndex()).findFirst().get();

                    boolean isTrue = true;

                    if(dataSet.getDataItems().get(targetProperty).getPositiveValues().size() > 0)
                    {
                        for(IValue value : dataSet.getDataItems().get(targetProperty).getPositiveValues())
                        {
                            if(value.equals(targetValue))
                            {
                                setText("Да");

                                isTrue = false;
                            }
                        }
                    }

                    if(isTrue)
                    {
                        if(dataSet.getDataItems().get(targetProperty).getNegativeValues().size() > 0)
                        {
                            for(IValue value : dataSet.getDataItems().get(targetProperty).getNegativeValues())
                            {
                                if(value.equals(targetValue))
                                {
                                    setText("Нет");

                                    isTrue = false;
                                }
                            }
                        }
                    }

                    if(isTrue)
                    {
                        if(dataSet.getDataItems().get(targetProperty).getUndefinedValues().size() > 0)
                        {
                            for(IValue value : dataSet.getDataItems().get(targetProperty).getUndefinedValues())
                            {
                                if(value.equals(targetValue))
                                {
                                    setText("Не известно");

                                    isTrue = false;
                                }
                            }
                        }
                    }
                    //setStyle("-fx-color: black;");
                }
                else
                {
                    formPane = PropertyFormPane.Factory.newPropertyFormPane(prop);
                    IDataSet dataSet = newDataSets.getDataSets().stream().skip(this.getTableRow().getIndex()).findFirst().get();

                    Set<Map.Entry<IProperty, IDataItem>> set = dataSet.getDataItems().entrySet();
                    Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                    while (iterator.hasNext()) {
                        Map.Entry<IProperty, IDataItem> entry = iterator.next();

                        if (entry.getKey().equals(prop)) {
                            dataItem = entry.getValue();
                        }
                    }

                    //dataItem = dataSet.getDataItems().stream().filter(iDataItem -> iDataItem.getIProperty().equals(prop)).findFirst().get();
                    formPane.init(dataItem);
                    setText(null);
                    setGraphic(formPane);
                    formPane.requestFocus();
                }

            }
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            if (!formPane.getDataItem().equals(dataItem)) {

            }
            setGraphic(null);
            setText(valuesAsString(dataItem.getPositiveValues()));
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(item);
            setGraphic(null);
        }
    }

    private String valuesAsString(Collection<IValue> values) {
        if (Utils.isEmpty(values))
            return "";
        return String.valueOf(values.iterator().next().getValue());
    }

    private TableColumn<IDataSet, String> createPropertyColumn(IProperty property) {
        TableColumn<IDataSet, String> column = new TableColumn<>(property.getName());

        column.setCellValueFactory(param -> StringConstant.valueOf(
                valuesAsString(param.getValue().getDataItems().get(property).getPositiveValues()
                )));
        column.setCellFactory(PropertyEditableCell::new);
        return column;
    }

    private TableColumn<IDataSet, String> createTargetPropertyColumn(IProperty property) {
        TableColumn<IDataSet, String> column = new TableColumn<>(property.getName());

        column.setCellValueFactory(param ->
        {
            boolean isTrue = true;


            if(param.getValue().getDataItems().get(targetProperty).getPositiveValues().size() > 0)
            {
                for(IValue value : param.getValue().getDataItems().get(targetProperty).getPositiveValues())
                {
                    if(value.equals(targetValue))
                    {
                        text = "Да";

                        isTrue = false;
                    }
                }
            }

            if(isTrue)
            {
                if(param.getValue().getDataItems().get(targetProperty).getNegativeValues().size() > 0)
                {
                    for(IValue value : param.getValue().getDataItems().get(targetProperty).getNegativeValues())
                    {
                        if(value.equals(targetValue))
                        {
                            text = "Нет";

                            isTrue = false;
                        }
                    }
                }
            }

            if(isTrue)
            {
                if(param.getValue().getDataItems().get(targetProperty).getUndefinedValues().size() > 0)
                {
                    for(IValue value : param.getValue().getDataItems().get(targetProperty).getUndefinedValues())
                    {
                        if(value.equals(targetValue))
                        {
                            text = "Не известно";

                            isTrue = false;
                        }
                    }
                }
            }
            return StringConstant.valueOf(text);
        });
        //column.setStyle("-fx-color: black;");
        column.setCellFactory(PropertyEditableCell::new);
        return column;
    }


    private IDataSets newDataSets;
    private List<Hypothesis> positiveHypothesis;
    private List<Hypothesis> negativeHypothesis;
    private IProperty targetProperty;
    private IValue targetValue;
    String text;
}