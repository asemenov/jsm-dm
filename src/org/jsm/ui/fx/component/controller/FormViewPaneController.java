package org.jsm.ui.fx.component.controller;

import com.google.common.collect.Lists;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.geometry.*;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;
import org.jsm.model.IDataItem;
import org.jsm.model.IDataSet;
import org.jsm.model.IDataSets;
import org.jsm.model.IProperty;
import org.jsm.model.impl.data.DataSet;
import org.jsm.ui.fx.component.pane.form.PropertyFormPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.*;

/**
 * @author Anatoly Semenov
 *         Date: 04.01.2015
 */
public class FormViewPaneController implements Initializable{
    private static final Logger log = LoggerFactory.getLogger(FormViewPaneController.class);

    private IDataSets iDataSets;

    private ObservableList<IDataSet> observableList = FXCollections.observableArrayList();

    @FXML
    private ListView<IDataSet> dataSetListView;

    @FXML
    private Pane dataTabFormPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        log.info("initialize {}, {}", location, resources);
    }

    public void initData(IDataSets iDataSets) {
        this.iDataSets = iDataSets;
        if (this.iDataSets != null) {
            Collection<IDataSet> iDataSetCollection = this.iDataSets.getDataSets();
            observableList.addAll(iDataSetCollection);
        }
        dataSetListView.setItems(observableList);
        dataSetListView.setOrientation(Orientation.VERTICAL);
        dataSetListView.setCellFactory(TextFieldListCell.forListView(new StringConverter<IDataSet>() {
            @Override
            public String toString(IDataSet object) {
                return object.getName();
            }

            @Override
            public IDataSet fromString(String string) {
                for (IDataSet iDataSet : observableList)
                    if (iDataSet.getName().equals(string))
                        return iDataSet;
                return null;
            }
        }));

        dataSetListView.setOnMouseClicked(event -> {

            log.info(event.getButton().toString());

            if (event.getButton().equals(MouseButton.PRIMARY)) {
                log.info("faaf" + dataSetListView.getSelectionModel().getSelectedItem().getName());

                IDataSet selected = dataSetListView.getSelectionModel().getSelectedItem();

                if (selected == null) {
                    return;
                } else {
                    log.info("select {} data item", selected.getName());
                    showDataSetForm(selected);
                }
            }
        });
    }

    @FXML
    public void listViewContextMenuAdd(ActionEvent actionEvent) {
        showDataSetForm(null);
    }

    private void showDataSetForm(IDataSet dataSet) {
        dataTabFormPane.getChildren().clear();
        dataTabFormPane.getChildren().add(createForm(dataSet));
    }

    @FXML
    public void listViewContextMenuRemove(ActionEvent actionEvent) {
        log.info("call listViewContextMenuRemove");
        IDataSet forRemove = dataSetListView.getSelectionModel().getSelectedItem();
        if (forRemove == null)
            return;
        dataSetListView.getItems().remove(dataSetListView.getSelectionModel().getSelectedIndex());
        iDataSets.getDataSets().remove(forRemove);
    }

    private VBox createForm(IDataSet iDataSet)
    {
        size = 0;

        VBox vBox1 = new VBox();
        GridPane grid = new GridPane();

        grid.setStyle("-fx-border-color: black;");

        log.info(grid.getStyle());

        grid.setPrefSize(300, 530);

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(4);
        grid.setVgap(4);
        grid.setPadding(new Insets(0, 0, 0, 0));

        Collection<PropertyFormPane> propertyFormPanes = Lists.newArrayList();

        for (IProperty iProperty : this.iDataSets.getModel().getProperties()) {
            PropertyFormPane pane;
            IDataItem initItem = null;

            if (iDataSet != null)
            {
                pane = PropertyFormPane.Factory.newPropertyFormPane(iProperty, iDataSet);

                Set<Map.Entry<IProperty, IDataItem>> set = iDataSet.getDataItems().entrySet();
                Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                initItem = iDataSet.getDataItems().get(iProperty);

                /*while(iterator.hasNext())
                {
                    Map.Entry<IProperty, IDataItem> entry = iterator.next();

                    if(entry.getKey().equals(iProperty))
                    {
                        initItem = entry.getValue();
                        break;
                    }
                }*/

                /*for (IDataItem item : iDataSet.getDataItems())
                    if (item.getIProperty().equals(iProperty)) {
                        initItem = item;
                        break;
                    }*/
            }
            else
            {
                pane = PropertyFormPane.Factory.newPropertyFormPane(iProperty);
            }

            pane.init(initItem);
            size = size + pane.getSize();
            //log.info("Size = " + size);
            propertyFormPanes.add(pane);
        }

        GridPane textFieldPane = new GridPane();

        textFieldPane.setAlignment(Pos.TOP_CENTER);
        textFieldPane.setHgap(3);
        textFieldPane.setVgap(3);
        textFieldPane.setPadding(new Insets(5, 50, 5, 50));

        TextField nameTextField = new TextField(iDataSet == null ? "Name" : iDataSet.getName());
        TextField commentTextField = new TextField(iDataSet == null ? "Comment" : iDataSet.getComment());

        nameTextField.setPrefSize(200, 25);
        nameTextField.setAlignment(Pos.CENTER);

        nameTextField.setOnMouseClicked(event -> {
            if (nameTextField.getText().equals("Name")) {
                nameTextField.setText("");
            }
            if(commentTextField.getText().equals(""))
            {
                commentTextField.setText("Comment");
            }
        });

        textFieldPane.add(nameTextField, 0, 0);

        commentTextField.setPrefSize(200, 25);
        commentTextField.setAlignment(Pos.CENTER);

        commentTextField.setOnMouseClicked(event -> {
            if (commentTextField.getText().equals("Comment")) {
                commentTextField.setText("");
            }
            if(nameTextField.getText().equals(""))
            {
                nameTextField.setText("Name");
            }
        });

        textFieldPane.add(commentTextField, 0, 1);

        grid.add(textFieldPane, 0, 0);

        grid.setOnMouseClicked(event->
        {
            if(nameTextField.getText().equals(""))
            {
                nameTextField.setText("Name");
            }

            if(commentTextField.getText().equals(""))
            {
                commentTextField.setText("Comment");
            }
        });

        VBox vBox = new VBox();

        for(PropertyFormPane propertyFormPane : propertyFormPanes)
        {
            propertyFormPane.setOnMouseClicked(event->
            {
                if(nameTextField.getText().equals(""))
                {
                    nameTextField.setText("Name");
                }

                if(commentTextField.getText().equals(""))
                {
                    commentTextField.setText("Comment");
                }

                //size = size + propertyFormPane.getChildren().size();
            });

           if(iDataSet != null)
           {

           }
        }

        /*for(IDataSet dataSet : observableList)
        {
            size = size + dataSet.getDataItems().size();

            log.info("Size = " + size);
        }*/
        vBox.setMinSize(300, 420);
        vBox.setPrefSize(300, size*20);
        vBox.setMaxSize(300, size*20);
        vBox.getChildren().addAll(propertyFormPanes);

        //log.info("color " + vBox.getBackground().toString());

        vBox.setPadding(new Insets(5, 5, 5, 5));

        ScrollPane scrollPane = new ScrollPane(vBox);
        scrollPane.setMinSize(300, 420);
        scrollPane.setPrefSize(300, size*20);
        scrollPane.setMaxSize(300, size*20);
        grid.add(scrollPane, 0, 2);

        vBox.setOnMouseClicked(event->
        {
            if(nameTextField.getText().equals(""))
            {
                nameTextField.setText("Name");
            }

            if(commentTextField.getText().equals(""))
            {
                commentTextField.setText("Comment");
            }
        });

        GridPane buttonPane = new GridPane();

        buttonPane.setAlignment(Pos.CENTER);
        buttonPane.setHgap(4);
        buttonPane.setVgap(4);
        buttonPane.setPadding(new Insets(5, 50, 5, 50));

        Button submitButton = new Button("Submit");
        Button cancelButton = new Button("Cancel");

        cancelButton.setOnAction(event -> {
            dataTabFormPane.getChildren().clear();
        });

        submitButton.setOnAction(event -> {
            if (iDataSet != null)
                throw new UnsupportedOperationException("Update not supported!!!");

            Map<IProperty, IDataItem> mapProperty = new LinkedHashMap<IProperty, IDataItem>();

            for(PropertyFormPane propertyFormPane : propertyFormPanes)
            {
                mapProperty.put(propertyFormPane.getDataItem().getIProperty(), propertyFormPane.getDataItem());
            }

            IDataSet dataSet = new DataSet(iDataSets.getModel(), mapProperty,  nameTextField.getText(), commentTextField.getText());
            /*IDataSet dataSet = new DataSet(iDataSets.getModel(), Lists.newArrayList(
                    Collections2.transform(propertyFormPanes, PropertyFormPane::getDataItem)),
                    nameTextField.getText(), commentTextField.getText());*/

            iDataSets.getDataSets().add(dataSet);
            observableList.add(dataSet);

            dataTabFormPane.getChildren().clear();

            //initData(iDataSets);
        });

        buttonPane.add(submitButton, 0, 0);
        buttonPane.add(cancelButton, 1, 0);

        grid.add(buttonPane, 0, 3);

        dataTabFormPane.getChildren().add(grid);

        dataTabFormPane.setOnMouseClicked(event->
        {
            if(nameTextField.getText().equals(""))
            {
                nameTextField.setText("Name");
            }

            if(commentTextField.getText().equals(""))
            {
                commentTextField.setText("Comment");
            }
        });

        dataSetListView.setOnMouseClicked(event->
        {
            if(nameTextField.getText().equals(""))
            {
                nameTextField.setText("Name");
            }

            if(commentTextField.getText().equals(""))
            {
                commentTextField.setText("Comment");
            }
        });

        vBox1.getChildren().add(grid);

        return vBox1;
    }

    int size;
}
