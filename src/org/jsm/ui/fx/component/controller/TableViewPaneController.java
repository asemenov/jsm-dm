package org.jsm.ui.fx.component.controller;

import com.google.common.collect.Lists;
import com.sun.javafx.binding.StringConstant;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.jsm.common.Utils;
import org.jsm.model.*;
import org.jsm.ui.fx.component.pane.form.PropertyFormPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//import java.awt.*;
//import java.awt.*;
import java.net.URL;
import java.util.*;

/**
 * @author Anatoly Semenov
 *         Date: 08.01.2015
 */
public class TableViewPaneController implements Initializable {

    private static final Logger log = LoggerFactory.getLogger(TableViewPaneController.class);

    private IDataSets iDataSets;
    private TableView<IDataSet> dataSetTableView = new TableView<IDataSet>();

    @FXML
    private Pane dataSetPanel;

    @FXML
    private FlowPane dataSetFlowPane;

    @FXML
    private Button addRowButton;

    @FXML
    private Button removeRowButton;

    /*@FXML
    private TableView<IDataSet> dataSetTableView;

    @FXML
    private ScrollPane dataSetScrollPane;*/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dataSetTableView.setEditable(true);
    }

    public void initData(IDataSets iDataSets)
    {
        this.iDataSets = iDataSets;

        addRowButton.setOnAction(event->
        {
            createModalWindow(null);
        });

        removeRowButton.setOnAction(event->
        {
            if(dataSetTableView.getSelectionModel().getSelectedItems().size() > 0)
            {
                for(IDataSet aDataSet : dataSetTableView.getSelectionModel().getSelectedItems())
                {
                    iDataSets.getDataSets().remove(aDataSet);

                    dataSetTableView.getItems().remove(aDataSet);
                }
            }
        });

        // this.dataSetTableView = new TableView<IDataSet>();
        if (iDataSets != null) {
            dataSetTableView.getItems().clear();
            dataSetTableView.getItems().addAll(this.iDataSets.getDataSets());

            dataSetTableView.getColumns().add(createTextColumn(TextColumn.NAME));
            dataSetTableView.getColumns().add(createTextColumn(TextColumn.COMMENT));

            for (IProperty property : iDataSets.getModel().getProperties()) {
                dataSetTableView.getColumns().add(createPropertyColumn(property));
            }

            /*dataSetTableView.getColumns().addAll(
                    iDataSets.getModel().getProperties().stream().map(this::createPropertyColumn).findAny().get());*/

            for (TableColumn column : dataSetTableView.getColumns()) {
                column.setPrefWidth(150);
            }

            dataSetTableView.setPrefSize(dataSetTableView.getColumns().size() * 150, Screen.getPrimary().getVisualBounds().getHeight() - 150);
            dataSetTableView.setMinWidth(Screen.getPrimary().getVisualBounds().getWidth() - 100);
            dataSetTableView.setOnMouseClicked(event->
            {
                if(event.getButton() == MouseButton.PRIMARY && event.getClickCount() == 2)
                {
                    if((dataSetTableView.getSelectionModel().getSelectedItems() != null && dataSetTableView.getSelectionModel().getSelectedItem() != null))
                    {
                        if(dataSetTableView.getSelectionModel().getSelectedItems().size() == 1)
                        {
                            createModalWindow(dataSetTableView.getSelectionModel().getSelectedItem());
                        }
                    }
                }
            });
            // dataSetTableView.setScr;

            ScrollPane dataSetTableViewScrollPane = new ScrollPane(dataSetTableView);

            dataSetTableViewScrollPane.setPrefSize(Screen.getPrimary().getVisualBounds().getWidth() - 100, Screen.getPrimary().getVisualBounds().getHeight() - 150);

            GridPane grid = new GridPane();
            grid.setPadding(new Insets(25, 25, 25, 25));

            /*dataSetTableView.setPadding(new Insets(25, 25, 25, 25));*/
            //dataSetTableViewScrollPane.setPadding(new Insets(25, 25, 25, 25));

            // dataSetTableViewScrollPane.setPrefSize(Screen.getPrimary().getVisualBounds().getWidth() - 100, Screen.getPrimary().getVisualBounds().getHeight() - 150);

            //dataSetTableViewScrollPane.setBorder(new Border());

            grid.add(dataSetFlowPane, 0, 0);
            grid.add(dataSetTableViewScrollPane, 0, 1);

            dataSetPanel.getChildren().add(grid);
            //dataSetPanel.setPadding(new Insets(25, 25, 25, 25));
            // dataSetScrollPane = new ScrollPane(dataSetTableView);
        }
    }

    private TableColumn<IDataSet, String> createTextColumn(TextColumn textColumn) {
        TableColumn<IDataSet, String> column = new TableColumn<>(textColumn.getColumnName());
        column.setCellValueFactory(param -> StringConstant.valueOf(textColumn.getDataSetValue(param.getValue())));
        column.setCellFactory(TextFieldTableCell.forTableColumn());
        column.setOnEditCommit(event -> textColumn.setDataSetValue(event.getRowValue(), event.getNewValue()));
        return column;
    }

    private TableColumn<IDataSet, String> createPropertyColumn(IProperty property) {
        TableColumn<IDataSet, String> column = new TableColumn<>(property.getName());

        /*TableColumn.CellDataFeatures<IDataSet, String> param = new TableColumn.CellDataFeatures<IDataSet, String>(column);

        IDataSet iDataSet = dataSetTableView.getSelectionModel().getSelectedItem();

        Set<Map.Entry<IProperty, IDataItem>> set = iDataSet.getDataItems().getDataItems().entrySet();
        Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

        IDataItem dataItem;

        while(iterator.hasNext())
        {
            Map.Entry<IProperty, IDataItem> entry =  iterator.next();

            if(entry.getKey().equals(property))
            {
                dataItem = entry.getValue();

                break;
            }
        }*/

        //column.setCellFactory(param->new TableViewCell());

        column.setCellValueFactory(param -> StringConstant.valueOf(
                valuesAsString(param.getValue().getDataItems().get(property).getPositiveValues()
                )));
        column.setCellFactory(PropertyEditableCell::new);
        return column;
    }


        /*column.setCellValueFactory(param -> StringConstant.valueOf(
                    valuesAsString(param.getValue().getDataItems().stream()
                                    .filter(dataItem -> dataItem.getIProperty().equals(property))
                                    .findFirst()
                                    .get()
                                    .getPositiveValues()
                    )));
        column.setCellFactory(PropertyEditableCell::new);
        return column;
    }*/


    private String valuesAsString(Collection<IValue> values) {
        if (Utils.isEmpty(values))
            return "";
        return String.valueOf(values.iterator().next().getValue()) + "...";
    }


    @FXML
    public void addRowAction() {
        log.info("Add row button pressed");
        throw new UnsupportedOperationException("Add row button");
    }

    @FXML
    public void removeRowAction() {
        log.info("Remove row button pressed");
        throw new UnsupportedOperationException("Add row button");
    }

    @FXML
    public void JSMAction() {
        log.info("Remove row button pressed");
        throw new UnsupportedOperationException("Add row button");
    }

    private class PropertyEditableCell extends TableCell<IDataSet, String> {
        private final TableColumn<IDataSet, String> column;
        private PropertyFormPane formPane;
        private IDataItem dataItem;

        private PropertyEditableCell(TableColumn<IDataSet, String> column) {
            this.column = column;
        }

        @Override
        public void startEdit() {
            super.startEdit(); //todo show edit pane with AV
            IProperty prop = iDataSets.getModel().getProperties().stream()
                    .filter(property -> property.getName().equals(column.getText()))
                    .findFirst()
                    .get();
            if (prop != null) {
                formPane = PropertyFormPane.Factory.newPropertyFormPane(prop);
                IDataSet dataSet = iDataSets.getDataSets().stream().skip(this.getTableRow().getIndex()).findFirst().get();

                Set<Map.Entry<IProperty, IDataItem>> set = dataSet.getDataItems().entrySet();
                Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                while (iterator.hasNext()) {
                    Map.Entry<IProperty, IDataItem> entry = iterator.next();

                    if (entry.getKey().equals(prop)) {
                        dataItem = entry.getValue();
                    }
                }

                //dataItem = dataSet.getDataItems().stream().filter(iDataItem -> iDataItem.getIProperty().equals(prop)).findFirst().get();
                formPane.init(dataItem);
                setText(null);
                setGraphic(formPane);
                formPane.requestFocus();
            }
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            if (!formPane.getDataItem().equals(dataItem)) {

            }
            setGraphic(null);
            setText(valuesAsString(dataItem.getPositiveValues()));
        }

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(item);
            setGraphic(null);
        }
    }

    private enum TextColumn {
        NAME("Name") {
            @Override
            public String getDataSetValue(IDataSet dataSet) {
                return dataSet.getName();
            }

            @Override
            public void setDataSetValue(IDataSet dataSet, String value) {
                dataSet.setName(value);
            }
        },
        COMMENT("Comment") {
            @Override
            public String getDataSetValue(IDataSet dataSet) {
                return dataSet.getComment();
            }

            @Override
            public void setDataSetValue(IDataSet dataSet, String value) {
                dataSet.setComments(value);
            }
        };

        private String columnName;

        private TextColumn(String columnName) {
            this.columnName = columnName;
        }

        public String getColumnName() {
            return columnName;
        }

        public abstract String getDataSetValue(IDataSet dataSet);

        public abstract void setDataSetValue(IDataSet dataSet, String value);
    }

    protected void createModalWindow(IDataSet iDataSet)
    {
        Stage stage = new Stage();
        if(iDataSet == null)
            stage.setTitle("Add Row");
        else
            stage.setTitle("Row");

        GridPane grid = new GridPane();

        grid.setStyle("-fx-border-color: black;");

        grid.setPrefSize(300, 530);

        grid.setAlignment(Pos.CENTER);
        grid.setHgap(4);
        grid.setVgap(4);
        grid.setPadding(new Insets(0, 0, 0, 0));

        Collection<PropertyFormPane> propertyFormPanes = Lists.newArrayList();

        for (IProperty iProperty : this.iDataSets.getModel().getProperties())
        {
            PropertyFormPane pane;
            IDataItem initItem = null;

            if (iDataSet != null)
            {
                pane = PropertyFormPane.Factory.newPropertyFormPane(iProperty, iDataSet);

                Set<Map.Entry<IProperty, IDataItem>> set = iDataSet.getDataItems().entrySet();
                Iterator<Map.Entry<IProperty, IDataItem>> iterator = set.iterator();

                initItem = iDataSet.getDataItems().get(iProperty);
            }
            else
            {
                pane = PropertyFormPane.Factory.newPropertyFormPane(iProperty);
            }

            pane.init(initItem);

            size = size + pane.getSize();

            propertyFormPanes.add(pane);
        }

        GridPane textFieldPane = new GridPane();

        textFieldPane.setAlignment(Pos.TOP_CENTER);
        textFieldPane.setHgap(3);
        textFieldPane.setVgap(3);
        textFieldPane.setPadding(new Insets(5, 50, 5, 50));

        TextField nameTextField = new TextField(iDataSet == null ? "Name" : iDataSet.getName());
        TextField commentTextField = new TextField(iDataSet == null ? "Comment" : iDataSet.getComment());

        nameTextField.setPrefSize(200, 25);
        nameTextField.setAlignment(Pos.CENTER);

        nameTextField.setOnMouseClicked(event -> {
            if (nameTextField.getText().equals("Name")) {
                nameTextField.setText("");
            }
            if(commentTextField.getText().equals(""))
            {
                commentTextField.setText("Comment");
            }
        });

        textFieldPane.add(nameTextField, 0, 0);

        commentTextField.setPrefSize(200, 25);
        commentTextField.setAlignment(Pos.CENTER);

        commentTextField.setOnMouseClicked(event -> {
            if (commentTextField.getText().equals("Comment")) {
                commentTextField.setText("");
            }
            if(nameTextField.getText().equals(""))
            {
                nameTextField.setText("Name");
            }
        });

        textFieldPane.add(commentTextField, 0, 1);

        grid.add(textFieldPane, 0, 0);

        VBox vBox = new VBox();

        for(PropertyFormPane propertyFormPane : propertyFormPanes)
        {
            propertyFormPane.setOnMouseClicked(event->
            {
                if(nameTextField.getText().equals(""))
                {
                    nameTextField.setText("Name");
                }

                if(commentTextField.getText().equals(""))
                {
                    commentTextField.setText("Comment");
                }
            });

            if(iDataSet != null)
            {

            }
        }

        vBox.setMinSize(300, 420);
        vBox.setPrefSize(300, size*20);
        vBox.setMaxSize(300, size*20);
        vBox.getChildren().addAll(propertyFormPanes);

        vBox.setPadding(new Insets(5, 5, 5, 5));

        ScrollPane scrollPane = new ScrollPane(vBox);
        scrollPane.setMinSize(300, 420);
        scrollPane.setPrefSize(300, size*20);
        scrollPane.setMaxSize(300, size*20);
        grid.add(scrollPane, 0, 2);

        GridPane buttonPane = new GridPane();

        buttonPane.setAlignment(Pos.CENTER);
        buttonPane.setHgap(4);
        buttonPane.setVgap(4);
        buttonPane.setPadding(new Insets(5, 50, 5, 50));

        Button submitButton = new Button("Submit");
        Button cancelButton = new Button("Cancel");

        submitButton.setOnAction(event->
        {
            if (iDataSet != null)
                throw new UnsupportedOperationException("Update not supported!!!");

            Map<IProperty, IDataItem> mapProperty = new LinkedHashMap<IProperty, IDataItem>();

            for(PropertyFormPane propertyFormPane : propertyFormPanes)
            {
                mapProperty.put(propertyFormPane.getDataItem().getIProperty(), propertyFormPane.getDataItem());
            }

            //IDataSet dataSet = new DataSet(iDataSets.getModel(), mapProperty,  nameTextField.getText(), commentTextField.getText());

            /*iDataSets.getDataSets().add(dataSet);
            dataSetTableView.getItems().add(dataSet);*/

            stage.close();
        });

        cancelButton.setOnAction(event->
        {
            stage.close();
        });

        buttonPane.add(submitButton, 0, 0);
        buttonPane.add(cancelButton, 1, 0);

        grid.add(buttonPane, 0, 3);

        Scene scene = new Scene(new Group(grid));

        stage.setScene(scene);

        stage.show();
    }

    int size;
}
