package org.jsm.ui.fx.component.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Screen;
import org.jsm.model.IProperty;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.trash.resolver.impl.Hypothesis;
import org.jsm.trash.resolver.impl.JSMSolver;
import org.jsm.ui.fx.component.pane.ExperimentTapPane;
import org.jsm.model.IDataSet;
import org.jsm.model.IDataSets;
import org.jsm.model.IValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Данил on 13.06.2015.
 */
public class ExperimentPaneController implements Initializable
{
    private static final Logger log = LoggerFactory.getLogger(ExperimentPaneController.class);

    @FXML
    private GridPane gridPaneFX;

    @FXML
    private Button startButton;

    @FXML
    private ListView<CheckBox> methodsListView;

    @FXML
    private ListView<IProperty> targetPropertyListView;

    @FXML
    private ListView<IValue> targetValueListView;

    @FXML
    private ListView<CheckBox> positiveListView;

    @FXML
    private ListView negativeListView;

    public ExperimentPaneController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        log.info("initialize {}, {}", location, resources);
    }

    public void init(IDataSets iDataSets, IDataSets newDataSets, List<Hypothesis> positiveHypothesis, List<Hypothesis> negativeHypothesis, final IProperty targetProperty, IValue targetValue, ExperimentTapPane experimentTapPane)
    {
        this.iDataSets = iDataSets;
        this.positiveHypothesis = positiveHypothesis;
        this.negativeHypothesis = negativeHypothesis;
        this.targetProperty = targetProperty;
        this.targetValue = targetValue;

        gridPaneFX.setMaxSize(Screen.getPrimary().getVisualBounds().getWidth() - 100, Screen.getPrimary().getVisualBounds().getHeight() - 100);

        ObservableList<IProperty> observableListProperty = FXCollections.observableArrayList();
        ObservableList<IValue> observableListValue = FXCollections.observableArrayList();
        ObservableList<CheckBox> methodsCheckBox = FXCollections.observableArrayList();
        ObservableList<Hypothesis> observablePositiveHypothesis = FXCollections.observableArrayList();
        ObservableList<Hypothesis> observableNegativeHypothesis = FXCollections.observableArrayList();

        if (this.iDataSets != null)
        {
            Collection<IProperty> iPropertiesSetCollection = this.iDataSets.getModel().getProperties();
            observableListProperty.addAll(iPropertiesSetCollection.stream().filter(aProperty -> aProperty instanceof SelectProperty).collect(Collectors.toList()));
        }

        targetPropertyListView.getItems().addAll(observableListProperty);
        targetPropertyListView.setCellFactory(param -> new PropertiesListCell());

        targetValueListView.getItems().addAll(observableListValue);
        targetValueListView.setCellFactory(param -> new ValuesListCell());

        testPositiveDataSet = new LinkedHashMap<IDataSet, CheckBox>();
        testNegativeDataSet = new LinkedHashMap<IDataSet, CheckBox>();

        targetPropertyListView.setOnMouseClicked(event ->
        {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (targetPropertyListView.getSelectionModel().getSelectedItems().size() == 1)
                {
                    this.targetProperty = targetPropertyListView.getSelectionModel().getSelectedItem();



                    log.info(targetPropertyListView.getItems().size() + "");
                    observableListValue.clear();
                    targetValueListView.getItems().clear();
                    observableListValue.addAll(targetPropertyListView.getSelectionModel().getSelectedItem().getPropertyValues().stream().collect(Collectors.toList()));

                    targetValueListView.getItems().addAll(observableListValue);
                }
            }
        });

        targetValueListView.setOnMouseClicked(event->
        {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                if (targetValueListView.getSelectionModel().getSelectedItems().size() == 1)
                {
                    positiveListView.getItems().clear();
                    negativeListView.getItems().clear();

                    this.targetValue = targetValueListView.getSelectionModel().getSelectedItem();
                    for(IDataSet iDataSet : iDataSets.getDataSets())
                    {
                        boolean tmp = false;

                        if((iDataSet.getDataItems().get(targetPropertyListView.getSelectionModel().getSelectedItem()).getPositiveValues() != null) && (iDataSet.getDataItems().get(targetPropertyListView.getSelectionModel().getSelectedItem()).getPositiveValues().size()>0))
                        {
                            for(IValue value : iDataSet.getDataItems().get(targetPropertyListView.getSelectionModel().getSelectedItem()).getPositiveValues())
                            {
                                if(value.toString().equals(targetValueListView.getSelectionModel().getSelectedItem().getValue().toString()))
                                {
                                    CheckBox checkBox = new CheckBox(iDataSet.getName());

                                    positiveListView.getItems().add(checkBox);

                                    testPositiveDataSet.put(iDataSet, checkBox);

                                    tmp = true;

                                    break;
                                }
                            }
                        }

                        if(!tmp)
                        {
                            if((iDataSet.getDataItems().get(targetPropertyListView.getSelectionModel().getSelectedItem()).getNegativeValues() != null) && (iDataSet.getDataItems().get(targetPropertyListView.getSelectionModel().getSelectedItem()).getNegativeValues().size()>0))
                            {
                                for(IValue value : iDataSet.getDataItems().get(targetPropertyListView.getSelectionModel().getSelectedItem()).getNegativeValues())
                                {
                                    if(value.toString().equals(targetValueListView.getSelectionModel().getSelectedItem().getValue().toString()))
                                    {
                                        CheckBox checkBox = new CheckBox(iDataSet.getName());

                                        negativeListView.getItems().add(new CheckBox(iDataSet.getName()));

                                        testNegativeDataSet.put(iDataSet, checkBox);

                                        tmp = true;

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        });

        if(positiveHypothesis != null && positiveHypothesis.size() > 0)
        {
            observablePositiveHypothesis.addAll(positiveHypothesis);
        }

        if(negativeHypothesis != null && negativeHypothesis.size() > 0)
        {
            observablePositiveHypothesis.addAll(negativeHypothesis);
        }


        methodsCheckBox.add(new CheckBox("Induction"));
        methodsCheckBox.add(new CheckBox("Analogy"));
        methodsCheckBox.add(new CheckBox("Abduction"));
        methodsCheckBox.add(new CheckBox("Ban (±)-examples"));

        methodsListView.setItems(methodsCheckBox);
        methodsListView.setFixedCellSize(25);

        startButton.setOnMouseClicked(event ->
        {
            IDataSets testPositiveDataSets = new DataSets(iDataSets.getModel(), new ArrayList<IDataSet>());
            IDataSets testNegativeDataSets = new DataSets(iDataSets.getModel(), new ArrayList<IDataSet>());

            Set<Map.Entry<IDataSet, CheckBox>> set = testPositiveDataSet.entrySet();
            Iterator<Map.Entry<IDataSet, CheckBox>> iterator = set.iterator();

            while(iterator.hasNext())
            {
                Map.Entry entry = iterator.next();

                if(((CheckBox) entry.getValue()).isSelected())
                {
                    testPositiveDataSets.getDataSets().add(((IDataSet) entry.getKey()));
                }
            }

            set = testNegativeDataSet.entrySet();
            iterator = set.iterator();

            while(iterator.hasNext())
            {
                Map.Entry entry = iterator.next();

                if(((CheckBox) entry.getValue()).isSelected())
                {
                    testNegativeDataSets.getDataSets().add(((IDataSet) entry.getKey()));
                }
            }

            JSMSolver = new JSMSolver(iDataSets, testPositiveDataSets, testNegativeDataSets, targetPropertyListView.getSelectionModel().getSelectedItem(), targetValueListView.getSelectionModel().getSelectedItem());

            this.positiveHypothesis = JSMSolver.getPositiveHypothesis();
            this.negativeHypothesis = JSMSolver.getNegativeHypothesis();
            this.newDataSets = JSMSolver.getNewDataSets();
            this.targetProperty = targetPropertyListView.getSelectionModel().getSelectedItem();
            this.targetValue = targetValueListView.getSelectionModel().getSelectedItem();

            experimentTapPane.setHypothesis(this.positiveHypothesis, this.negativeHypothesis);
            experimentTapPane.setNewDataSets(this.newDataSets);
            experimentTapPane.setTargetProperty(this.targetProperty);
            experimentTapPane.setTargetValue(this.targetValue);
        });
    }

    public class PropertiesListCell extends ListCell<IProperty>
    {
        @Override
        protected void updateItem(IProperty item, boolean empty)
        {
            super.updateItem(item, empty);

            if ( empty || item == null )
            {
                setText(null);
                setGraphic(null);
            }
            else
            {
                Label propertyName = new Label(item.getName());
                propertyName.setMinWidth(50);
                setGraphic(new HBox(propertyName));
            }
        }
    }

    public class ValuesListCell extends ListCell<IValue>
    {
        @Override
        protected void updateItem(IValue item, boolean empty)
        {
            super.updateItem(item, empty);

            if ( empty || item == null )
            {
                setText(null);
                setGraphic(null);
            }
            else
            {
                Label propertyName = new Label(item.toString());
                propertyName.setMinWidth(50);
                setGraphic(new HBox(propertyName));
            }
        }
    }

    private IDataSets iDataSets;
    private JSMSolver JSMSolver;
    private Map<IDataSet, CheckBox> testPositiveDataSet;
    private Map<IDataSet, CheckBox> testNegativeDataSet;
    private IDataSets newDataSets;
    private List<Hypothesis> positiveHypothesis;
    private List<Hypothesis> negativeHypothesis;
    private IProperty targetProperty;
    private IValue targetValue;
    /*private TreeView<String> positiveTreeView;
    private TreeView<String> negativeTreeView;
    //private ObservableList<IProperty> observableList = FXCollections.observableArrayList();
    /*private ObservableList<IProperty> observableListProperty = FXCollections.observableArrayList();
    private ObservableList<IValue> observableListValue = FXCollections.observableArrayList();*/
}
