package org.jsm.ui.fx.component.list;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import org.jsm.model.*;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.property.TreeProperty;
import org.jsm.ui.fx.FXConst;
import org.jsm.ui.fx.component.menu.PropertiesListContextMenu;
import org.jsm.ui.fx.component.pane.SelectPropertyPane;
import org.jsm.ui.fx.component.pane.TreePropertyPane;
import org.jsm.common.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;

import static org.jsm.ui.fx.util.UIUtils.idAsSelector;

/**
 * Created by Данил on 18.05.2015.
 */
public class PropertiesList extends ListView<IProperty>
{
    private static final Logger log = LoggerFactory.getLogger(PropertiesList.class);

    private final IModel model;
    private final Scene mainScene;
    private final IDataSets iDataSets;

    public PropertiesList(IModel model, Scene mainScene, IDataSets iDataSets)
    {
        this.model = model;
        this.mainScene = mainScene;
        this.iDataSets = iDataSets;

        PropertiesListContextMenu contextMenu = new PropertiesListContextMenu(mainScene, model);

        MenuItem removeProperty = new MenuItem("Remove");

        removeProperty.setOnAction(value->{
            if(getSelectionModel().getSelectedItem() != null)
            {
                model.getProperties().remove(getSelectionModel().getSelectedItem());

                for(IDataSet iDataSet : iDataSets.getDataSets())
                {
                    iDataSet.getDataItems().remove(getSelectionModel().getSelectedItem());
                }

                getItems().remove(getSelectionModel().getSelectedItem());
            }

        });

        contextMenu.getItems().add(removeProperty);

        setContextMenu(contextMenu);

        //this.propertiesList = new ListView<IProperty>();

        PropertiesListCell propertiesListCell = new PropertiesListCell();

        setCellFactory(param -> new PropertiesListCell());
        setEditable(true);

        Collection<IProperty> children = Utils.nvl(model.getProperties(), Collections.<IProperty>emptyList());

        for (IProperty child : children)
            getItems().add(child);

        setOnMouseClicked(event->
        {
            IProperty property = getSelectionModel().getSelectedItem();

            if(event.getButton().equals(MouseButton.PRIMARY) && (property != null))
            {
                if(property instanceof ITreeProperty)
                {
                    Pane freePane = (Pane) mainScene.lookup(idAsSelector(FXConst.MODEL_TAB_FREE_PANE));
                    freePane.getChildren().clear();
                    freePane.getChildren().add(new TreePropertyPane(mainScene, TreePropertyPane.UPDATE_TREE_PROPERTY, TreePropertyPane.UPDATE, (TreeProperty) property));
                }
                else
                {
                    Pane freePane = (Pane) mainScene.lookup(idAsSelector(FXConst.MODEL_TAB_FREE_PANE));
                    freePane.getChildren().clear();
                    freePane.getChildren().add(new SelectPropertyPane(mainScene, SelectPropertyPane.UPDATE_SELECT_PROPERTY, SelectPropertyPane.UPDATE, (SelectProperty) property));
                }
            }
        });
    }

    public void addIProperty(IProperty property)
    {
        model.getProperties().add(property);

        getItems().add(property);
    }

    public void changeProperty(IProperty oldProperty, IProperty newProperty)
    {
        model.getProperties().remove(oldProperty);
        getItems().remove(oldProperty);

        model.getProperties().add(newProperty);
        getItems().add(newProperty);
    }

    public IModel getModel() {
        return model;
    }

    public class PropertiesListCell extends ListCell<IProperty>
    {
        @Override
        protected void updateItem(IProperty item, boolean empty)
        {
            super.updateItem(item, empty);

            if ( empty || item == null )
            {
                setText(null);
                setGraphic(null);
            }
            else
            {
                Label propertyName = new Label(item.getName());
                propertyName.setMinWidth(50);
               // setText("fsdf");
                setGraphic(new HBox(propertyName));
            }
        }
    }
}
