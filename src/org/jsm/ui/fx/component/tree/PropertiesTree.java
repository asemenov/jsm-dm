package org.jsm.ui.fx.component.tree;

import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import org.jsm.model.IModel;
import org.jsm.model.IProperty;
import org.jsm.model.ITreeProperty;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.property.TreeProperty;
import org.jsm.ui.fx.FXConst;
import org.jsm.ui.fx.component.pane.SelectPropertyPane;
import org.jsm.ui.fx.component.pane.TreePropertyPane;
import org.jsm.common.Utils;
import org.jsm.ui.fx.util.UIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Anatoly Semenov
 *         Date: 11.10.2014
 */
public class PropertiesTree extends TreeView<String> {

    @SuppressWarnings("UnusedDeclaration")
    private static final Logger log = LoggerFactory.getLogger(PropertiesTree.class);

    private final IModel model;
    private final TreeItem<String> root;
    private final Scene mainScene;
    private TreeItem<String> currentItem;

    public PropertiesTree(IModel model, Scene mainScene) {
        this.model = model;
        this.mainScene = mainScene;
        this.root = new TreeItem<>(model.getName());
//        this.root = new TreeItem<>("Root");
        setRoot(root);
        setEditable(true);


        Collection<IProperty> children = Utils.nvl(model.getProperties(), Collections.<IProperty>emptyList());
        for (IProperty child : children)
            root.getChildren().add(new TreeItem<>(child.getName()));

//        getSelectionModel()./
    }

    public void addIProperty(IProperty property) {
        model.getProperties().add(property);
        TreeItem<String> selectedTreeItem = new TreeItem<>(property.getName());

        if(property instanceof ITreeProperty)
        {
            getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                currentItem = newValue;
                model.getProperties()
                        .stream()
                        .filter(iProperty -> newValue.getValue().equals(iProperty.getName()))
                        .forEach(iProperty -> {
                            Pane freePane = (Pane) mainScene.lookup(UIUtils.idAsSelector(FXConst.MODEL_TAB_FREE_PANE));
                            freePane.getChildren().clear();
                            freePane.getChildren().add(new TreePropertyPane(mainScene, TreePropertyPane.UPDATE_TREE_PROPERTY, TreePropertyPane.UPDATE, (TreeProperty) iProperty));
                        });
            });
            root.getChildren().add(selectedTreeItem);
        }
        else
        {
            getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                currentItem = newValue;
                model.getProperties()
                        .stream()
                        .filter(iProperty -> newValue.getValue().equals(iProperty.getName()))
                        .forEach(iProperty -> {
                            Pane freePane = (Pane) mainScene.lookup(UIUtils.idAsSelector(FXConst.MODEL_TAB_FREE_PANE));
                            freePane.getChildren().clear();
                            freePane.getChildren().add(new SelectPropertyPane(mainScene, SelectPropertyPane.UPDATE_SELECT_PROPERTY, SelectPropertyPane.UPDATE, (SelectProperty) iProperty));
                        });
            });
            root.getChildren().add(selectedTreeItem);
        }


    }

    public void addITreeProperty(IProperty property) {
        model.getProperties().add(property);
        if(!(property instanceof ITreeProperty))
        {
            TreeItem<String> treePropertyTreeItem = new TreeItem<>(property.getName());

            getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
                currentItem = newValue;
                model.getProperties()
                        .stream()
                        .filter(iProperty -> newValue.getValue().equals(iProperty.getName()))
                        .forEach(iProperty -> {
                            Pane freePane = (Pane) mainScene.lookup(UIUtils.idAsSelector(FXConst.MODEL_TAB_FREE_PANE));
                            freePane.getChildren().clear();
                            freePane.getChildren().add(new SelectPropertyPane(mainScene, TreePropertyPane.UPDATE_TREE_PROPERTY, TreePropertyPane.UPDATE, (SelectProperty) iProperty));
                        });
            });
            root.getChildren().add(treePropertyTreeItem);
        }


    }

    public void changeProperty(IProperty oldProperty, IProperty newProperty) {
        if (currentItem != null) {
            currentItem.setValue(newProperty.getName());
            Collection<IProperty> properties = model.getProperties();
            properties.remove(oldProperty);
            properties.add(newProperty);
        }
    }

    public IModel getModel() {
        return model;
    }
}
