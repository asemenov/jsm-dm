package org.jsm.ui.fx.component.menu;

import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Pane;
import org.jsm.model.IModel;
import org.jsm.ui.fx.FXConst;
import org.jsm.ui.fx.component.pane.SelectPropertyPane;
import org.jsm.ui.fx.component.pane.TreePropertyPane;
import org.jsm.ui.fx.util.UIUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Anatoly Semenov
 *         Date: 12.10.2014
 */
public class PropertiesTreeContextMenu extends ContextMenu {
    private static final Logger log = LoggerFactory.getLogger(PropertiesTreeContextMenu.class);

   private final Scene mainScene;

    public PropertiesTreeContextMenu(Scene mainScene, IModel model) {
        this.mainScene = mainScene;
        MenuItem addSelectProperty = new MenuItem("Add select property");
        MenuItem addIntervalProperty = new MenuItem("Add interval property");
        MenuItem addTreeProperty = new MenuItem("Add tree property");
        getItems().addAll(addSelectProperty, addIntervalProperty,
                addTreeProperty);

        addSelectProperty.setOnAction(value->{
            Pane freePane = (Pane) this.mainScene.lookup(UIUtils.idAsSelector(FXConst.MODEL_TAB_FREE_PANE));
            freePane.getChildren().clear();
            freePane.getChildren().add(new SelectPropertyPane(this.mainScene, SelectPropertyPane.NEW_SELECT_PROPERTY, SelectPropertyPane.CREATE, null));
            log.info("new select property",  addSelectProperty);
        });
        addIntervalProperty.setOnAction(value->{
            log.info("new interval property",  addIntervalProperty);
        });
        addTreeProperty.setOnAction(value->{
            Pane freePane = (Pane) this.mainScene.lookup(UIUtils.idAsSelector(FXConst.MODEL_TAB_FREE_PANE));
            freePane.getChildren().clear();
            freePane.getChildren().add(new TreePropertyPane(this.mainScene, TreePropertyPane.NEW_TREE_PROPERTY, TreePropertyPane.CREATE, null));
            log.info("new tree property",  addTreeProperty);
        });
    }
}
