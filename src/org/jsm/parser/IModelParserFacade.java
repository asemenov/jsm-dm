package org.jsm.parser;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.jsm.common.AppConfig;
import org.jsm.model.IModel;
import org.jsm.model.IProperty;
import org.jsm.model.ITreeProperty;
import org.jsm.parser.output.JDMLexer;
import org.jsm.parser.output.JDMParser;
import org.jsm.common.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;


/**
 * User: Anatoly Semenov
 * Date: 01.05.2014
 */
public class IModelParserFacade {
    private static final Logger log = LoggerFactory.getLogger(IModelParserFacade.class);

    public static IModel parseModel(String text) {
        try {
            String encodedString = Utils.encodeString(text, AppConfig.getSystemEncoding());
            JDMParser parser = createParser(encodedString.trim());
            IModel model = parser.model();
            if (model == null)
                return null;
//            check model and set parent field to TreeProperties
            Collection<IProperty> properties = model.getProperties();
            for (IProperty iProperty : properties) {
                if (iProperty instanceof ITreeProperty)
                    fillParentField((ITreeProperty)iProperty, null);
            }
            return model;
        } catch (Exception e) {
            log.error("failed to parse the model: m={}, msg={}", text, e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public static JDMParser createParser(String input) {
        String encodedString = Utils.encodeString(input, AppConfig.getSystemEncoding());
        CharStream stream = new ANTLRStringStream(encodedString);
        JDMLexer lexer = new JDMLexer(stream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        return new JDMParser(tokens);
    }

    private static void fillParentField(ITreeProperty iTreeProperty, ITreeProperty parent) {
        iTreeProperty.setParent(parent);
        for (IProperty iProperty : iTreeProperty.getChildren())
            if (iProperty instanceof ITreeProperty)
                fillParentField((ITreeProperty)iProperty, iTreeProperty);
    }
}
