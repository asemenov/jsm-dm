// $ANTLR 3.5 E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g 2014-05-12 16:21:20

package org.jsm.parser.output;

import java.util.ArrayList;
import java.util.LinkedList;

import org.jsm.model.IModel;
import org.jsm.model.IProperty;
import org.jsm.model.ITreeProperty;
import org.jsm.model.IValue;




import org.antlr.runtime.*;

import java.util.List;

@SuppressWarnings("all")
public class JDMParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMENT", "GROUP_TREE_BEGIN", 
		"GROUP_TREE_END", "LEFT_BRACER", "MODEL_NAME", "MULTI_SELECT", "NON_NULLABLE_PROP", 
		"NULLABLE_PROP", "OTHER_SYMBOLS", "RIGHT_BRACER", "SET_BEGIN", "SET_END", 
		"SET_ITEM", "SINGLE_SELECT", "SUB_TREE_BEGIN", "SUB_TREE_END", "TREE_ITEM", 
		"WHITESPACE"
	};
	public static final int EOF=-1;
	public static final int COMMENT=4;
	public static final int GROUP_TREE_BEGIN=5;
	public static final int GROUP_TREE_END=6;
	public static final int LEFT_BRACER=7;
	public static final int MODEL_NAME=8;
	public static final int MULTI_SELECT=9;
	public static final int NON_NULLABLE_PROP=10;
	public static final int NULLABLE_PROP=11;
	public static final int OTHER_SYMBOLS=12;
	public static final int RIGHT_BRACER=13;
	public static final int SET_BEGIN=14;
	public static final int SET_END=15;
	public static final int SET_ITEM=16;
	public static final int SINGLE_SELECT=17;
	public static final int SUB_TREE_BEGIN=18;
	public static final int SUB_TREE_END=19;
	public static final int TREE_ITEM=20;
	public static final int WHITESPACE=21;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public JDMParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public JDMParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return JDMParser.tokenNames; }
	@Override public String getGrammarFileName() { return "E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g"; }


	@Override
	public void reportError(RecognitionException e) {
	  super.reportError(e);
	  throw new RuntimeException(e);
	}
	// for debug purposes
	private void sout(Object o) {System.out.println(o);}



	// $ANTLR start "model"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:44:1: model returns [IModel iModel] : ( (n= model_name props= properties ) | (props= properties ) );
	public final IModel model() throws  RecognitionException {
		IModel iModel = null;


		String n =null;
		List<IProperty> props =null;

		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:45:2: ( (n= model_name props= properties ) | (props= properties ) )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==COMMENT) ) {
				alt1=1;
			}
			else if ( (LA1_0==GROUP_TREE_BEGIN||LA1_0==SET_BEGIN||LA1_0==WHITESPACE) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:45:4: (n= model_name props= properties )
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:45:4: (n= model_name props= properties )
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:45:5: n= model_name props= properties
					{
					pushFollow(FOLLOW_model_name_in_model61);
					n=model_name();
					state._fsp--;

					pushFollow(FOLLOW_properties_in_model67);
					props=properties();
					state._fsp--;

					iModel = IModel.Factory.createModel(n, props);
					}

					}
					break;
				case 2 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:46:6: (props= properties )
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:46:6: (props= properties )
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:46:7: props= properties
					{
					pushFollow(FOLLOW_properties_in_model82);
					props=properties();
					state._fsp--;

					iModel = IModel.Factory.createModel("model", props);
					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return iModel;
	}
	// $ANTLR end "model"



	// $ANTLR start "model_name"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:49:1: model_name returns [String text] : ( COMMENT ( WHITESPACE )* MODEL_NAME ( WHITESPACE )* LEFT_BRACER t= name RIGHT_BRACER ) ;
	public final String model_name() throws  RecognitionException {
		String text = null;


		String t =null;

		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:2: ( ( COMMENT ( WHITESPACE )* MODEL_NAME ( WHITESPACE )* LEFT_BRACER t= name RIGHT_BRACER ) )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:4: ( COMMENT ( WHITESPACE )* MODEL_NAME ( WHITESPACE )* LEFT_BRACER t= name RIGHT_BRACER )
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:4: ( COMMENT ( WHITESPACE )* MODEL_NAME ( WHITESPACE )* LEFT_BRACER t= name RIGHT_BRACER )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:5: COMMENT ( WHITESPACE )* MODEL_NAME ( WHITESPACE )* LEFT_BRACER t= name RIGHT_BRACER
			{
			match(input,COMMENT,FOLLOW_COMMENT_in_model_name101); 
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:13: ( WHITESPACE )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( (LA2_0==WHITESPACE) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:13: WHITESPACE
					{
					match(input,WHITESPACE,FOLLOW_WHITESPACE_in_model_name103); 
					}
					break;

				default :
					break loop2;
				}
			}

			match(input,MODEL_NAME,FOLLOW_MODEL_NAME_in_model_name106); 
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:36: ( WHITESPACE )*
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( (LA3_0==WHITESPACE) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:50:36: WHITESPACE
					{
					match(input,WHITESPACE,FOLLOW_WHITESPACE_in_model_name108); 
					}
					break;

				default :
					break loop3;
				}
			}

			match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_model_name111); 
			pushFollow(FOLLOW_name_in_model_name115);
			t=name();
			state._fsp--;

			text = t;
			match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_model_name118); 
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return text;
	}
	// $ANTLR end "model_name"



	// $ANTLR start "properties"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:52:1: properties returns [List<IProperty> properties] : (p= property (ps= properties )* ) ;
	public final List<IProperty> properties() throws  RecognitionException {
		List<IProperty> properties = null;


		IProperty p =null;
		List<IProperty> ps =null;

		properties = new ArrayList<IProperty>();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:53:2: ( (p= property (ps= properties )* ) )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:53:4: (p= property (ps= properties )* )
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:53:4: (p= property (ps= properties )* )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:54:3: p= property (ps= properties )*
			{
			pushFollow(FOLLOW_property_in_properties147);
			p=property();
			state._fsp--;

			properties.add(p);
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:54:36: (ps= properties )*
			loop4:
			while (true) {
				int alt4=2;
				switch ( input.LA(1) ) {
				case WHITESPACE:
					{
					alt4=1;
					}
					break;
				case SET_BEGIN:
					{
					alt4=1;
					}
					break;
				case GROUP_TREE_BEGIN:
					{
					alt4=1;
					}
					break;
				}
				switch (alt4) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:54:37: ps= properties
					{
					pushFollow(FOLLOW_properties_in_properties155);
					ps=properties();
					state._fsp--;

					properties.addAll(ps);
					}
					break;

				default :
					break loop4;
				}
			}

			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return properties;
	}
	// $ANTLR end "properties"



	// $ANTLR start "property"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:57:1: property returns [IProperty property] : ( ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) ) ;
	public final IProperty property() throws  RecognitionException {
		IProperty property = null;


		String n =null;
		Boolean nll =null;
		Boolean m =null;
		List<IValue> v =null;
		List<IProperty> ch =null;

		String name = null; List<IValue> values = null; 
		Boolean mult = null; Boolean nul = null; List<IProperty> child = new LinkedList<IProperty>();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:59:2: ( ( ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) ) )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:59:4: ( ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) )
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:59:4: ( ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) )
			int alt25=4;
			alt25 = dfa25.predict(input);
			switch (alt25) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:3: ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END )
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:3: ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END )
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:4: ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:4: ( WHITESPACE )*
					loop5:
					while (true) {
						int alt5=2;
						int LA5_0 = input.LA(1);
						if ( (LA5_0==WHITESPACE) ) {
							alt5=1;
						}

						switch (alt5) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:4: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property184); 
							}
							break;

						default :
							break loop5;
						}
					}

					match(input,SET_BEGIN,FOLLOW_SET_BEGIN_in_property187); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:26: ( WHITESPACE )*
					loop6:
					while (true) {
						int alt6=2;
						int LA6_0 = input.LA(1);
						if ( (LA6_0==WHITESPACE) ) {
							alt6=1;
						}

						switch (alt6) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:26: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property189); 
							}
							break;

						default :
							break loop6;
						}
					}

					match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_property192); 
					pushFollow(FOLLOW_name_in_property198);
					n=name();
					state._fsp--;

					name = n;
					match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_property201); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:84: ( WHITESPACE )*
					loop7:
					while (true) {
						int alt7=2;
						int LA7_0 = input.LA(1);
						if ( (LA7_0==WHITESPACE) ) {
							alt7=1;
						}

						switch (alt7) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:84: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property204); 
							}
							break;

						default :
							break loop7;
						}
					}

					pushFollow(FOLLOW_nullable_in_property211);
					nll=nullable();
					state._fsp--;

					nul = nll;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:124: ( WHITESPACE )*
					loop8:
					while (true) {
						int alt8=2;
						int LA8_0 = input.LA(1);
						if ( (LA8_0==WHITESPACE) ) {
							alt8=1;
						}

						switch (alt8) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:60:124: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property215); 
							}
							break;

						default :
							break loop8;
						}
					}

					pushFollow(FOLLOW_multiple_in_property222);
					m=multiple();
					state._fsp--;

					mult = m;
					pushFollow(FOLLOW_valuesList_in_property233);
					v=valuesList();
					state._fsp--;

					values = v;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:62:4: ( WHITESPACE )*
					loop9:
					while (true) {
						int alt9=2;
						int LA9_0 = input.LA(1);
						if ( (LA9_0==WHITESPACE) ) {
							alt9=1;
						}

						switch (alt9) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:62:4: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property242); 
							}
							break;

						default :
							break loop9;
						}
					}

					match(input,SET_END,FOLLOW_SET_END_in_property246); 
					}

					property = IProperty.Factory.selectPropertyBuilder()
											.setName(name)
											.setNullable(nul)
											.setMultiple(mult)
											.setPropertyValues(values)
											.build();
					}
					break;
				case 2 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:7: ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END )
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:7: ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END )
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:8: ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:8: ( WHITESPACE )*
					loop10:
					while (true) {
						int alt10=2;
						int LA10_0 = input.LA(1);
						if ( (LA10_0==WHITESPACE) ) {
							alt10=1;
						}

						switch (alt10) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:8: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property258); 
							}
							break;

						default :
							break loop10;
						}
					}

					match(input,SET_BEGIN,FOLLOW_SET_BEGIN_in_property261); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:30: ( WHITESPACE )*
					loop11:
					while (true) {
						int alt11=2;
						int LA11_0 = input.LA(1);
						if ( (LA11_0==WHITESPACE) ) {
							alt11=1;
						}

						switch (alt11) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:30: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property263); 
							}
							break;

						default :
							break loop11;
						}
					}

					match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_property266); 
					pushFollow(FOLLOW_name_in_property270);
					n=name();
					state._fsp--;

					name = n;
					match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_property273); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:85: ( WHITESPACE )*
					loop12:
					while (true) {
						int alt12=2;
						int LA12_0 = input.LA(1);
						if ( (LA12_0==WHITESPACE) ) {
							alt12=1;
						}

						switch (alt12) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:85: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property275); 
							}
							break;

						default :
							break loop12;
						}
					}

					pushFollow(FOLLOW_multiple_in_property280);
					m=multiple();
					state._fsp--;

					mult = m;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:119: ( WHITESPACE )*
					loop13:
					while (true) {
						int alt13=2;
						int LA13_0 = input.LA(1);
						if ( (LA13_0==WHITESPACE) ) {
							alt13=1;
						}

						switch (alt13) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:68:119: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property283); 
							}
							break;

						default :
							break loop13;
						}
					}

					pushFollow(FOLLOW_nullable_in_property288);
					nll=nullable();
					state._fsp--;

					nul = nll;
					pushFollow(FOLLOW_valuesList_in_property299);
					v=valuesList();
					state._fsp--;

					values = v;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:70:4: ( WHITESPACE )*
					loop14:
					while (true) {
						int alt14=2;
						int LA14_0 = input.LA(1);
						if ( (LA14_0==WHITESPACE) ) {
							alt14=1;
						}

						switch (alt14) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:70:4: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property308); 
							}
							break;

						default :
							break loop14;
						}
					}

					match(input,SET_END,FOLLOW_SET_END_in_property311); 
					}

					property = IProperty.Factory.selectPropertyBuilder()
											.setName(name)
											.setNullable(nul)
											.setMultiple(mult)
											.setPropertyValues(values)
											.build();
					}
					break;
				case 3 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:5: ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END )
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:5: ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END )
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:6: ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:6: ( WHITESPACE )*
					loop15:
					while (true) {
						int alt15=2;
						int LA15_0 = input.LA(1);
						if ( (LA15_0==WHITESPACE) ) {
							alt15=1;
						}

						switch (alt15) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:6: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property321); 
							}
							break;

						default :
							break loop15;
						}
					}

					match(input,GROUP_TREE_BEGIN,FOLLOW_GROUP_TREE_BEGIN_in_property324); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:35: ( WHITESPACE )*
					loop16:
					while (true) {
						int alt16=2;
						int LA16_0 = input.LA(1);
						if ( (LA16_0==WHITESPACE) ) {
							alt16=1;
						}

						switch (alt16) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:35: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property326); 
							}
							break;

						default :
							break loop16;
						}
					}

					match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_property329); 
					pushFollow(FOLLOW_name_in_property333);
					n=name();
					state._fsp--;

					name = n;
					match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_property336); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:90: ( WHITESPACE )*
					loop17:
					while (true) {
						int alt17=2;
						int LA17_0 = input.LA(1);
						if ( (LA17_0==WHITESPACE) ) {
							alt17=1;
						}

						switch (alt17) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:90: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property338); 
							}
							break;

						default :
							break loop17;
						}
					}

					pushFollow(FOLLOW_multiple_in_property343);
					m=multiple();
					state._fsp--;

					mult = m;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:124: ( WHITESPACE )*
					loop18:
					while (true) {
						int alt18=2;
						int LA18_0 = input.LA(1);
						if ( (LA18_0==WHITESPACE) ) {
							alt18=1;
						}

						switch (alt18) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:76:124: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property346); 
							}
							break;

						default :
							break loop18;
						}
					}

					pushFollow(FOLLOW_nullable_in_property351);
					nll=nullable();
					state._fsp--;

					nul = nll;
					pushFollow(FOLLOW_valuesList_in_property361);
					v=valuesList();
					state._fsp--;

					values = v;
					pushFollow(FOLLOW_children_in_property369);
					ch=children();
					state._fsp--;

					child = ch;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:77:60: ( WHITESPACE )*
					loop19:
					while (true) {
						int alt19=2;
						int LA19_0 = input.LA(1);
						if ( (LA19_0==WHITESPACE) ) {
							alt19=1;
						}

						switch (alt19) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:77:60: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property372); 
							}
							break;

						default :
							break loop19;
						}
					}

					match(input,GROUP_TREE_END,FOLLOW_GROUP_TREE_END_in_property375); 
					}

					property = ITreeProperty.TreeFactory.treePropertyBuilder()
												.setProperties(ch)
												.setName(name)
												.setNullable(nll)
												.setMultiple(m)
												.setPropertyValues(values)
												.build();
					}
					break;
				case 4 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:5: ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END )
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:5: ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END )
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:6: ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:6: ( WHITESPACE )*
					loop20:
					while (true) {
						int alt20=2;
						int LA20_0 = input.LA(1);
						if ( (LA20_0==WHITESPACE) ) {
							alt20=1;
						}

						switch (alt20) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:6: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property389); 
							}
							break;

						default :
							break loop20;
						}
					}

					match(input,GROUP_TREE_BEGIN,FOLLOW_GROUP_TREE_BEGIN_in_property392); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:35: ( WHITESPACE )*
					loop21:
					while (true) {
						int alt21=2;
						int LA21_0 = input.LA(1);
						if ( (LA21_0==WHITESPACE) ) {
							alt21=1;
						}

						switch (alt21) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:35: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property394); 
							}
							break;

						default :
							break loop21;
						}
					}

					match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_property397); 
					pushFollow(FOLLOW_name_in_property401);
					n=name();
					state._fsp--;

					name = n;
					match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_property404); 
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:90: ( WHITESPACE )*
					loop22:
					while (true) {
						int alt22=2;
						int LA22_0 = input.LA(1);
						if ( (LA22_0==WHITESPACE) ) {
							alt22=1;
						}

						switch (alt22) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:90: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property406); 
							}
							break;

						default :
							break loop22;
						}
					}

					pushFollow(FOLLOW_nullable_in_property411);
					nll=nullable();
					state._fsp--;

					nul = nll;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:127: ( WHITESPACE )*
					loop23:
					while (true) {
						int alt23=2;
						int LA23_0 = input.LA(1);
						if ( (LA23_0==WHITESPACE) ) {
							alt23=1;
						}

						switch (alt23) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:85:127: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property414); 
							}
							break;

						default :
							break loop23;
						}
					}

					pushFollow(FOLLOW_multiple_in_property419);
					m=multiple();
					state._fsp--;

					mult = m;
					pushFollow(FOLLOW_valuesList_in_property430);
					v=valuesList();
					state._fsp--;

					values = v;
					pushFollow(FOLLOW_children_in_property438);
					ch=children();
					state._fsp--;

					child = ch;
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:86:60: ( WHITESPACE )*
					loop24:
					while (true) {
						int alt24=2;
						int LA24_0 = input.LA(1);
						if ( (LA24_0==WHITESPACE) ) {
							alt24=1;
						}

						switch (alt24) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:86:60: WHITESPACE
							{
							match(input,WHITESPACE,FOLLOW_WHITESPACE_in_property441); 
							}
							break;

						default :
							break loop24;
						}
					}

					match(input,GROUP_TREE_END,FOLLOW_GROUP_TREE_END_in_property444); 
					}

					property = ITreeProperty.TreeFactory.treePropertyBuilder()
												.setProperties(ch)
												.setName(name)
												.setNullable(nll)
												.setMultiple(m)
												.setPropertyValues(values)
												.build();
					}
					break;

			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return property;
	}
	// $ANTLR end "property"



	// $ANTLR start "children"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:96:1: children returns [List<IProperty> children] : (p= child )* ;
	public final List<IProperty> children() throws  RecognitionException {
		List<IProperty> children = null;


		IProperty p =null;

		children = new LinkedList<IProperty>();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:97:2: ( (p= child )* )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:97:4: (p= child )*
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:97:4: (p= child )*
			loop26:
			while (true) {
				int alt26=2;
				alt26 = dfa26.predict(input);
				switch (alt26) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:97:5: p= child
					{
					pushFollow(FOLLOW_child_in_children501);
					p=child();
					state._fsp--;

					children.add(p);
					}
					break;

				default :
					break loop26;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return children;
	}
	// $ANTLR end "children"



	// $ANTLR start "child"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:99:1: child returns [IProperty child] : ( ( WHITESPACE )* SUB_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER v= valuesList ch= children ( WHITESPACE )* SUB_TREE_END ) ;
	public final IProperty child() throws  RecognitionException {
		IProperty child = null;


		String n =null;
		List<IValue> v =null;
		List<IProperty> ch =null;

		String name = null; List<IValue> values = new LinkedList<IValue>(); List<IProperty> c = new LinkedList<IProperty>();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:2: ( ( ( WHITESPACE )* SUB_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER v= valuesList ch= children ( WHITESPACE )* SUB_TREE_END ) )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:4: ( ( WHITESPACE )* SUB_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER v= valuesList ch= children ( WHITESPACE )* SUB_TREE_END )
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:4: ( ( WHITESPACE )* SUB_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER v= valuesList ch= children ( WHITESPACE )* SUB_TREE_END )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:5: ( WHITESPACE )* SUB_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER v= valuesList ch= children ( WHITESPACE )* SUB_TREE_END
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:5: ( WHITESPACE )*
			loop27:
			while (true) {
				int alt27=2;
				int LA27_0 = input.LA(1);
				if ( (LA27_0==WHITESPACE) ) {
					alt27=1;
				}

				switch (alt27) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:5: WHITESPACE
					{
					match(input,WHITESPACE,FOLLOW_WHITESPACE_in_child523); 
					}
					break;

				default :
					break loop27;
				}
			}

			match(input,SUB_TREE_BEGIN,FOLLOW_SUB_TREE_BEGIN_in_child526); 
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:32: ( WHITESPACE )*
			loop28:
			while (true) {
				int alt28=2;
				int LA28_0 = input.LA(1);
				if ( (LA28_0==WHITESPACE) ) {
					alt28=1;
				}

				switch (alt28) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:32: WHITESPACE
					{
					match(input,WHITESPACE,FOLLOW_WHITESPACE_in_child528); 
					}
					break;

				default :
					break loop28;
				}
			}

			match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_child531); 
			pushFollow(FOLLOW_name_in_child535);
			n=name();
			state._fsp--;

			name = n;
			match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_child538); 
			pushFollow(FOLLOW_valuesList_in_child545);
			v=valuesList();
			state._fsp--;

			values = v;
			pushFollow(FOLLOW_children_in_child553);
			ch=children();
			state._fsp--;

			c = ch;
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:140: ( WHITESPACE )*
			loop29:
			while (true) {
				int alt29=2;
				int LA29_0 = input.LA(1);
				if ( (LA29_0==WHITESPACE) ) {
					alt29=1;
				}

				switch (alt29) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:100:140: WHITESPACE
					{
					match(input,WHITESPACE,FOLLOW_WHITESPACE_in_child556); 
					}
					break;

				default :
					break loop29;
				}
			}

			match(input,SUB_TREE_END,FOLLOW_SUB_TREE_END_in_child559); 
			}

			child = ITreeProperty.TreeFactory.treePropertyBuilder()
										.setProperties(c)
										.setName(name)
										.setNullable(false)
										.setMultiple(false)
										.setPropertyValues(values)
										.build();
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return child;
	}
	// $ANTLR end "child"



	// $ANTLR start "valuesList"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:109:1: valuesList returns [List<IValue> values] : ( ( ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) ) )* ) ;
	public final List<IValue> valuesList() throws  RecognitionException {
		List<IValue> values = null;


		String n =null;

		values = new LinkedList<IValue>();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:110:2: ( ( ( ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) ) )* ) )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:110:4: ( ( ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) ) )* )
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:110:4: ( ( ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) ) )* )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:111:3: ( ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) ) )*
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:111:3: ( ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) ) )*
			loop35:
			while (true) {
				int alt35=2;
				alt35 = dfa35.predict(input);
				switch (alt35) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:4: ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) )
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:4: ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) )
					int alt34=2;
					alt34 = dfa34.predict(input);
					switch (alt34) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:5: ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER )
							{
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:5: ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER )
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:6: ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER
							{
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:6: ( WHITESPACE )*
							loop30:
							while (true) {
								int alt30=2;
								int LA30_0 = input.LA(1);
								if ( (LA30_0==WHITESPACE) ) {
									alt30=1;
								}

								switch (alt30) {
								case 1 :
									// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:6: WHITESPACE
									{
									match(input,WHITESPACE,FOLLOW_WHITESPACE_in_valuesList594); 
									}
									break;

								default :
									break loop30;
								}
							}

							match(input,SET_ITEM,FOLLOW_SET_ITEM_in_valuesList597); 
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:28: ( WHITESPACE )*
							loop31:
							while (true) {
								int alt31=2;
								int LA31_0 = input.LA(1);
								if ( (LA31_0==WHITESPACE) ) {
									alt31=1;
								}

								switch (alt31) {
								case 1 :
									// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:112:28: WHITESPACE
									{
									match(input,WHITESPACE,FOLLOW_WHITESPACE_in_valuesList600); 
									}
									break;

								default :
									break loop31;
								}
							}

							match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_valuesList603); 
							pushFollow(FOLLOW_name_in_valuesList607);
							n=name();
							state._fsp--;

							values.add(IValue.Factory.createStringValue(n));
							match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_valuesList610); 
							}

							}
							break;
						case 2 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:114:4: ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER )
							{
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:114:4: ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER )
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:114:5: ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER
							{
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:114:5: ( WHITESPACE )*
							loop32:
							while (true) {
								int alt32=2;
								int LA32_0 = input.LA(1);
								if ( (LA32_0==WHITESPACE) ) {
									alt32=1;
								}

								switch (alt32) {
								case 1 :
									// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:114:5: WHITESPACE
									{
									match(input,WHITESPACE,FOLLOW_WHITESPACE_in_valuesList623); 
									}
									break;

								default :
									break loop32;
								}
							}

							match(input,TREE_ITEM,FOLLOW_TREE_ITEM_in_valuesList626); 
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:114:27: ( WHITESPACE )*
							loop33:
							while (true) {
								int alt33=2;
								int LA33_0 = input.LA(1);
								if ( (LA33_0==WHITESPACE) ) {
									alt33=1;
								}

								switch (alt33) {
								case 1 :
									// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:114:27: WHITESPACE
									{
									match(input,WHITESPACE,FOLLOW_WHITESPACE_in_valuesList628); 
									}
									break;

								default :
									break loop33;
								}
							}

							match(input,LEFT_BRACER,FOLLOW_LEFT_BRACER_in_valuesList631); 
							pushFollow(FOLLOW_name_in_valuesList635);
							n=name();
							state._fsp--;

							values.add(IValue.Factory.createStringValue(n));
							match(input,RIGHT_BRACER,FOLLOW_RIGHT_BRACER_in_valuesList638); 
							}

							}
							break;

					}

					}
					break;

				default :
					break loop35;
				}
			}

			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
        return values;
	}
	// $ANTLR end "valuesList"



	// $ANTLR start "multiple"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:118:1: multiple returns [Boolean b] : ( MULTI_SELECT | SINGLE_SELECT );
	public final Boolean multiple() throws  RecognitionException {
		Boolean b = null;


		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:119:2: ( MULTI_SELECT | SINGLE_SELECT )
			int alt36=2;
			int LA36_0 = input.LA(1);
			if ( (LA36_0==MULTI_SELECT) ) {
				alt36=1;
			}
			else if ( (LA36_0==SINGLE_SELECT) ) {
				alt36=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 36, 0, input);
				throw nvae;
			}

			switch (alt36) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:119:4: MULTI_SELECT
					{
					match(input,MULTI_SELECT,FOLLOW_MULTI_SELECT_in_multiple662); 
					b = Boolean.TRUE;
					}
					break;
				case 2 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:119:39: SINGLE_SELECT
					{
					match(input,SINGLE_SELECT,FOLLOW_SINGLE_SELECT_in_multiple668); 
					b = Boolean.FALSE;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return b;
	}
	// $ANTLR end "multiple"



	// $ANTLR start "nullable"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:121:1: nullable returns [Boolean b] : ( NULLABLE_PROP | NON_NULLABLE_PROP );
	public final Boolean nullable() throws  RecognitionException {
		Boolean b = null;


		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:122:2: ( NULLABLE_PROP | NON_NULLABLE_PROP )
			int alt37=2;
			int LA37_0 = input.LA(1);
			if ( (LA37_0==NULLABLE_PROP) ) {
				alt37=1;
			}
			else if ( (LA37_0==NON_NULLABLE_PROP) ) {
				alt37=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 37, 0, input);
				throw nvae;
			}

			switch (alt37) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:122:4: NULLABLE_PROP
					{
					match(input,NULLABLE_PROP,FOLLOW_NULLABLE_PROP_in_nullable684); 
					b = Boolean.TRUE;
					}
					break;
				case 2 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:122:40: NON_NULLABLE_PROP
					{
					match(input,NON_NULLABLE_PROP,FOLLOW_NON_NULLABLE_PROP_in_nullable690); 
					b = Boolean.FALSE;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return b;
	}
	// $ANTLR end "nullable"



	// $ANTLR start "name"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:124:1: name returns [String str] : ( (ws= WHITESPACE )* t= textWithSpaces ) ;
	public final String name() throws  RecognitionException {
		String str = null;


		Token ws=null;
		String t =null;

		StringBuilder sb = new StringBuilder();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:125:2: ( ( (ws= WHITESPACE )* t= textWithSpaces ) )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:125:4: ( (ws= WHITESPACE )* t= textWithSpaces )
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:125:4: ( (ws= WHITESPACE )* t= textWithSpaces )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:125:5: (ws= WHITESPACE )* t= textWithSpaces
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:125:5: (ws= WHITESPACE )*
			loop38:
			while (true) {
				int alt38=2;
				int LA38_0 = input.LA(1);
				if ( (LA38_0==WHITESPACE) ) {
					alt38=1;
				}

				switch (alt38) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:125:6: ws= WHITESPACE
					{
					ws=(Token)match(input,WHITESPACE,FOLLOW_WHITESPACE_in_name719); 
					sb.append((ws!=null?ws.getText():null));
					}
					break;

				default :
					break loop38;
				}
			}

			pushFollow(FOLLOW_textWithSpaces_in_name729);
			t=textWithSpaces();
			state._fsp--;

			sb.append(t);
			}

			str = sb.toString();
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return str;
	}
	// $ANTLR end "name"



	// $ANTLR start "textWithSpaces"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:127:1: textWithSpaces returns [String text] : (t= someText ( (ws= WHITESPACE )+ t= someText )* ) ;
	public final String textWithSpaces() throws  RecognitionException {
		String text = null;


		Token ws=null;
		String t =null;

		StringBuilder sb = new StringBuilder();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:2: ( (t= someText ( (ws= WHITESPACE )+ t= someText )* ) )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:4: (t= someText ( (ws= WHITESPACE )+ t= someText )* )
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:4: (t= someText ( (ws= WHITESPACE )+ t= someText )* )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:5: t= someText ( (ws= WHITESPACE )+ t= someText )*
			{
			pushFollow(FOLLOW_someText_in_textWithSpaces756);
			t=someText();
			state._fsp--;

			sb.append(t);
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:32: ( (ws= WHITESPACE )+ t= someText )*
			loop40:
			while (true) {
				int alt40=2;
				int LA40_0 = input.LA(1);
				if ( (LA40_0==WHITESPACE) ) {
					alt40=1;
				}

				switch (alt40) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:33: (ws= WHITESPACE )+ t= someText
					{
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:33: (ws= WHITESPACE )+
					int cnt39=0;
					loop39:
					while (true) {
						int alt39=2;
						int LA39_0 = input.LA(1);
						if ( (LA39_0==WHITESPACE) ) {
							alt39=1;
						}

						switch (alt39) {
						case 1 :
							// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:128:34: ws= WHITESPACE
							{
							ws=(Token)match(input,WHITESPACE,FOLLOW_WHITESPACE_in_textWithSpaces764); 
							sb.append((ws!=null?ws.getText():null));
							}
							break;

						default :
							if ( cnt39 >= 1 ) break loop39;
							EarlyExitException eee = new EarlyExitException(39, input);
							throw eee;
						}
						cnt39++;
					}

					pushFollow(FOLLOW_someText_in_textWithSpaces772);
					t=someText();
					state._fsp--;

					sb.append(t);
					}
					break;

				default :
					break loop40;
				}
			}

			}

			text = sb.toString();
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return text;
	}
	// $ANTLR end "textWithSpaces"



	// $ANTLR start "someText"
	// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:130:1: someText returns [String text] : (v= OTHER_SYMBOLS )* ;
	public final String someText() throws  RecognitionException {
		String text = null;


		Token v=null;

		StringBuilder sb = new StringBuilder();
		try {
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:131:2: ( (v= OTHER_SYMBOLS )* )
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:131:3: (v= OTHER_SYMBOLS )*
			{
			// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:131:3: (v= OTHER_SYMBOLS )*
			loop41:
			while (true) {
				int alt41=2;
				int LA41_0 = input.LA(1);
				if ( (LA41_0==OTHER_SYMBOLS) ) {
					alt41=1;
				}

				switch (alt41) {
				case 1 :
					// E:\\Projects\\jsm-dm\\src\\org\\edu\\jsm\\parser\\JDM.g:131:4: v= OTHER_SYMBOLS
					{
					v=(Token)match(input,OTHER_SYMBOLS,FOLLOW_OTHER_SYMBOLS_in_someText800); 
					sb.append((v!=null?v.getText():null));
					}
					break;

				default :
					break loop41;
				}
			}

			text = sb.toString();
			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return text;
	}
	// $ANTLR end "someText"

	// Delegated rules


	protected DFA25 dfa25 = new DFA25(this);
	protected DFA26 dfa26 = new DFA26(this);
	protected DFA35 dfa35 = new DFA35(this);
	protected DFA34 dfa34 = new DFA34(this);
	static final String DFA25_eotS =
		"\32\uffff";
	static final String DFA25_eofS =
		"\32\uffff";
	static final String DFA25_minS =
		"\2\5\3\7\1\14\1\7\3\14\1\11\2\14\1\11\2\14\1\11\2\uffff\2\14\1\11\2\uffff"+
		"\2\14";
	static final String DFA25_maxS =
		"\21\25\2\uffff\3\25\2\uffff\2\25";
	static final String DFA25_acceptS =
		"\21\uffff\1\1\1\2\3\uffff\1\3\1\4\2\uffff";
	static final String DFA25_specialS =
		"\32\uffff}>";
	static final String[] DFA25_transitionS = {
			"\1\3\10\uffff\1\2\6\uffff\1\1",
			"\1\3\10\uffff\1\2\6\uffff\1\1",
			"\1\5\15\uffff\1\4",
			"\1\7\15\uffff\1\6",
			"\1\5\15\uffff\1\4",
			"\1\11\1\12\7\uffff\1\10",
			"\1\7\15\uffff\1\6",
			"\1\14\1\15\7\uffff\1\13",
			"\1\16\1\12\7\uffff\1\10",
			"\1\11\1\12\7\uffff\1\17",
			"\1\22\2\21\5\uffff\1\22\3\uffff\1\20",
			"\1\23\1\15\7\uffff\1\13",
			"\1\14\1\15\7\uffff\1\24",
			"\1\26\2\27\5\uffff\1\26\3\uffff\1\25",
			"\1\16\1\12\7\uffff\1\17",
			"\1\30\1\12\7\uffff\1\17",
			"\1\22\2\21\5\uffff\1\22\3\uffff\1\20",
			"",
			"",
			"\1\23\1\15\7\uffff\1\24",
			"\1\31\1\15\7\uffff\1\24",
			"\1\26\2\27\5\uffff\1\26\3\uffff\1\25",
			"",
			"",
			"\1\30\1\12\7\uffff\1\17",
			"\1\31\1\15\7\uffff\1\24"
	};

	static final short[] DFA25_eot = DFA.unpackEncodedString(DFA25_eotS);
	static final short[] DFA25_eof = DFA.unpackEncodedString(DFA25_eofS);
	static final char[] DFA25_min = DFA.unpackEncodedStringToUnsignedChars(DFA25_minS);
	static final char[] DFA25_max = DFA.unpackEncodedStringToUnsignedChars(DFA25_maxS);
	static final short[] DFA25_accept = DFA.unpackEncodedString(DFA25_acceptS);
	static final short[] DFA25_special = DFA.unpackEncodedString(DFA25_specialS);
	static final short[][] DFA25_transition;

	static {
		int numStates = DFA25_transitionS.length;
		DFA25_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA25_transition[i] = DFA.unpackEncodedString(DFA25_transitionS[i]);
		}
	}

	protected class DFA25 extends DFA {

		public DFA25(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 25;
			this.eot = DFA25_eot;
			this.eof = DFA25_eof;
			this.min = DFA25_min;
			this.max = DFA25_max;
			this.accept = DFA25_accept;
			this.special = DFA25_special;
			this.transition = DFA25_transition;
		}
		@Override
		public String getDescription() {
			return "59:4: ( ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* SET_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ( WHITESPACE )* SET_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* m= multiple ( WHITESPACE )* nll= nullable v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) | ( ( WHITESPACE )* GROUP_TREE_BEGIN ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ( WHITESPACE )* nll= nullable ( WHITESPACE )* m= multiple v= valuesList ch= children ( WHITESPACE )* GROUP_TREE_END ) )";
		}
	}

	static final String DFA26_eotS =
		"\4\uffff";
	static final String DFA26_eofS =
		"\4\uffff";
	static final String DFA26_minS =
		"\2\6\2\uffff";
	static final String DFA26_maxS =
		"\2\25\2\uffff";
	static final String DFA26_acceptS =
		"\2\uffff\1\2\1\1";
	static final String DFA26_specialS =
		"\4\uffff}>";
	static final String[] DFA26_transitionS = {
			"\1\2\13\uffff\1\3\1\2\1\uffff\1\1",
			"\1\2\13\uffff\1\3\1\2\1\uffff\1\1",
			"",
			""
	};

	static final short[] DFA26_eot = DFA.unpackEncodedString(DFA26_eotS);
	static final short[] DFA26_eof = DFA.unpackEncodedString(DFA26_eofS);
	static final char[] DFA26_min = DFA.unpackEncodedStringToUnsignedChars(DFA26_minS);
	static final char[] DFA26_max = DFA.unpackEncodedStringToUnsignedChars(DFA26_maxS);
	static final short[] DFA26_accept = DFA.unpackEncodedString(DFA26_acceptS);
	static final short[] DFA26_special = DFA.unpackEncodedString(DFA26_specialS);
	static final short[][] DFA26_transition;

	static {
		int numStates = DFA26_transitionS.length;
		DFA26_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA26_transition[i] = DFA.unpackEncodedString(DFA26_transitionS[i]);
		}
	}

	protected class DFA26 extends DFA {

		public DFA26(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 26;
			this.eot = DFA26_eot;
			this.eof = DFA26_eof;
			this.min = DFA26_min;
			this.max = DFA26_max;
			this.accept = DFA26_accept;
			this.special = DFA26_special;
			this.transition = DFA26_transition;
		}
		@Override
		public String getDescription() {
			return "()* loopback of 97:4: (p= child )*";
		}
	}

	static final String DFA35_eotS =
		"\4\uffff";
	static final String DFA35_eofS =
		"\4\uffff";
	static final String DFA35_minS =
		"\2\6\2\uffff";
	static final String DFA35_maxS =
		"\2\25\2\uffff";
	static final String DFA35_acceptS =
		"\2\uffff\1\2\1\1";
	static final String DFA35_specialS =
		"\4\uffff}>";
	static final String[] DFA35_transitionS = {
			"\1\2\10\uffff\1\2\1\3\1\uffff\2\2\1\3\1\1",
			"\1\2\10\uffff\1\2\1\3\1\uffff\2\2\1\3\1\1",
			"",
			""
	};

	static final short[] DFA35_eot = DFA.unpackEncodedString(DFA35_eotS);
	static final short[] DFA35_eof = DFA.unpackEncodedString(DFA35_eofS);
	static final char[] DFA35_min = DFA.unpackEncodedStringToUnsignedChars(DFA35_minS);
	static final char[] DFA35_max = DFA.unpackEncodedStringToUnsignedChars(DFA35_maxS);
	static final short[] DFA35_accept = DFA.unpackEncodedString(DFA35_acceptS);
	static final short[] DFA35_special = DFA.unpackEncodedString(DFA35_specialS);
	static final short[][] DFA35_transition;

	static {
		int numStates = DFA35_transitionS.length;
		DFA35_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA35_transition[i] = DFA.unpackEncodedString(DFA35_transitionS[i]);
		}
	}

	protected class DFA35 extends DFA {

		public DFA35(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 35;
			this.eot = DFA35_eot;
			this.eof = DFA35_eof;
			this.min = DFA35_min;
			this.max = DFA35_max;
			this.accept = DFA35_accept;
			this.special = DFA35_special;
			this.transition = DFA35_transition;
		}
		@Override
		public String getDescription() {
			return "()* loopback of 111:3: ( ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) ) )*";
		}
	}

	static final String DFA34_eotS =
		"\4\uffff";
	static final String DFA34_eofS =
		"\4\uffff";
	static final String DFA34_minS =
		"\2\20\2\uffff";
	static final String DFA34_maxS =
		"\2\25\2\uffff";
	static final String DFA34_acceptS =
		"\2\uffff\1\1\1\2";
	static final String DFA34_specialS =
		"\4\uffff}>";
	static final String[] DFA34_transitionS = {
			"\1\2\3\uffff\1\3\1\1",
			"\1\2\3\uffff\1\3\1\1",
			"",
			""
	};

	static final short[] DFA34_eot = DFA.unpackEncodedString(DFA34_eotS);
	static final short[] DFA34_eof = DFA.unpackEncodedString(DFA34_eofS);
	static final char[] DFA34_min = DFA.unpackEncodedStringToUnsignedChars(DFA34_minS);
	static final char[] DFA34_max = DFA.unpackEncodedStringToUnsignedChars(DFA34_maxS);
	static final short[] DFA34_accept = DFA.unpackEncodedString(DFA34_acceptS);
	static final short[] DFA34_special = DFA.unpackEncodedString(DFA34_specialS);
	static final short[][] DFA34_transition;

	static {
		int numStates = DFA34_transitionS.length;
		DFA34_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA34_transition[i] = DFA.unpackEncodedString(DFA34_transitionS[i]);
		}
	}

	protected class DFA34 extends DFA {

		public DFA34(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 34;
			this.eot = DFA34_eot;
			this.eof = DFA34_eof;
			this.min = DFA34_min;
			this.max = DFA34_max;
			this.accept = DFA34_accept;
			this.special = DFA34_special;
			this.transition = DFA34_transition;
		}
		@Override
		public String getDescription() {
			return "112:4: ( ( ( WHITESPACE )* SET_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) | ( ( WHITESPACE )* TREE_ITEM ( WHITESPACE )* LEFT_BRACER n= name RIGHT_BRACER ) )";
		}
	}

	public static final BitSet FOLLOW_model_name_in_model61 = new BitSet(new long[]{0x0000000000204020L});
	public static final BitSet FOLLOW_properties_in_model67 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_properties_in_model82 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_COMMENT_in_model_name101 = new BitSet(new long[]{0x0000000000200100L});
	public static final BitSet FOLLOW_WHITESPACE_in_model_name103 = new BitSet(new long[]{0x0000000000200100L});
	public static final BitSet FOLLOW_MODEL_NAME_in_model_name106 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_model_name108 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_model_name111 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_model_name115 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_model_name118 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_property_in_properties147 = new BitSet(new long[]{0x0000000000204022L});
	public static final BitSet FOLLOW_properties_in_properties155 = new BitSet(new long[]{0x0000000000204022L});
	public static final BitSet FOLLOW_WHITESPACE_in_property184 = new BitSet(new long[]{0x0000000000204000L});
	public static final BitSet FOLLOW_SET_BEGIN_in_property187 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_property189 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_property192 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_property198 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_property201 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_WHITESPACE_in_property204 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_nullable_in_property211 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_WHITESPACE_in_property215 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_multiple_in_property222 = new BitSet(new long[]{0x0000000000318000L});
	public static final BitSet FOLLOW_valuesList_in_property233 = new BitSet(new long[]{0x0000000000208000L});
	public static final BitSet FOLLOW_WHITESPACE_in_property242 = new BitSet(new long[]{0x0000000000208000L});
	public static final BitSet FOLLOW_SET_END_in_property246 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHITESPACE_in_property258 = new BitSet(new long[]{0x0000000000204000L});
	public static final BitSet FOLLOW_SET_BEGIN_in_property261 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_property263 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_property266 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_property270 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_property273 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_WHITESPACE_in_property275 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_multiple_in_property280 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_WHITESPACE_in_property283 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_nullable_in_property288 = new BitSet(new long[]{0x0000000000318000L});
	public static final BitSet FOLLOW_valuesList_in_property299 = new BitSet(new long[]{0x0000000000208000L});
	public static final BitSet FOLLOW_WHITESPACE_in_property308 = new BitSet(new long[]{0x0000000000208000L});
	public static final BitSet FOLLOW_SET_END_in_property311 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHITESPACE_in_property321 = new BitSet(new long[]{0x0000000000200020L});
	public static final BitSet FOLLOW_GROUP_TREE_BEGIN_in_property324 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_property326 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_property329 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_property333 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_property336 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_WHITESPACE_in_property338 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_multiple_in_property343 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_WHITESPACE_in_property346 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_nullable_in_property351 = new BitSet(new long[]{0x0000000000350040L});
	public static final BitSet FOLLOW_valuesList_in_property361 = new BitSet(new long[]{0x0000000000240040L});
	public static final BitSet FOLLOW_children_in_property369 = new BitSet(new long[]{0x0000000000200040L});
	public static final BitSet FOLLOW_WHITESPACE_in_property372 = new BitSet(new long[]{0x0000000000200040L});
	public static final BitSet FOLLOW_GROUP_TREE_END_in_property375 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHITESPACE_in_property389 = new BitSet(new long[]{0x0000000000200020L});
	public static final BitSet FOLLOW_GROUP_TREE_BEGIN_in_property392 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_property394 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_property397 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_property401 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_property404 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_WHITESPACE_in_property406 = new BitSet(new long[]{0x0000000000200C00L});
	public static final BitSet FOLLOW_nullable_in_property411 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_WHITESPACE_in_property414 = new BitSet(new long[]{0x0000000000220200L});
	public static final BitSet FOLLOW_multiple_in_property419 = new BitSet(new long[]{0x0000000000350040L});
	public static final BitSet FOLLOW_valuesList_in_property430 = new BitSet(new long[]{0x0000000000240040L});
	public static final BitSet FOLLOW_children_in_property438 = new BitSet(new long[]{0x0000000000200040L});
	public static final BitSet FOLLOW_WHITESPACE_in_property441 = new BitSet(new long[]{0x0000000000200040L});
	public static final BitSet FOLLOW_GROUP_TREE_END_in_property444 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_child_in_children501 = new BitSet(new long[]{0x0000000000240002L});
	public static final BitSet FOLLOW_WHITESPACE_in_child523 = new BitSet(new long[]{0x0000000000240000L});
	public static final BitSet FOLLOW_SUB_TREE_BEGIN_in_child526 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_child528 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_child531 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_child535 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_child538 = new BitSet(new long[]{0x00000000003D0000L});
	public static final BitSet FOLLOW_valuesList_in_child545 = new BitSet(new long[]{0x00000000002C0000L});
	public static final BitSet FOLLOW_children_in_child553 = new BitSet(new long[]{0x0000000000280000L});
	public static final BitSet FOLLOW_WHITESPACE_in_child556 = new BitSet(new long[]{0x0000000000280000L});
	public static final BitSet FOLLOW_SUB_TREE_END_in_child559 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHITESPACE_in_valuesList594 = new BitSet(new long[]{0x0000000000210000L});
	public static final BitSet FOLLOW_SET_ITEM_in_valuesList597 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_valuesList600 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_valuesList603 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_valuesList607 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_valuesList610 = new BitSet(new long[]{0x0000000000310002L});
	public static final BitSet FOLLOW_WHITESPACE_in_valuesList623 = new BitSet(new long[]{0x0000000000300000L});
	public static final BitSet FOLLOW_TREE_ITEM_in_valuesList626 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_WHITESPACE_in_valuesList628 = new BitSet(new long[]{0x0000000000200080L});
	public static final BitSet FOLLOW_LEFT_BRACER_in_valuesList631 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_name_in_valuesList635 = new BitSet(new long[]{0x0000000000002000L});
	public static final BitSet FOLLOW_RIGHT_BRACER_in_valuesList638 = new BitSet(new long[]{0x0000000000310002L});
	public static final BitSet FOLLOW_MULTI_SELECT_in_multiple662 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_SINGLE_SELECT_in_multiple668 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NULLABLE_PROP_in_nullable684 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NON_NULLABLE_PROP_in_nullable690 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_WHITESPACE_in_name719 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_textWithSpaces_in_name729 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_someText_in_textWithSpaces756 = new BitSet(new long[]{0x0000000000200002L});
	public static final BitSet FOLLOW_WHITESPACE_in_textWithSpaces764 = new BitSet(new long[]{0x0000000000201000L});
	public static final BitSet FOLLOW_someText_in_textWithSpaces772 = new BitSet(new long[]{0x0000000000200002L});
	public static final BitSet FOLLOW_OTHER_SYMBOLS_in_someText800 = new BitSet(new long[]{0x0000000000001002L});
}
