grammar JDM;


@lexer::header {
package org.edu.jsm.parser.output;
}

@lexer::members {
@Override
public void reportError(RecognitionException e) {
  super.reportError(e);
  throw new RuntimeException(e);
}
}

@parser::header {
package org.edu.jsm.parser.output;

import java.util.ArrayList;
import java.util.LinkedList;

import IModel;
import IProperty;
import ITreeProperty;
import IValue;


}


@parser::members {
@Override
public void reportError(RecognitionException e) {
  super.reportError(e);
  throw new RuntimeException(e);
}
// for debug purposes
private void sout(Object o) {System.out.println(o);}
}

/*
    CUSTOM_COMPARISON_NODE - ???
*/
model	returns [IModel iModel]	
	: (n = model_name props = properties {iModel = IModel.Factory.createModel(n, props);})
	  | (props = properties {iModel = IModel.Factory.createModel("model", props);})
	;

model_name returns [String text]
	: (COMMENT WHITESPACE* MODEL_NAME WHITESPACE* LEFT_BRACER t=name{text = t;} RIGHT_BRACER); 
	
properties returns [List<IProperty> properties] @init {properties = new ArrayList<IProperty>();}
	: (
		p = property{properties.add(p);} (ps = properties {properties.addAll(ps);})*
	);

property returns [IProperty property] @init{String name = null; List<IValue> values = null; 
Boolean mult = null; Boolean nul = null; List<IProperty> child = new LinkedList<IProperty>();}
	: (
		(WHITESPACE* SET_BEGIN WHITESPACE* LEFT_BRACER n = name{name = n;} RIGHT_BRACER  WHITESPACE* nll = nullable{nul = nll;}  WHITESPACE* m = multiple{mult = m;} 
			v = valuesList {values = v;}		
		 WHITESPACE*  SET_END) {property = IProperty.Factory.selectPropertyBuilder()
						.setName(name)
						.setNullable(nul)
						.setMultiple(mult)
						.setPropertyValues(values)
						.build();}
  		| (WHITESPACE* SET_BEGIN WHITESPACE* LEFT_BRACER n=name{name = n;} RIGHT_BRACER WHITESPACE* m=multiple{mult = m;} WHITESPACE* nll=nullable{nul = nll;} 
			v = valuesList {values = v;}		
		 WHITESPACE* SET_END) {property = IProperty.Factory.selectPropertyBuilder()
						.setName(name)
						.setNullable(nul)
						.setMultiple(mult)
						.setPropertyValues(values)
						.build();}
		| (WHITESPACE* GROUP_TREE_BEGIN WHITESPACE* LEFT_BRACER n=name{name = n;} RIGHT_BRACER WHITESPACE* m=multiple{mult = m;} WHITESPACE* nll=nullable{nul = nll;}
			v = valuesList {values = v;} ch = children{child = ch;} WHITESPACE* GROUP_TREE_END) 
			{property = ITreeProperty.Factory.treePropertyBuilder()
							.setProperties(ch)
							.setName(name)
							.setNullable(nll)
							.setMultiple(m)
							.setPropertyValues(values)
							.build();}
		| (WHITESPACE* GROUP_TREE_BEGIN WHITESPACE* LEFT_BRACER n=name{name = n;} RIGHT_BRACER WHITESPACE* nll=nullable{nul = nll;} WHITESPACE* m=multiple{mult = m;} 
			v = valuesList {values = v;} ch = children{child = ch;} WHITESPACE* GROUP_TREE_END) 
			{property = ITreeProperty.Factory.treePropertyBuilder()
							.setProperties(ch)
							.setName(name)
							.setNullable(nll)
							.setMultiple(m)
							.setPropertyValues(values)
							.build();}	        					 						
	); 	
	
children returns [List<IProperty> children] @init{children = new LinkedList<IProperty>();} 
	: (p = child{children.add(p);})*;

child returns [IProperty child] @init{String name = null; List<IValue> values = new LinkedList<IValue>(); List<IProperty> c = new LinkedList<IProperty>();} 
	: (WHITESPACE* SUB_TREE_BEGIN WHITESPACE* LEFT_BRACER n=name{name = n;} RIGHT_BRACER  v = valuesList {values = v;} ch = children{c = ch;} WHITESPACE* SUB_TREE_END) 
			{child = ITreeProperty.Factory.treePropertyBuilder()
							.setProperties(c)
							.setName(name)
							.setNullable(false)
							.setMultiple(false)
							.setPropertyValues(values)
							.build();};

valuesList returns [List<IValue> values] @init{values = new LinkedList<IValue>();}
	: (
		(
			((WHITESPACE* SET_ITEM  WHITESPACE* LEFT_BRACER n=name{values.add(IValue.Factory.createStringValue(n));} RIGHT_BRACER)
			| 
			(WHITESPACE* TREE_ITEM WHITESPACE* LEFT_BRACER n=name{values.add(IValue.Factory.createStringValue(n));} RIGHT_BRACER))
		)*
	);
	
multiple returns [Boolean b]
	:	MULTI_SELECT {b = Boolean.TRUE;} | SINGLE_SELECT {b = Boolean.FALSE;};
	
nullable returns [Boolean b]
	:	NULLABLE_PROP {b = Boolean.TRUE;} | NON_NULLABLE_PROP {b = Boolean.FALSE;};	
		
name returns [String str] @init{StringBuilder sb = new StringBuilder();} 
	: ((ws = WHITESPACE {sb.append($ws.text);})* t = textWithSpaces {sb.append($t.text);}) {str = sb.toString();};	
	
textWithSpaces returns [String text] @init{StringBuilder sb = new StringBuilder();}
	: (t=someText {sb.append(t);} ((ws=WHITESPACE {sb.append($ws.text);})+ t=someText {sb.append(t);})*) {text = sb.toString();};
		
someText returns [String text] @init{StringBuilder sb = new StringBuilder();}
	:(v=OTHER_SYMBOLS {sb.append($v.text);})* {text = sb.toString();};	

WHITESPACE : ( ' '
        | '\t'
        | '\r'
        | '\n'
        );
LEFT_BRACER :	 '{';
RIGHT_BRACER :	 '}';

SET_BEGIN : 	'Set';
SET_ITEM :	'ItemSet';	 
SET_END :	'EndSet';


GROUP_TREE_BEGIN :	'GroupTree';
SUB_TREE_BEGIN : 	'Tree';
SUB_TREE_END : 		'EndTree';
TREE_ITEM : 		'ItemTree';
GROUP_TREE_END : 	'EndGroupTree';

SINGLE_SELECT 
	:	'SingleSelect';
MULTI_SELECT 
	:	'MultiSelect';	
	
NULLABLE_PROP: 'Value=(+-t)';
NON_NULLABLE_PROP
	:	'Value=(+-)';
	
COMMENT	:	'\\';
MODEL_NAME
	:	'ModelName';	
			
OTHER_SYMBOLS : .;