package org.jsm.solver;

import java.util.List;

/**
 * @author Anatoly Semenov
 * 24.08.2015
 */
public interface Inductor {
    List<Hypothesis> induction(BaseFacts baseFacts);
}
