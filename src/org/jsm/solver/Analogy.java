package org.jsm.solver;

import com.google.common.collect.Multimap;

import java.util.List;

/**
 * @author Anatoly Semenov
 *         27.10.2015
 */
public interface Analogy {
    Multimap<Hypothesis.Type, Hypothesis> analogy(List<Hypothesis> positive, BaseFacts positiveFacts,
                                                  List<Hypothesis> negative, BaseFacts negativeFacts);
}
