package org.jsm.solver;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import org.jsm.common.Utils;
import org.jsm.model.IDataItem;
import org.jsm.model.IDataSet;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * @author Anatoly Semenov
 * 24.08.2015
 */
public class Hypothesis {

    private final Type type;
    private final Multimap<IProperty, IValue> values;


    public Hypothesis(Type type, Multimap<IProperty, IValue> values) {
        this.type = type;
        this.values = ImmutableMultimap.copyOf(values);
    }


    public static Hypothesis plus(Multimap<IProperty, IValue> values) {
        return new Hypothesis(Type.POSITIVE, values);
    }

    public static Hypothesis minus(Multimap<IProperty, IValue> values) {
        return new Hypothesis(Type.NEGATIVE, values);
    }

    public Type getType() {
        return type;
    }

    public Multimap<IProperty, IValue> getValues() {
        return values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hypothesis that = (Hypothesis) o;

        return type == that.type && !(values != null ? !values.equals(that.values) : that.values != null);

    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (values != null ? values.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Hypothesis{" +
                "type=" + type +
                ", values=" + values +
                '}';
    }


    public boolean apply(IDataSet dataSet) {
        for (IProperty iProperty : values.keySet()) {
            Collection<IValue> possibleValues = values.get(iProperty);
            IDataItem dataItem = dataSet.getDataItems().get(iProperty);
            Collection<IValue> dataSetValues = ImmutableList.<IValue>builder()
                    .addAll(dataItem.getPositiveValues())
                    .addAll(dataItem.getNegativeValues())
                    .build();
            if (Utils.isEmpty(dataSetValues) && !Utils.isEmpty(possibleValues))
                return false;
            Set<IValue> intersection = Sets.intersection(Sets.newHashSet(possibleValues), Sets.newHashSet(dataSetValues));
            if (intersection.isEmpty())
                return false;
        }
        return true;
    }

    public enum Type {
        POSITIVE {
            @Override
            public Collection<IValue> getDataCollection(@Nullable IDataItem dataItem) {
                if (dataItem == null)
                    return Collections.emptyList();
                return dataItem.getPositiveValues();
            }
        },
        NEGATIVE {
            @Override
            public Collection<IValue> getDataCollection(@Nullable IDataItem dataItem) {
                if (dataItem == null)
                    return Collections.emptyList();
                return dataItem.getNegativeValues();
            }
        };

        public abstract Collection<IValue> getDataCollection(@Nullable IDataItem dataItem);
    }
}
