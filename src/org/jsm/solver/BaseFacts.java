package org.jsm.solver;

import com.google.common.collect.Lists;
import org.jsm.model.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Anatoly Semenov
 * Date 13.09.2015.
 */
public class BaseFacts {
    private final IProperty targetProperty;
    private final IValue targetValue;
    private final Collection<IDataSet> facts;
    private final Hypothesis.Type type;


    private BaseFacts(Collection<IDataSet> facts, IProperty targetProperty, IValue targetValue, Hypothesis.Type type) {
        this.type = type;
        this.facts = Collections.unmodifiableCollection(facts);
        this.targetProperty = targetProperty;
        this.targetValue = targetValue;
    }

    public IProperty getTargetProperty() {
        return targetProperty;
    }

    public IValue getTargetValue() {
        return targetValue;
    }

    public Collection<IDataSet> getFacts() {
        return facts;
    }

    public static BaseFacts createForDatasets(IDataSets dataSets, IProperty targetProperty, IValue targetValue,
                                              Hypothesis.Type type) {
        List<IDataSet> list = Lists.newArrayList();
        for (IDataSet dataSet : dataSets.getDataSets()) {
            IDataItem candidateItem = dataSet.getDataItems().get(targetProperty);
            if (candidateItem != null && type.getDataCollection(candidateItem).contains(targetValue))
                list.add(dataSet);
        }
        return new BaseFacts(list, targetProperty, targetValue, type);
    }

    public Hypothesis.Type getType() {
        return type;
    }
}
