package org.jsm.solver.impl;

import com.google.common.collect.*;
import org.jsm.model.IDataItem;
import org.jsm.model.IDataSet;
import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.solver.BaseFacts;
import org.jsm.solver.Hypothesis;
import org.jsm.solver.Inductor;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Anatoly Semenov
 * Date 04.09.2015.
 */
public class DefaultInductor implements Inductor {
    @Override
    public List<Hypothesis> induction(BaseFacts baseFacts) {
        List<IDataSet> facts = Lists.newArrayList(baseFacts.getFacts());
        Set<Hypothesis> hypothesises = Sets.newHashSet();
        for (int i = 0; i < facts.size(); i++) {
            for (int j = i + 1; j < facts.size(); j++) {
                hypothesises.addAll(intersection(facts.get(i), facts.get(j),
                        baseFacts.getTargetProperty(),
                        baseFacts.getType()));
            }
        }
        return Lists.newArrayList(hypothesises);
    }

    private List<Hypothesis> intersection(IDataSet a, IDataSet b, IProperty targetProperty, Hypothesis.Type type) {
        Map<IProperty, IDataItem> aValues = a.getDataItems();
        Map<IProperty, IDataItem> bValues = b.getDataItems();
        List<Hypothesis> hypothesises = Lists.newArrayList();

        Set<IProperty> allProperties = Sets.union(aValues.keySet(), bValues.keySet());

        for (IProperty iProperty : allProperties) {
            if (aValues.containsKey(iProperty) && bValues.containsKey(iProperty) && !targetProperty.equals(iProperty)) {
                IDataItem aDataItem = aValues.get(iProperty);
                IDataItem bDataItem = bValues.get(iProperty);

                Set<IValue> positiveCommonValues = Sets.intersection(Sets.newHashSet(aDataItem.getPositiveValues()),
                        Sets.newHashSet(bDataItem.getPositiveValues()));
                Set<IValue> negativeCommonValues = Sets.intersection(Sets.newHashSet(aDataItem.getPositiveValues()),
                        Sets.newHashSet(bDataItem.getPositiveValues()));

                Collection<IValue> propertyResult = ImmutableSet.copyOf(Sets.intersection(positiveCommonValues, negativeCommonValues));
                if (propertyResult.isEmpty())
                    continue;
                ArrayListMultimap<IProperty, IValue> arrayListMultimap = ArrayListMultimap.create();
                arrayListMultimap.putAll(iProperty, propertyResult);
                hypothesises.add(new Hypothesis(type, arrayListMultimap));
            }
        }
        return collapseHypothesis(hypothesises, type);
    }


    private List<Hypothesis> collapseHypothesis(List<Hypothesis> hypothesises, Hypothesis.Type type) {
        Hypothesis[] arr = hypothesises.toArray(new Hypothesis[hypothesises.size()]); //todo copy-paste from http://kesh.kz/blog/%D0%BF%D0%B5%D1%80%D0%B5%D0%B1%D0%BE%D1%80-%D0%B2%D1%81%D0%B5%D1%85-%D0%BF%D0%BE%D0%B4%D0%BC%D0%BD%D0%BE%D0%B6%D0%B5%D1%81%D1%82%D0%B2-%D0%BC%D0%BD%D0%BE%D0%B6%D0%B5%D1%81%D1%82%D0%B2%D0%B0/
        int N = arr.length;
        List<Hypothesis> result = Lists.newArrayList();
        for (int mask = 0; mask < (1L << N); mask++) {//������� �����
            Multimap<IProperty, IValue> collapseCollector = ArrayListMultimap.create();
            for (int j = 0; j < N; j++) {//������� �������� �������
                if((mask & (1L << j)) != 0){//����� ������� � �����
                    collapseCollector.putAll(arr[j].getValues());
                }
            }
            if (collapseCollector.isEmpty()) //ignore empty subset of set
                continue;
            result.add(new Hypothesis(type, collapseCollector));
        }
        return result;
    }
}
