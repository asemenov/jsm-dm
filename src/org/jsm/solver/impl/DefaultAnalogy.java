package org.jsm.solver.impl;

import com.google.common.collect.*;
import org.jsm.common.Utils;
import org.jsm.model.IDataSet;
import org.jsm.solver.Analogy;
import org.jsm.solver.BaseFacts;
import org.jsm.solver.Hypothesis;

import java.util.List;
import java.util.Set;

/**
 * @author Anatoly Semenov
 *         27.10.2015
 */
public class DefaultAnalogy implements Analogy {

    private static final int DENY_THRESHOLD = 1;


    @Override
    public Multimap<Hypothesis.Type, Hypothesis> analogy(List<Hypothesis> positive, BaseFacts positiveFacts,
                                                         List<Hypothesis> negative, BaseFacts negativeFacts) {

        List<Hypothesis> filteredPositiveHypothesis = denyCounterExample(positive, negativeFacts, DENY_THRESHOLD);
        List<Hypothesis> filteredNegativeHypothesis = denyCounterExample(negative, positiveFacts, DENY_THRESHOLD);
        if (!Utils.isEmpty(filteredNegativeHypothesis) && !Utils.isEmpty(filteredPositiveHypothesis)) {
            Set<Hypothesis> p = Sets.newHashSet(filteredPositiveHypothesis);
            p.removeAll(filteredNegativeHypothesis);

            Set<Hypothesis> n = Sets.newHashSet(filteredNegativeHypothesis);
            n.removeAll(filteredPositiveHypothesis);
            return ImmutableMultimap.<Hypothesis.Type, Hypothesis>builder()
                    .putAll(Hypothesis.Type.POSITIVE, p)
                    .putAll(Hypothesis.Type.NEGATIVE, n)
                    .build();
        }
        else if (Utils.isEmpty(filteredNegativeHypothesis, filteredPositiveHypothesis))
            return ImmutableMultimap.of();
        else if (Utils.isEmpty(filteredNegativeHypothesis))
            return ImmutableMultimap.<Hypothesis.Type, Hypothesis>builder()
                    .putAll(Hypothesis.Type.POSITIVE, filteredPositiveHypothesis)
                    .build();
        else if (Utils.isEmpty(filteredPositiveHypothesis))
            return ImmutableMultimap.<Hypothesis.Type, Hypothesis>builder()
                    .putAll(Hypothesis.Type.NEGATIVE, filteredNegativeHypothesis)
                    .build();
        return ImmutableMultimap.of();
    }

    private List<Hypothesis> denyCounterExample(List<Hypothesis> hypothesises, BaseFacts counterFacts, int denyThreshold) {
        List<Hypothesis> filtered = Lists.newArrayList();
        for (Hypothesis h : hypothesises) {
            int matchCount = 0;
            boolean isAcceptable = true;
            for (IDataSet fact : counterFacts.getFacts()) {
                if (h.apply(fact) && ++matchCount >= denyThreshold) {
                    isAcceptable = false;
                    break;
                }
            }
            if (isAcceptable)
                filtered.add(h);
        }
        return filtered;
    }






}
