package org.jsm;

import org.jsm.ui.fx.FXMainFrame;

/**
 * @author Anatoly Semenov
 *         Date: 27.09.2014
 */
public class ApplicationRunner {
    public static void main(String[] args) {
//        MainFrame.main(new String[]{});
        FXMainFrame.main(new String[]{});
//        new SwingMainFrame();
    }
}
