package org.jsm;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Lists;
import org.jsm.model.*;
import org.jsm.model.impl.data.DataItem;
import org.jsm.model.impl.data.DataSet;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.model.ModelImpl;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;
import org.jsm.solver.Hypothesis;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * @author Anatoly Semenov
 *         Date 14.09.2015.
 *         <p>
 *         Sample at https://ru.wikipedia.org/wiki/%D0%94%D0%A1%D0%9C-%D0%BC%D0%B5%D1%82%D0%BE%D0%B4
 */
public class WikiDatasetsExample {
    public static final IValue<String> PLUS = new StringValue("+");
    public static final IValue<String> MINUS = new StringValue("-");
    public static final IValue<String> T = new StringValue("t");

    public final static IProperty P_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("p")
            .setNullable(true)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .addPropertyValue(T)
            .build();

    public static final IProperty C1_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c1")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C2_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c2")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C3_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c3")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C4_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c4")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C5_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c5")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C6_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c6")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C7_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c7")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C8_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c8")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();
    public static final IProperty C9_PROPERTY = SelectProperty.Factory.selectPropertyBuilder()
            .setName("c9")
            .setNullable(false)
            .setMultiple(false)
            .addPropertyValue(PLUS)
            .addPropertyValue(MINUS)
            .build();


    public static IDataSets create() {
        String name = "wiki";
        Collection<IProperty> propertyCollection = Lists.newArrayList(P_PROPERTY, C1_PROPERTY, C2_PROPERTY,
                C3_PROPERTY, C4_PROPERTY, C5_PROPERTY, C6_PROPERTY, C7_PROPERTY, C8_PROPERTY, C9_PROPERTY);
        IModel model = new ModelImpl(name, propertyCollection);

        IDataSet o1 = new DataSet(model, ImmutableMap.<IProperty, IDataItem>builder()
                .put(P_PROPERTY, positiveDataItem(P_PROPERTY))
                .put(C1_PROPERTY, positiveDataItem(C1_PROPERTY))
                .put(C2_PROPERTY, positiveDataItem(C2_PROPERTY))
                .put(C3_PROPERTY, positiveDataItem(C3_PROPERTY))
                .put(C4_PROPERTY, positiveDataItem(C4_PROPERTY))
                .put(C5_PROPERTY, negativeDataItem(C5_PROPERTY))
                .put(C6_PROPERTY, negativeDataItem(C6_PROPERTY))
                .put(C7_PROPERTY, positiveDataItem(C7_PROPERTY))
                .put(C8_PROPERTY, negativeDataItem(C8_PROPERTY))
                .put(C9_PROPERTY, negativeDataItem(C9_PROPERTY))
                .build(), "o1", "o1");

        IDataSet o2 = new DataSet(model, ImmutableMap.<IProperty, IDataItem>builder()
                .put(C1_PROPERTY, positiveDataItem(C1_PROPERTY))
                .put(C2_PROPERTY, positiveDataItem(C2_PROPERTY))
                .put(C3_PROPERTY, negativeDataItem(C3_PROPERTY))
                .put(C4_PROPERTY, positiveDataItem(C4_PROPERTY))
                .put(C5_PROPERTY, positiveDataItem(C5_PROPERTY))
                .put(C6_PROPERTY, negativeDataItem(C6_PROPERTY))
                .put(C7_PROPERTY, positiveDataItem(C7_PROPERTY))
                .put(C8_PROPERTY, negativeDataItem(C8_PROPERTY))
                .put(C9_PROPERTY, negativeDataItem(C9_PROPERTY))
//                .put(P_PROPERTY, (P_PROPERTY))
                .build(), "o2", "o2");

        IDataSet o3 = new DataSet(model, ImmutableMap.<IProperty, IDataItem>builder()
                .put(P_PROPERTY, negativeDataItem(P_PROPERTY))
                .put(C1_PROPERTY, positiveDataItem(C1_PROPERTY))
                .put(C2_PROPERTY, positiveDataItem(C2_PROPERTY))
                .put(C3_PROPERTY, positiveDataItem(C3_PROPERTY))
                .put(C4_PROPERTY, negativeDataItem(C4_PROPERTY))
                .put(C5_PROPERTY, positiveDataItem(C5_PROPERTY))
                .put(C6_PROPERTY, negativeDataItem(C6_PROPERTY))
                .put(C7_PROPERTY, negativeDataItem(C7_PROPERTY))
                .put(C8_PROPERTY, positiveDataItem(C8_PROPERTY))
                .put(C9_PROPERTY, positiveDataItem(C9_PROPERTY))
                .build(), "o3", "o3");

        IDataSet o4 = new DataSet(model, ImmutableMap.<IProperty, IDataItem>builder()
                .put(P_PROPERTY, negativeDataItem(P_PROPERTY))
                .put(C1_PROPERTY, positiveDataItem(C1_PROPERTY))
                .put(C2_PROPERTY, negativeDataItem(C2_PROPERTY))
                .put(C3_PROPERTY, negativeDataItem(C3_PROPERTY))
                .put(C4_PROPERTY, negativeDataItem(C4_PROPERTY))
                .put(C5_PROPERTY, positiveDataItem(C5_PROPERTY))
                .put(C6_PROPERTY, positiveDataItem(C6_PROPERTY))
                .put(C7_PROPERTY, negativeDataItem(C7_PROPERTY))
                .put(C8_PROPERTY, positiveDataItem(C8_PROPERTY))
                .put(C9_PROPERTY, positiveDataItem(C9_PROPERTY))
                .build(), "o4", "o4");

        IDataSet o5 = new DataSet(model, ImmutableMap.<IProperty, IDataItem>builder()
                .put(P_PROPERTY, positiveDataItem(P_PROPERTY))
                .put(C1_PROPERTY, negativeDataItem(C1_PROPERTY))
                .put(C2_PROPERTY, positiveDataItem(C2_PROPERTY))
                .put(C3_PROPERTY, negativeDataItem(C3_PROPERTY))
                .put(C4_PROPERTY, positiveDataItem(C4_PROPERTY))
                .put(C5_PROPERTY, negativeDataItem(C5_PROPERTY))
                .put(C6_PROPERTY, positiveDataItem(C6_PROPERTY))
                .put(C7_PROPERTY, negativeDataItem(C7_PROPERTY))
                .put(C8_PROPERTY, positiveDataItem(C8_PROPERTY))
                .put(C9_PROPERTY, negativeDataItem(C9_PROPERTY))
                .build(), "o5", "o5");

        IDataSet o6 = new DataSet(model, ImmutableMap.<IProperty, IDataItem>builder()
                .put(P_PROPERTY, negativeDataItem(P_PROPERTY))
                .put(C1_PROPERTY, negativeDataItem(C1_PROPERTY))
                .put(C2_PROPERTY, positiveDataItem(C2_PROPERTY))
                .put(C3_PROPERTY, positiveDataItem(C3_PROPERTY))
                .put(C4_PROPERTY, negativeDataItem(C4_PROPERTY))
                .put(C5_PROPERTY, negativeDataItem(C5_PROPERTY))
                .put(C6_PROPERTY, positiveDataItem(C6_PROPERTY))
                .put(C7_PROPERTY, negativeDataItem(C7_PROPERTY))
                .put(C8_PROPERTY, positiveDataItem(C8_PROPERTY))
                .put(C9_PROPERTY, positiveDataItem(C9_PROPERTY))
                .build(), "o6", "o6");

        IDataSet o7 = new DataSet(model, ImmutableMap.<IProperty, IDataItem>builder()
                .put(P_PROPERTY, positiveDataItem(P_PROPERTY))
                .put(C1_PROPERTY, negativeDataItem(C1_PROPERTY))
                .put(C2_PROPERTY, positiveDataItem(C2_PROPERTY))
                .put(C3_PROPERTY, positiveDataItem(C3_PROPERTY))
                .put(C4_PROPERTY, negativeDataItem(C4_PROPERTY))
                .put(C5_PROPERTY, negativeDataItem(C5_PROPERTY))
                .put(C6_PROPERTY, positiveDataItem(C6_PROPERTY))
                .put(C7_PROPERTY, positiveDataItem(C7_PROPERTY))
                .put(C8_PROPERTY, negativeDataItem(C8_PROPERTY))
                .put(C9_PROPERTY, positiveDataItem(C9_PROPERTY))
                .build(), "o7", "o7");

        Collection<IDataSet> dataSetCollection = Lists.newArrayList(o1, o2, o3, o4, o5, o6, o7);
        return new DataSets(model, dataSetCollection);
    }

    private static IDataItem positiveDataItem(IProperty property) {
        return new DataItem(property, Collections.singletonList(PLUS), Collections.emptyList(), Collections.emptyList());
    }

    private static IDataItem negativeDataItem(IProperty property) {
        return new DataItem(property, Collections.emptyList(), Collections.singletonList(MINUS), Collections.emptyList());
    }

    public static List<Hypothesis> positiveHypothesis() {
        return Lists.newArrayList(
                Hypothesis.plus( ImmutableMultimap.of(C2_PROPERTY, PLUS, C4_PROPERTY, PLUS)),
                Hypothesis.plus( ImmutableMultimap.of(C2_PROPERTY, PLUS, C3_PROPERTY, PLUS, C7_PROPERTY, PLUS)),
                Hypothesis.plus( ImmutableMultimap.of(C2_PROPERTY, PLUS)),
                Hypothesis.plus( ImmutableMultimap.of(C2_PROPERTY, PLUS, C6_PROPERTY, PLUS))
        );
    }

    public static List<Hypothesis> negativeHypothesis() {
        return Lists.newArrayList(
                Hypothesis.minus( ImmutableMultimap.of(C1_PROPERTY, PLUS, C5_PROPERTY, PLUS, C8_PROPERTY, PLUS, C9_PROPERTY, PLUS)),
                Hypothesis.minus( ImmutableMultimap.of(C2_PROPERTY, PLUS, C3_PROPERTY, PLUS, C8_PROPERTY, PLUS, C9_PROPERTY, PLUS)),
                Hypothesis.minus( ImmutableMultimap.of(C8_PROPERTY, PLUS, C9_PROPERTY, PLUS)),
                Hypothesis.minus( ImmutableMultimap.of(C6_PROPERTY, PLUS, C8_PROPERTY, PLUS, C9_PROPERTY, PLUS))
        );
    }
}
