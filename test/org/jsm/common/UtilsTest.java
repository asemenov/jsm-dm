package org.jsm.common;

import org.jsm.model.IDataItem;
import org.jsm.model.IModel;
import org.jsm.model.IProperty;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.model.ModelImpl;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * @author Anatoly Semenov
 *         Date: 11.10.2014
 */
public class UtilsTest {
    @Ignore(value = "DataSets dataSets = null;")
    @Test
    public void testReadWrite() throws IOException, ClassNotFoundException {
        StringValue stringValue1 = new StringValue("1");
        StringValue stringValue2 = new StringValue("2");
        SelectProperty selectProperty = new SelectProperty("test prop", false, false, Arrays.asList(stringValue1, stringValue2));

        IModel model = new ModelImpl("test model", Arrays.asList(selectProperty));

        Map<IProperty, IDataItem> mapProperty = new HashMap<IProperty, IDataItem>();

        //mapProperty.put()

        DataSets dataSets = null;

        /*DataSets dataSets = new DataSets(model, Arrays.asList(
                new DataSet(model, Arrays.asList(new DataItem(selectProperty, Arrays.asList(stringValue1), Arrays.asList(stringValue2), Collections.<IValue>emptyList())),
                "name", "comment")));*/

        File tmpFile = File.createTempFile("bla", "bla");
        Path to = Utils.Serialization.save(dataSets, tmpFile.toPath(),  StandardOpenOption.WRITE);

        DataSets read = Utils.Serialization.read(to);

        assertEquals(model.getName(), read.getModel().getName());
        assertEquals(dataSets.getDataSets().size(), read.getDataSets().size());
    }
}
