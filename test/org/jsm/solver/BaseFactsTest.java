package org.jsm.solver;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.jsm.model.*;
import org.jsm.model.impl.data.DataItem;
import org.jsm.model.impl.data.DataSet;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.model.ModelImpl;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.*;

public class BaseFactsTest {

    @Test
    public void testCreatePositiveBaseFacts(){
        IProperty property = new SelectProperty("a", false, false,
                Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        StringValue positive = new StringValue("positive");
        StringValue negative = new StringValue("negative");
        IProperty targetProperty = new SelectProperty("b", false, false,
                Arrays.asList(positive, negative));

        Collection<IProperty> properties = Lists.newArrayList(property, targetProperty);
        IModel model = new ModelImpl("test", properties);

        Map<IProperty, IDataItem> firstDataItems = ImmutableMap.<IProperty, IDataItem>builder()
                .put(property, new DataItem(property, Collections.singleton(new StringValue("1")), Collections.emptyList(), Collections.emptyList()))
                .put(targetProperty, new DataItem(targetProperty, Collections.singleton(positive), Collections.<IValue>emptyList(), Collections.<IValue>emptyList()))
                .build();

        Map<IProperty, IDataItem> secondDataItems = ImmutableMap.<IProperty, IDataItem>builder()
                .put(property, new DataItem(property, Collections.singleton(new StringValue("2")), Collections.emptyList(), Collections.emptyList()))
                .put(targetProperty, new DataItem(targetProperty, Collections.singleton(negative), Collections.<IValue>emptyList(), Collections.<IValue>emptyList()))
                .build();

        IDataSet first = new DataSet(model, firstDataItems, "first", "first");
        IDataSet second = new DataSet(model, secondDataItems, "second", "second");
        IDataSets iDataSets = new DataSets(model, first, second);

        BaseFacts baseFacts = BaseFacts.createForDatasets(iDataSets, targetProperty, positive, Hypothesis.Type.POSITIVE);
        assertEquals("positive base facts size", 1, baseFacts.getFacts().size());
        assertEquals("data item", first, baseFacts.getFacts().iterator().next());
    }

    @Test
    public void testCreateNegativeBaseFacts(){
        IProperty property = new SelectProperty("a", false, false,
                Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        StringValue positive = new StringValue("positive");
        StringValue negative = new StringValue("negative");
        IProperty targetProperty = new SelectProperty("b", false, false,
                Arrays.asList(positive, negative));

        Collection<IProperty> properties = Lists.newArrayList(property, targetProperty);
        IModel model = new ModelImpl("test", properties);

        Map<IProperty, IDataItem> firstDataItems = ImmutableMap.<IProperty, IDataItem>builder()
                .put(property, new DataItem(property, Collections.singleton(new StringValue("1")), Collections.emptyList(), Collections.emptyList()))
                .put(targetProperty, new DataItem(targetProperty, Collections.singleton(positive), Collections.<IValue>emptyList(), Collections.<IValue>emptyList()))
                .build();

        Map<IProperty, IDataItem> secondDataItems = ImmutableMap.<IProperty, IDataItem>builder()
                .put(property, new DataItem(property, Collections.singleton(new StringValue("2")), Collections.emptyList(), Collections.emptyList()))
                .put(targetProperty, new DataItem(targetProperty, Collections.emptyList(), Collections.singletonList(negative), Collections.<IValue>emptyList()))
                .build();

        IDataSet first = new DataSet(model, firstDataItems, "first", "first");
        IDataSet second = new DataSet(model, secondDataItems, "second", "second");
        IDataSets iDataSets = new DataSets(model, first, second);

        BaseFacts baseFacts = BaseFacts.createForDatasets(iDataSets, targetProperty, negative, Hypothesis.Type.NEGATIVE);
        assertEquals("negative base facts size", 1, baseFacts.getFacts().size());
        assertEquals("data item", second, baseFacts.getFacts().iterator().next());
    }

}