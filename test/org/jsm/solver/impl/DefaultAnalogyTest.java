package org.jsm.solver.impl;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import org.jsm.model.IDataSets;
import org.jsm.solver.Analogy;
import org.jsm.solver.BaseFacts;
import org.jsm.solver.Hypothesis;
import org.junit.Test;

import java.util.Collection;
import java.util.List;

import static org.jsm.WikiDatasetsExample.*;
import static org.junit.Assert.assertTrue;

/**
 * @author Anatoly Semenov
 *         27.10.2015
 */
public class DefaultAnalogyTest {

    @Test
    public void simpleAnalogyWikiTest() {
        IDataSets iDataSets = create();
        BaseFacts plusBF = BaseFacts.createForDatasets(iDataSets, P_PROPERTY, PLUS, Hypothesis.Type.POSITIVE);
        BaseFacts minusBF = BaseFacts.createForDatasets(iDataSets, P_PROPERTY, MINUS, Hypothesis.Type.NEGATIVE);

        List<Hypothesis> positiveHypothesis = positiveHypothesis();
        List<Hypothesis> negativeHypothesis = negativeHypothesis();

        Analogy analogy = new DefaultAnalogy();
        Multimap<Hypothesis.Type, Hypothesis> hypothesisMultimap = analogy.analogy(positiveHypothesis, plusBF, negativeHypothesis, minusBF);

        Collection<Hypothesis> positiveResult = hypothesisMultimap.get(Hypothesis.Type.POSITIVE);
        assertTrue(positiveResult.contains(Hypothesis.plus(ImmutableMultimap.of(C2_PROPERTY, PLUS, C4_PROPERTY, PLUS))));
        assertTrue(positiveResult.contains(Hypothesis.plus(ImmutableMultimap.of(C2_PROPERTY, PLUS, C3_PROPERTY, PLUS, C7_PROPERTY, PLUS))));


        Collection<Hypothesis> negativeResult = hypothesisMultimap.get(Hypothesis.Type.NEGATIVE);
        assertTrue(negativeResult.contains(Hypothesis.minus(ImmutableMultimap.of(C1_PROPERTY, PLUS, C5_PROPERTY, PLUS, C8_PROPERTY, PLUS, C9_PROPERTY, PLUS))));
        assertTrue(negativeResult.contains(Hypothesis.minus(ImmutableMultimap.of(C2_PROPERTY, PLUS, C3_PROPERTY, PLUS, C8_PROPERTY, PLUS, C9_PROPERTY, PLUS))));
        assertTrue(negativeResult.contains(Hypothesis.minus(ImmutableMultimap.of(C8_PROPERTY, PLUS, C9_PROPERTY, PLUS))));
        assertTrue(negativeResult.contains(Hypothesis.minus(ImmutableMultimap.of(C6_PROPERTY, PLUS, C8_PROPERTY, PLUS, C9_PROPERTY, PLUS))));

    }
}