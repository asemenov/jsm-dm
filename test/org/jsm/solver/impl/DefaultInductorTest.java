package org.jsm.solver.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import org.jsm.WikiDatasetsExample;
import org.jsm.common.Utils;
import org.jsm.model.*;
import org.jsm.model.impl.data.DataItem;
import org.jsm.model.impl.data.DataSet;
import org.jsm.model.impl.data.DataSets;
import org.jsm.model.impl.model.ModelImpl;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;
import org.jsm.solver.BaseFacts;
import org.jsm.solver.Hypothesis;
import org.jsm.solver.Inductor;
import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DefaultInductorTest {
    @Test
    public void testPositiveWikiInduction() throws IOException, ClassNotFoundException {
        IDataSets testDatasets = WikiDatasetsExample.create();
        BaseFacts baseFacts = BaseFacts.createForDatasets(testDatasets, WikiDatasetsExample.P_PROPERTY,
                WikiDatasetsExample.PLUS, Hypothesis.Type.POSITIVE);
        DefaultInductor defaultInductor = new DefaultInductor();
        List<Hypothesis> hypothesises = defaultInductor.induction(baseFacts);
        assertFalse("hypothesises", Utils.isEmpty(hypothesises));

        for (Hypothesis hypothesis : WikiDatasetsExample.positiveHypothesis())
            assertTrue("induction result contains hypo " + hypothesis, hypothesises.contains(hypothesis));
    }

    @Test
    public void testNegativeWikiInduction() {
        IDataSets testDatasets = WikiDatasetsExample.create();
        BaseFacts baseFacts = BaseFacts.createForDatasets(testDatasets, WikiDatasetsExample.P_PROPERTY,
                WikiDatasetsExample.MINUS, Hypothesis.Type.NEGATIVE);
        DefaultInductor defaultInductor = new DefaultInductor();
        List<Hypothesis> hypothesises = defaultInductor.induction(baseFacts);
        assertFalse("hypothesises", Utils.isEmpty(hypothesises));

        for (Hypothesis hypothesis : WikiDatasetsExample.negativeHypothesis())
            assertTrue("induction result contains hypo " + hypothesis, hypothesises.contains(hypothesis));
    }


    @Test
    public void testSimilarInduction() {

        IProperty aProperty = new SelectProperty("a", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty bProperty = new SelectProperty("b", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty cProperty = new SelectProperty("c", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty yProperty = new SelectProperty("y", false, false, Arrays.asList(new StringValue("+"), new StringValue("-")));

        IModel iModel = new ModelImpl("test model", Arrays.asList(aProperty, bProperty, cProperty, yProperty));

        IDataSets dataSets = new DataSets(iModel, Arrays.asList(
                new DataSet(iModel, ImmutableMap.<IProperty, IDataItem>builder()
                        .put(aProperty, new DataItem(aProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(bProperty, new DataItem(bProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(cProperty, new DataItem(cProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(yProperty, new DataItem(yProperty, Collections.singletonList(new StringValue("+")),
                                Collections.emptyList(), Collections.emptyList()))
                        .build(), "u1", "u1"),
                new DataSet(iModel, ImmutableMap.<IProperty, IDataItem>builder()
                        .put(aProperty, new DataItem(aProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(bProperty, new DataItem(bProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(cProperty, new DataItem(cProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(yProperty, new DataItem(yProperty, Collections.singletonList(new StringValue("+")),
                                Collections.emptyList(), Collections.emptyList()))
                        .build(), "u2", "u2")
                ));

        BaseFacts baseFacts = BaseFacts.createForDatasets(dataSets, yProperty, new StringValue("+"), Hypothesis.Type.POSITIVE);
        DefaultInductor defaultInductor = new DefaultInductor();
        List<Hypothesis> hypothesises = defaultInductor.induction(baseFacts);
        assertFalse("hypothesises are empty", Utils.isEmpty(hypothesises));

        Hypothesis onlyAHypothesis = Hypothesis.plus( ImmutableMultimap.of(aProperty, new StringValue("1")));
        Hypothesis onlyBHypothesis = Hypothesis.plus( ImmutableMultimap.of(bProperty, new StringValue("1")));
        Hypothesis onlyCHypothesis = Hypothesis.plus( ImmutableMultimap.of(cProperty, new StringValue("1")));
        Hypothesis abHypothesis = Hypothesis.plus(    ImmutableMultimap.of(
                aProperty, new StringValue("1"), bProperty, new StringValue("1")));
        Hypothesis bcHypothesis = Hypothesis.plus(    ImmutableMultimap.of(
                bProperty, new StringValue("1"), cProperty, new StringValue("1")));
        Hypothesis acHypothesis = Hypothesis.plus(    ImmutableMultimap.of(
                aProperty, new StringValue("1"), cProperty, new StringValue("1")));
        Hypothesis abcHypothesis = Hypothesis.plus( ImmutableMultimap.of(
                aProperty, new StringValue("1"), bProperty, new StringValue("1"), cProperty, new StringValue("1")));

        for (Hypothesis testHypothesis : new Hypothesis[] {onlyAHypothesis, onlyBHypothesis, onlyCHypothesis,
                abHypothesis, bcHypothesis, acHypothesis, abcHypothesis}) {
            assertTrue("induction not contains testHypothesis " + testHypothesis,
                    hypothesises.contains(testHypothesis));
        }
    }

    @Test
    public void testEmptyIntersection() {
        IProperty aProperty = new SelectProperty("a", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty bProperty = new SelectProperty("b", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty cProperty = new SelectProperty("c", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty yProperty = new SelectProperty("y", false, false, Arrays.asList(new StringValue("+"), new StringValue("-")));

        IModel iModel = new ModelImpl("test model", Arrays.asList(aProperty, bProperty, cProperty, yProperty));

        IDataSets dataSets = new DataSets(iModel, Arrays.asList(
                new DataSet(iModel, ImmutableMap.<IProperty, IDataItem>builder()
                        .put(aProperty, new DataItem(aProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.<IValue>singletonList(new StringValue("2")), Collections.emptyList()))
                        .put(bProperty, new DataItem(bProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.<IValue>singletonList(new StringValue("2")), Collections.emptyList()))
                        .put(cProperty, new DataItem(cProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.<IValue>singletonList(new StringValue("2")), Collections.emptyList()))
                        .put(yProperty, new DataItem(yProperty, Collections.singletonList(new StringValue("+")),
                                Collections.emptyList(), Collections.emptyList()))
                        .build(), "u1", "u1"),
                new DataSet(iModel, ImmutableMap.<IProperty, IDataItem>builder()
                        .put(aProperty, new DataItem(aProperty, Collections.<IValue>singletonList(new StringValue("2")),
                                Collections.<IValue>singletonList(new StringValue("1")), Collections.emptyList()))
                        .put(bProperty, new DataItem(bProperty, Collections.<IValue>singletonList(new StringValue("2")),
                                Collections.<IValue>singletonList(new StringValue("1")), Collections.emptyList()))
                        .put(cProperty, new DataItem(cProperty, Collections.<IValue>singletonList(new StringValue("2")),
                                Collections.<IValue>singletonList(new StringValue("1")), Collections.emptyList()))
                        .put(yProperty, new DataItem(yProperty, Collections.singletonList(new StringValue("+")),
                                Collections.emptyList(), Collections.emptyList()))
                        .build(), "u2", "u2")
        ));

        Inductor inductor = new DefaultInductor();
        List<Hypothesis> result = inductor.induction(BaseFacts.createForDatasets(dataSets, yProperty, new StringValue("+"), Hypothesis.Type.POSITIVE));
        assertTrue("result is not empty", Utils.isEmpty(result));
    }

}