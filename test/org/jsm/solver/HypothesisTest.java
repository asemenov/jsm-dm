package org.jsm.solver;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import org.jsm.model.*;
import org.jsm.model.impl.data.DataItem;
import org.jsm.model.impl.data.DataSet;
import org.jsm.model.impl.model.ModelImpl;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.value.StringValue;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Anatoly Semenov
 *         28.10.2015
 */
public class HypothesisTest {

    @Test
    public void testSimplePositiveApply() throws Exception {
        IProperty aProperty = new SelectProperty("a", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty bProperty = new SelectProperty("b", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty cProperty = new SelectProperty("c", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty yProperty = new SelectProperty("y", false, false, Arrays.asList(new StringValue("+"), new StringValue("-")));

        IModel iModel = new ModelImpl("test model", Arrays.asList(aProperty, bProperty, cProperty, yProperty));

       IDataSet u1 = new DataSet(iModel, ImmutableMap.<IProperty, IDataItem>builder()
                        .put(aProperty, new DataItem(aProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(bProperty, new DataItem(bProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(cProperty, new DataItem(cProperty, Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList(), Collections.emptyList()))
                        .put(yProperty, new DataItem(yProperty, Collections.singletonList(new StringValue("+")),
                                Collections.emptyList(), Collections.emptyList()))
                        .build(), "u1", "u1");

        Hypothesis onlyAHypothesis = Hypothesis.plus( ImmutableMultimap.of(aProperty, new StringValue("1")));
        Hypothesis onlyBHypothesis = Hypothesis.plus( ImmutableMultimap.of(bProperty, new StringValue("1")));
        Hypothesis onlyCHypothesis = Hypothesis.plus( ImmutableMultimap.of(cProperty, new StringValue("1")));
        Hypothesis abHypothesis = Hypothesis.plus(    ImmutableMultimap.of(
                aProperty, new StringValue("1"), bProperty, new StringValue("1")));
        Hypothesis bcHypothesis = Hypothesis.plus(    ImmutableMultimap.of(
                bProperty, new StringValue("1"), cProperty, new StringValue("1")));
        Hypothesis acHypothesis = Hypothesis.plus(    ImmutableMultimap.of(
                aProperty, new StringValue("1"), cProperty, new StringValue("1")));
        Hypothesis abcHypothesis = Hypothesis.plus( ImmutableMultimap.of(
                aProperty, new StringValue("1"), bProperty, new StringValue("1"), cProperty, new StringValue("1")));

        for (Hypothesis testHypothesis : new Hypothesis[] {onlyAHypothesis, onlyBHypothesis, onlyCHypothesis,
                abHypothesis, bcHypothesis, acHypothesis, abcHypothesis}) {
            assertTrue("h" + testHypothesis + " isn`t apply to offer " + u1, testHypothesis.apply(u1));
        }
    }


    @Test
    public void testSimpleNegativeApply() throws Exception {
        IProperty aProperty = new SelectProperty("a", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty bProperty = new SelectProperty("b", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty cProperty = new SelectProperty("c", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty yProperty = new SelectProperty("y", false, false, Arrays.asList(new StringValue("+"), new StringValue("-")));

        IModel iModel = new ModelImpl("test model", Arrays.asList(aProperty, bProperty, cProperty, yProperty));

       IDataSet u1 = new DataSet(iModel, ImmutableMap.<IProperty, IDataItem>builder()
                        .put(aProperty, new DataItem(aProperty,
                                Collections.emptyList(),
                                Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList()))
                        .put(bProperty, new DataItem(bProperty, Collections.emptyList(),
                                Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList()))
                        .put(cProperty, new DataItem(cProperty, Collections.emptyList(),
                                Collections.<IValue>singletonList(new StringValue("1")),
                                Collections.emptyList()))
                        .put(yProperty, new DataItem(yProperty, Collections.singletonList(new StringValue("+")),
                                Collections.emptyList(), Collections.emptyList()))
                        .build(), "u1", "u1");

        Hypothesis onlyAHypothesis = Hypothesis.minus( ImmutableMultimap.of(aProperty, new StringValue("1")));
        Hypothesis onlyBHypothesis = Hypothesis.minus( ImmutableMultimap.of(bProperty, new StringValue("1")));
        Hypothesis onlyCHypothesis = Hypothesis.minus( ImmutableMultimap.of(cProperty, new StringValue("1")));
        Hypothesis abHypothesis = Hypothesis.minus(    ImmutableMultimap.of(
                aProperty, new StringValue("1"), bProperty, new StringValue("1")));
        Hypothesis bcHypothesis = Hypothesis.minus(    ImmutableMultimap.of(
                bProperty, new StringValue("1"), cProperty, new StringValue("1")));
        Hypothesis acHypothesis = Hypothesis.minus(    ImmutableMultimap.of(
                aProperty, new StringValue("1"), cProperty, new StringValue("1")));
        Hypothesis abcHypothesis = Hypothesis.minus( ImmutableMultimap.of(
                aProperty, new StringValue("1"), bProperty, new StringValue("1"), cProperty, new StringValue("1")));

        for (Hypothesis testHypothesis : new Hypothesis[] {onlyAHypothesis, onlyBHypothesis, onlyCHypothesis,
                abHypothesis, bcHypothesis, acHypothesis, abcHypothesis}) {
            assertTrue("h" + testHypothesis + " isn`t apply to offer " + u1, testHypothesis.apply(u1));
        }
    }

    @Test
    public void testDiffeerentApply() {
        IProperty aProperty = new SelectProperty("a", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty bProperty = new SelectProperty("b", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty cProperty = new SelectProperty("c", false, false, Arrays.asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));
        IProperty yProperty = new SelectProperty("y", false, false, Arrays.asList(new StringValue("+"), new StringValue("-")));

        IModel iModel = new ModelImpl("test model", Arrays.asList(aProperty, bProperty, cProperty, yProperty));

        IDataSet u1 = new DataSet(iModel, ImmutableMap.<IProperty, IDataItem>builder()
                .put(aProperty, new DataItem(aProperty,
                        Collections.<IValue>singletonList(new StringValue("1")),
                        Collections.emptyList(),
                        Collections.emptyList()))
                .put(bProperty, new DataItem(bProperty, Collections.emptyList(),
                        Collections.<IValue>singletonList(new StringValue("1")),
                        Collections.emptyList()))
                .put(cProperty, new DataItem(cProperty, Collections.emptyList(),
                        Collections.emptyList(),
                        Collections.<IValue>singletonList(new StringValue("1"))
                ))
                .put(yProperty, new DataItem(yProperty, Collections.singletonList(new StringValue("+")),
                        Collections.emptyList(), Collections.emptyList()))
                .build(), "u1", "u1");

        Hypothesis trueHypothesis = Hypothesis.plus( ImmutableMultimap.of(aProperty, new StringValue("1")));
        assertTrue(trueHypothesis.apply(u1));

        Hypothesis falseHypothesis = Hypothesis.plus( ImmutableMultimap.of(bProperty, new StringValue("3")));
        assertFalse(falseHypothesis.apply(u1));

        Hypothesis trueCompositeHipothesis = Hypothesis.plus( ImmutableMultimap.of(aProperty, new StringValue("1"), bProperty, new StringValue("1")));
        assertTrue(trueCompositeHipothesis.apply(u1));

        Hypothesis falseWithUndef = Hypothesis.plus( ImmutableMultimap.of(aProperty, new StringValue("1"), bProperty, new StringValue("1"), cProperty,  new StringValue("1")));
        assertFalse(falseWithUndef.apply(u1));
    }
}