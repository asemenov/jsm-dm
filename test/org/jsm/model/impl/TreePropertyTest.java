package org.jsm.model.impl;

import org.jsm.model.IProperty;
import org.jsm.model.IValue;
import org.jsm.model.impl.property.SelectProperty;
import org.jsm.model.impl.property.TreeProperty;
import org.jsm.model.impl.value.StringValue;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

/**
 * User: Anatoly Semenov
 * Date: 16.04.2014
 */
public class TreePropertyTest {
    @Ignore(value = "mainProperty.getPropertyValues() contract")
    @Test
    public void testGetPropertyValues() {
        SelectProperty selectProperty = new SelectProperty("test_select_property_lv_1", false, false,
                Arrays.<IValue>asList(new StringValue("1"), new StringValue("2"), new StringValue("3")));

        TreeProperty subTree = new TreeProperty("sub_tree_lv_1", false, false, Arrays.<IProperty>asList(
                new SelectProperty("test_select_property_lv_2", false, false,
                        Arrays.<IValue>asList(new StringValue("4"), new StringValue("5"), new StringValue("6"))),
                new SelectProperty("test_select_property_2_lv_2", false, false,
                        Arrays.<IValue>asList(new StringValue("7"), new StringValue("8"), new StringValue("9")))),
                Collections.<IValue>emptyList());

        SelectProperty selectProperty2 = new SelectProperty("test_select_property_2_lv_1", false, false,
                Arrays.<IValue>asList(new StringValue("10"), new StringValue("11"), new StringValue("12")));


        TreeProperty mainProperty = new TreeProperty("main tree", false, false,
                Arrays.<IProperty>asList(selectProperty, subTree, selectProperty2),
                Collections.<IValue>emptyList());
        assertEquals(Arrays.<IValue>asList(
                 new StringValue("1")
                ,new StringValue("2")
                ,new StringValue("3")
                ,new StringValue("4")
                ,new StringValue("5")
                ,new StringValue("6")
                ,new StringValue("7")
                ,new StringValue("8")
                ,new StringValue("9")
                ,new StringValue("10")
                ,new StringValue("11")
                ,new StringValue("12")
        ), mainProperty.getPropertyValues());
    }


}
